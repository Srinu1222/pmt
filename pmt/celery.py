from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pmt.settings.base')  # DON'T FORGET TO CHANGE THIS ACCORDINGLY
app = Celery('pmt',
             broker='amqp://safearth:kaustubh@ec2-3-111-47-174.ap-south-1.compute.amazonaws.com:5672/host')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
