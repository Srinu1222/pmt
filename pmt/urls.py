from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from fcm_django.api.rest_framework import FCMDeviceViewSet
from pmt_backend.api.redirect_login import redirect_login

urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('accounts/login/', redirect_login),
    url(r'^', include('django.contrib.auth.urls')),
    path('api/', include('pmt_backend.urls')),
    path('devices', FCMDeviceViewSet.as_view({'post': 'create'}), name='create_fcm_device'),
]
