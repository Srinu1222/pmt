from pathlib import Path
import os

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'y$^=jslae$iqubib!wwt@=&e)3%w)h21ngwglay%ex0j@7^ok='
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = ['pmt.safearth-api.in', '3.111.47.174']

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'pmt_backend',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'corsheaders',
    'rest_auth',
    'rest_auth.registration',
    'rest_framework',
    'rest_framework.authtoken',
    'storages',
    # 'django_elasticsearch_dsl',
    'django_better_admin_arrayfield.apps.DjangoBetterAdminArrayfieldConfig',
    'import_export',
    "fcm_django",
    "firebase_admin",
]

from firebase_admin import credentials, initialize_app

cred = credentials.Certificate('pmt_backend/utils/pmt-notifications-firebase-adminsdk-em1wt-63e8b144fd.json')
initialize_app(cred)

FCM_DJANGO_SETTINGS = {
    # default: _('FCM Django')
    "APP_VERBOSE_NAME": 'pmt_backend',
    # Fcm SErver Key
    "FCM_SERVER_KEY": 'AAAA2N2w0OM:APA91bGgxdzQWHHWbDbXHJ9E0HNiWdYqmypDWSAxV3kh2THxYa0yRhntukAvpxG_cEW_dcCK330TQQv9U6EsaEGUWIV2NPZo6qyqXqy42BOjgt021v5Fm7r24xbRW_8wC6pb4OwNImX9',
    # true if you want to have only one active device per registered user at a time
    # default: False
    "ONE_DEVICE_PER_USER": True,
    # devices to which notifications cannot be sent,
    # are deleted upon receiving error response from FCM
    # default: False
    "DELETE_INACTIVE_DEVICES": False,
}

SITE_ID = 1
MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'pmt.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
WSGI_APPLICATION = 'pmt.wsgi.application'
# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASES = {
     'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'safearth',
        'HOST': 'pmt-development.cjs7h1qubgjn.ap-south-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

TIME_ZONE = 'Asia/Kolkata'
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static/")
CSRF_COOKIE_NAME = "csrftoken"
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = 'username'
ACCOUNT_EMAIL_VERIFICATION = 'none'
AUTH_USER_MODEL = 'pmt_backend.User'
OLD_PASSWORD_FIELD_ENABLED = True
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
}
REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'pmt_backend.serializers.token_serializer.UserSerializer',
    'TOKEN_SERIALIZER': 'pmt_backend.serializers.token_serializer.TokenSerializer'
}
REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'pmt_backend.serializers.token_serializer.CustomRegisterSerializer',
}
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
# ELASTICSEARCH_DSL = {
#     'default': {
#         'hosts': 'https://search-safearth-ecom-ukzkoqduqt3ml6vmczg5d7wtwq.ap-south-1.es.amazonaws.com',
#         # 'http_auth': ['safearth', 'Solar@123']
#     },
# }
# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True
USE_TZ = True
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/
# STATIC_ROOT = os.path.join(BASE_DIR, "static/")
AWS_ACCESS_KEY_ID = 'AKIA5IK33RLDAFDW72ZO'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'safearth-pmt-static'
AWS_S3_BUCKET_AUTH = False
AWS_QUERYSTRING_AUTH = False  # don't add complex authentication-related query parameters for requests
AWS_S3_FILE_OVERWRITE = False
AWS_DEFAULT_ACL = None
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
BROKER_URL = 'amqp://safearth:kaustubh@ec2-3-111-47-174.ap-south-1.compute.amazonaws.com:5672/host'

EMAIL_BACKEND = 'django_ses.SESBackend'
DEFAULT_FROM_EMAIL = 'pmt@safearth.in'
# IMPORT_EXPORT_USE_TRANSACTIONS = True
