from django.db import models
from pmt_backend.meta_models import *
from multi_email_field.fields import MultiEmailField
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.timezone import now

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class PendingOTP(models.Model):
    email = models.CharField(max_length=100, blank=True, null=True)
    otp = models.IntegerField(null=True, blank=True)


class Company(models.Model):
    account_owner = models.ForeignKey('User', on_delete=models.CASCADE, related_name='company_owner', null=True, blank=True)
    team_members = models.ManyToManyField('TeamMember', blank=True)
    sub_contractor_company = models.ManyToManyField('self', blank=True)
    credits = models.IntegerField(default=0, null=True, blank=True)
    name = models.CharField(max_length=1000)
    company_name = models.CharField(max_length=1000, blank=True, null=True)
    website = models.CharField(max_length=1000, blank=True, null=True)
    upload_logo = models.ImageField(upload_to='company_images', null=True, blank=True)
    upload_brochure = models.FileField(upload_to='company_images', null=True, blank=True)
    image = models.ImageField(upload_to='company_images', null=True, blank=True)
    primary_email = models.EmailField(max_length=254, null=True, blank=True)
    head_office = models.CharField(max_length=1000, blank=True, null=True)
    regional_office = models.CharField(max_length=1000, blank=True, null=True)
    founded_year = models.IntegerField(null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    annual_turn_over = models.CharField(max_length=1000, blank=True, null=True)
    employee_strength = models.CharField(max_length=1000, blank=True, null=True)
    installed_capacity = models.CharField(max_length=1000, blank=True, null=True)
    min_size_of_projects_installed = models.CharField(max_length=1000, blank=True, null=True)
    max_size_of_projects_installed = models.CharField(max_length=1000, blank=True, null=True)
    states_active_in = models.IntegerField(null=True, blank=True)
    business_model_offered = models.CharField(max_length=1000, blank=True, null=True)
    flagship_projects = models.CharField(max_length=1000, blank=True, null=True)
    is_consumer = models.BooleanField(default=False)
    num_projects_per_year = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Companies"

    def __str__(self):
        return self.name


class KeyPersonnel(models.Model):
    company = models.ForeignKey('company', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    designation = models.CharField(max_length=100, blank=True, null=True)
    qualification = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True, null=True)
    image = models.ImageField(null=True, blank=True, upload_to='company_key_personnel')

    def __str__(self):
        return self.company.name


class Testimonial(models.Model):
    description = models.CharField(max_length=1500, blank=False)
    company = models.ForeignKey('Company', on_delete=models.CASCADE, blank=True, null=True)
    person = models.CharField(max_length=150, blank=False)
    rating = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return self.person


class Award(models.Model):
    company = models.ForeignKey('Company', null=True, blank=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=1500, blank=False)
    date = models.DateTimeField(default=now, blank=True, null=True)
    image = models.ImageField(upload_to='company_awards', null=True, blank=True)

    def __str__(self):
        return self.title


class User(AbstractUser):
    company = models.ForeignKey('Company', on_delete=models.CASCADE, blank=True, null=True)
    designation = models.CharField(max_length=100, null=True, blank=True, choices=DESIGNATION)
    user_type = models.CharField(max_length=100, null=True, blank=True, choices=USERTYPES)
    is_consumer = models.BooleanField(default=False, null=True, blank=True)
    email = models.CharField(max_length=100, null=False, blank=False)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    last_login = models.DateTimeField(null=True, blank=True)
    account_id = models.IntegerField(null=True, blank=True)
    trial_notification_shown = models.BooleanField(default=False, null=True, blank=True)
    is_sample_events_data_read = models.BooleanField(default=True)

    objects = UserManager()

    def __str__(self):
        return self.email


class ReportIssue(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE, null=True, blank=True)
    issue = models.TextField(blank=True, null=True)
    issue_type = models.CharField(max_length=100, null=True, blank=True, choices=ISSUETYPES)

    def __str__(self):
        return self.issue


class ReportImages(models.Model):
    report = models.ForeignKey('ReportIssue', on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(upload_to='issues_screenshots/', null=True, blank=True)

    def __str__(self):
        return str(self.pk)


class Project(models.Model):
    access_provided = models.BooleanField(default=False)
    six_digit_otp = models.IntegerField(null=True, blank=True)
    customer = models.ForeignKey('Customer', on_delete=models.CASCADE, null=True, blank=True)
    stakeholder = models.ForeignKey('StakeHolder', on_delete=models.CASCADE, null=True, blank=True)
    gantt = models.ForeignKey('Gantt', on_delete=models.CASCADE, null=True, blank=True)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    status = models.CharField(default="ACTIVE", max_length=100, null=True, blank=True, choices=PROJECT_STATUS)
    name = models.CharField('Name', max_length=200)
    business_model = models.CharField(max_length=100, null=True, blank=True)
    cod = models.DateTimeField(null=True, blank=True)
    modules_used = models.CharField(max_length=100, null=True, blank=True)
    inverters_used = models.CharField(max_length=100, null=True, blank=True)
    first_year_kw_generation = models.CharField(max_length=100, null=True, blank=True)
    size = models.DecimalField(max_digits=10, decimal_places=2, blank=True)
    location = models.CharField(max_length=100)
    image = models.ImageField(upload_to='project_images', null=True, blank=True)
    score = models.DecimalField(default=8, max_digits=5, decimal_places=2, blank=True)
    stage_reached = models.CharField(default='PRE-REQUISITES', max_length=100, null=True, blank=True, choices=STAGES)
    basic_gantt_report = models.FileField(upload_to='basic_gantt_file', null=True, blank=True)
    details_gantt_report = models.FileField(upload_to='details_gantt_file', null=True, blank=True)
    project_complete = models.BooleanField(default=False)
    subcontractor_project_planning = models.BooleanField(default=False)
    project_completion_date = models.DateTimeField(null=True, blank=True)
    project_start_date = models.DateTimeField(null=True, blank=True)
    upload_boq_file_status = models.BooleanField(default=False)
    no_sub_contractor_info = models.BooleanField(default=False)
    credits_used = models.IntegerField(default=0, null=True, blank=True)
    created_by = models.ForeignKey('TeamMember', on_delete=models.CASCADE, null=True, blank=True)

    def get_status(self):
        return self.status.upper()

    def get_stage_reached(self):
        return self.stage_reached.upper()

    def __str__(self):
        return str(self.pk)


class Customer(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE, null=True, blank=True)
    is_customer_info = models.BooleanField(default=False)
    property_type = models.CharField(max_length=100, null=True, blank=True, choices=PROPERTIES, default=False)
    customer_name = models.CharField(max_length=100)
    company_name = models.CharField(max_length=100)
    primary_email = models.EmailField(max_length=254)
    phone_number = models.CharField(max_length=20)
    emails = MultiEmailField()

    def __str__(self):
        return self.customer_name


class StakeHolder(models.Model):
    stakeholder_type = models.CharField(max_length=100, null=True, blank=True, choices=STAKEHOLDERTYPES, default=False)
    user = models.ForeignKey('User', on_delete=models.CASCADE, null=True, blank=True)
    property_type = models.CharField(max_length=100, null=True, blank=True, choices=PROPERTIES, default=False)
    name = models.CharField(max_length=100)
    company_name = models.CharField(max_length=100)
    primary_email = models.EmailField(max_length=254)
    phone_number = models.CharField(max_length=20)
    emails = MultiEmailField()

    def __str__(self):
        return self.name


class TeamMember(models.Model):
    user = models.ForeignKey('User', on_delete=models.DO_NOTHING)
    projects = models.ManyToManyField('Project', blank=True)
    name = models.CharField('Name', max_length=100)
    is_active_member = models.BooleanField(default=True)
    department = models.CharField(max_length=1000, choices=DEPARTMENT)
    location = models.CharField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return str(self.user.email)


class SubContractorTeamMember(models.Model):
    user = models.ForeignKey('User', on_delete=models.DO_NOTHING)
    projects = models.ManyToManyField('Project', blank=True)
    name = models.CharField(max_length=100)
    is_active_member = models.BooleanField(default=True)
    department = models.CharField(max_length=100, choices=DEPARTMENT)
    location = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.pk)


class ProjectRight(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    team_member = models.ForeignKey('TeamMember', on_delete=models.CASCADE, null=True, blank=True)
    sub_contractor_team_member = models.ForeignKey('SubContractorTeamMember', on_delete=models.CASCADE, null=True,
                                                   blank=True)
    roles = ArrayField(models.CharField(max_length=10000), null=True, blank=True, choices=TEAM_ROLE_TYPES)
    can_view = ArrayField(models.CharField(max_length=10000, null=True, blank=True), null=True, blank=True)
    can_work = ArrayField(models.CharField(max_length=10000, null=True, blank=True), null=True, blank=True)
    can_approve = ArrayField(models.CharField(max_length=10000), null=True, blank=True)
    can_edit = ArrayField(models.CharField(max_length=10000), null=True, blank=True)
    additional_rights = ArrayField(models.JSONField(), null=True, blank=True)


class Databank(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE)


class Document(models.Model):
    databank = models.ForeignKey(Databank, blank=True, null=True, on_delete=models.CASCADE)
    event = models.ForeignKey('Event', blank=True, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=30, blank=True, null=True)
    file = models.FileField(upload_to='project_docs', null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    is_image = models.BooleanField(default=False)
    upload_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    daily_update_pk = models.IntegerField(null=True, blank=True)

    def get_file(self):
        return self.file.url if self.file else None

    def __str__(self):
        return self.title


class ProjectSpecification(models.Model):
    project = models.OneToOneField('Project', on_delete=models.CASCADE)
    selected_project_types = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_drawings = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_accessories = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_components = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_area_of_installation = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_termination = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_inverter_to_ACDB = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_ACDB_to_Transformer = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    selected_Transformer_to_HT_Panel = ArrayField(models.CharField(max_length=1000), null=True, blank=True)

    def __str__(self):
        return self.project.name


class Event(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    name = models.CharField(max_length=10000, null=True, blank=True)
    default_spoc = models.ForeignKey('TeamMember', on_delete=models.CASCADE, related_name='event_spoc', null=True)
    sub_contractor_spoc = models.ForeignKey('SubContractorTeamMember', on_delete=models.CASCADE,
                                            related_name='event_spoc', null=True)
    sub_contractor_approver = models.ForeignKey('SubContractorTeamMember', on_delete=models.CASCADE, related_name='event_approver',
                                         null=True)
    default_approver = models.ForeignKey('TeamMember', on_delete=models.CASCADE, related_name='event_approver',
                                         null=True)
    is_customer = models.BooleanField(default=False, null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    completed_time = models.DateTimeField(null=True, blank=True)
    actual_start_time = models.DateTimeField(null=True, blank=True)
    actual_completed_time = models.DateTimeField(null=True, blank=True)
    days_needed = models.PositiveIntegerField(default=8, null=True, blank=True)
    status = models.CharField(default="PENDING", max_length=100, null=True, blank=True, choices=EVENT_STATUS)
    stage = models.CharField(max_length=100, null=True, blank=True, choices=STAGES)
    Prerequisites = ArrayField(models.JSONField(), null=True, blank=True)
    checklists = ArrayField(models.JSONField(), null=True, blank=True)
    approval_status = models.BooleanField(default=False, null=True, blank=True)
    customer_approval_required = models.BooleanField(default=False, null=True, blank=True)
    is_requested = models.BooleanField(default=False, null=True, blank=True)
    stage_status = models.CharField(default="PENDING", max_length=10000, null=True, blank=True, choices=STAGE_STATUS)
    stage_completion_date = models.DateTimeField(null=True, blank=True)
    event_order_no = models.IntegerField(blank=True, null=True)
    event_start_date = models.IntegerField(blank=True, null=True)
    is_sub_contractor_event = models.BooleanField(default=False, null=True, blank=True)
    selected_names = ArrayField(models.CharField(max_length=100000), null=True, blank=True, default=[])
    additional_members = ArrayField(models.CharField(max_length=1000), null=True, blank=True, default=[])
    delay_reason = models.TextField(null=True, blank=True)
    is_delayed_reason_provided = models.BooleanField(default=False, null=True, blank=True)
    is_delayed = models.BooleanField(default=False)

    def get_status(self):
        return self.status.upper()

    def __str__(self):
        return self.name


class SampleEvent(models.Model):
    stage = models.TextField(null=True, blank=True)
    event = models.TextField(null=True, blank=True)
    is_mandatory = models.BooleanField(default=False)
    pre_requisites = ArrayField(models.CharField(max_length=16))
    checklists = ArrayField(models.CharField(max_length=16))
    sample_event_spoc = models.ForeignKey('TeamMember', on_delete=models.CASCADE, related_name='SampleEvent_spoc', null=True)
    sample_event_approver = models.ForeignKey('TeamMember', on_delete=models.CASCADE, related_name='SampleEvent_approver', null=True)
    days_needed = models.PositiveIntegerField(default=8, null=True, blank=True)

    def __str__(self):
        return str(self.id)


class CommentFile(models.Model):
    event_id = models.CharField(max_length=1000, blank=True, null=True)
    prerequisite_id = models.CharField(max_length=1000, blank=True, null=True)
    checklist_id = models.CharField(max_length=1000, blank=True, null=True)
    comment_id = models.IntegerField(blank=True, null=True)
    file = models.FileField(upload_to='comments_file', blank=True)

    def get_file(self):
        return self.file.url if self.file else None


class Ticket(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    added_date = models.DateTimeField(auto_now_add=True)
    subject = models.TextField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    raised_by = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name="ticket_raised_by", blank=True, null=True)
    assigned_to = models.ForeignKey(
        'TeamMember', on_delete=models.CASCADE, related_name="ticket_assigned_to", blank=True, null=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.subject


class DailyUpdate(models.Model):
    project = models.ForeignKey('Project', null=True, on_delete=models.CASCADE)
    member = models.ForeignKey('TeamMember', on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    today_completed_tasks = ArrayField(models.JSONField(), null=True, blank=True)
    on_going_tasks = ArrayField(models.JSONField(), null=True, blank=True)
    tomorrow_tasks = ArrayField(models.JSONField(), null=True, blank=True)
    document_file = models.FileField(null=True, blank=True)
    image_file = models.FileField(null=True, blank=True)
    today_task_message = ArrayField(models.CharField(max_length=1000), null=True, blank=True)
    tomorrow_task_message = ArrayField(models.CharField(max_length=1000), null=True, blank=True)

    def get_document_file(self):
        return self.document_file.url if self.document_file else None

    def get_image_file(self):
        return self.image_file.url if self.image_file else None

    def __str__(self):
        return self.member.name


class DailyReport(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    filled_by = ArrayField(models.CharField(max_length=10000), null=True, blank=True)
    not_filled_by = ArrayField(models.CharField(max_length=10000), null=True, blank=True)
    report = models.FileField(upload_to='daily_reports', null=True, blank=True)

    def get_file(self):
        return self.report.url if self.report else None

    def __str__(self):
        return self.project.name


class MonthlyReport(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    report = models.FileField(upload_to='monthly_reports', null=True, blank=True)

    def get_file(self):
        return self.report.url if self.report else None


class WeeklyReport(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    report = models.FileField(upload_to='weekly_reports', null=True, blank=True)

    def get_file(self):
        return self.report.url if self.report else None


class Notification(models.Model):
    day = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    is_seen = models.BooleanField(default=False, null=True, blank=True)
    notified_to = models.ManyToManyField('TeamMember', blank=True)
    company = models.ForeignKey('Company', null=True, blank=True, on_delete=models.CASCADE)
    project = models.ForeignKey('Project', on_delete=models.CASCADE, null=True)
    event = models.ForeignKey('event', on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=1000, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    notification_type = models.CharField(max_length=1000, null=True, blank=True, choices=NOTIFICATION_CHOICES)

    def __str__(self):
        return self.title


class Inventory(models.Model):
    company = models.ForeignKey('Company', on_delete=models.CASCADE, blank=True, null=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE, blank=True, null=True)
    stock_quantity = models.FloatField(default=0.000000)
    value = models.FloatField(default=0.000000)
    target_price = models.FloatField(default=0.000000)
    used_quantity = models.FloatField(default=0.000000)
    quantity_wasted = models.FloatField(default=0.000000)
    product_name = models.CharField(max_length=1000, blank=True, null=True)
    location = models.CharField(max_length=1000, blank=True, null=True)
    target_delivery_date = models.DateTimeField(null=True, blank=True)
    preferred_brands = models.CharField(max_length=1000, blank=True, null=True)
    material_name = models.CharField(max_length=1000, blank=True, null=True, choices=COMPONENTS)
    specifications = models.CharField(max_length=10000, blank=True, null=True)
    pdi_needed = models.CharField(max_length=1000, null=True, blank=True, choices=PDI_NEEDED)
    status = models.CharField(max_length=1000, default='PO_PLACED', choices=INVENTORY_STATUS)
    upload_po_file = models.FileField(default=None, upload_to='uploaded_po_file', null=True, blank=True)
    is_wasted = models.BooleanField(default=False, null=True, blank=True)
    assign_spoc = models.OneToOneField('TeamMember', on_delete=models.CASCADE, related_name='inventory_assign_spoc', blank=True, null=True)
    spoc = models.ForeignKey('TeamMember', on_delete=models.CASCADE, blank=True, null=True)
    reason = models.TextField(blank=True, null=True)
    quantity_format = models.CharField(max_length=1000, blank=True, null=True)

    def get_upload_po_file(self):
        return self.upload_po_file.url if self.upload_po_file else None

    def __str__(self):
        return self.project.name


class PaymentInfo(models.Model):
    company = models.OneToOneField('Company', on_delete=models.CASCADE)
    billing_name = models.CharField(max_length=10000, blank=True, null=True)
    gst = models.CharField(max_length=100, null=True, blank=True)
    address = models.TextField(blank=True, null=True)
    contact_person = models.CharField(max_length=10000, blank=True, null=True)
    contact_person_number = models.CharField(max_length=20, blank=True, null=True)
    estimated_amount = models.IntegerField(default=0, blank=True, null=True)
    next_bill = models.CharField(max_length=10000, blank=True, null=True)

    def __str__(self):
        return self.company.name


class Invoice(models.Model):
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    name = models.CharField(max_length=10000, blank=True, null=True)
    file = models.FileField(default=None, upload_to='invoices', null=True, blank=True)
    order_id = models.CharField(max_length=10000, blank=True, null=True)
    payment_success = models.BooleanField(default=False)

    def __str__(self):
        return self.company.name


def get_tags():
    return list(dict(TAGS).keys())


class Gantt(models.Model):
    gantt_file = models.FileField(default=None, upload_to='gantt_file', null=True, blank=True)
    specifications = models.FileField(upload_to='specifications', null=True, blank=True)
    project_impact = models.FileField(default=None, upload_to='project_impact', null=True, blank=True)
    gantt_image = models.ImageField(upload_to='gantt_images', null=True, blank=True)
    company = models.ManyToManyField('Company', blank=True)
    remarks = models.TextField(null=True, blank=True)
    tags = ArrayField(models.CharField(choices=TAGS, max_length=16), default=get_tags)
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    created_by = models.ForeignKey('TeamMember', on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=10000, blank=True, null=True)

    def __str__(self):
        return str(self.name)


class Finance(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, null=True)
    budget_service_side = ArrayField(models.JSONField(), null=True, blank=True)
    budget_supply_side = ArrayField(models.JSONField(), null=True, blank=True)
    actual_service_side = ArrayField(models.JSONField(), null=True, blank=True)
    actual_supply_side = ArrayField(models.JSONField(), null=True, blank=True)
    finance_file = models.FileField(default=None, upload_to='finance_file', null=True, blank=True)

    def __str__(self):
        return str(self.id)


class RequestDemo(models.Model):
    email = models.CharField(max_length=100, null=False, blank=False)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    company = models.CharField(max_length=100, null=False, blank=False)
    name = models.CharField(max_length=100, null=False, blank=False)
    comment = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return str(self.id)


# CRM
class Board(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return str(self.id)


class Lead(models.Model):
    lead_board = models.ForeignKey('Board', on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=100, null=False, blank=False)
    handler_name = models.CharField(max_length=100, null=False, blank=False)
    project_size = models.IntegerField(default=0, blank=True, null=True)


    def __str__(self):
        return str(self.id)


