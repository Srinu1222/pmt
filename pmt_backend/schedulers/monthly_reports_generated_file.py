import os
from datetime import datetime

import xlsxwriter
from reportlab.platypus import Paragraph, Table, TableStyle, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate
from reportlab.lib.units import mm, inch
from reportlab.lib.enums import TA_CENTER, TA_LEFT
import io
from django.core.files import File


def generate_monthly_report_in_pdf(project_name, date, finished_stage_events_dict, in_progress_stages_dict, monthly_report):
    stream = io.BytesIO()
    im = Image('https://safearth-static.s3.ap-south-1.amazonaws.com/logo.png.png', 2 * inch, 1 * inch)
    im.hAlign = 'LEFT'
    sp = ParagraphStyle('Title',
                        alignment=TA_CENTER,
                        fontSize=12,
                        fontName="Helvetica-Bold",
                        textColor="#5a79af",
                        leading=8
                        )
    p = ParagraphStyle('body',
                       alignment=TA_CENTER,
                       fontSize=10,
                       fontName="Helvetica",
                       leading=10,
                       )
    bp = ParagraphStyle('body',
                        alignment=TA_LEFT,
                        fontSize=14,
                        fontName="Helvetica",
                        leading=16,
                        leftIndent=14,
                        )

    bpd = ParagraphStyle('body',
                         textColor='#102537',
                         alignment=TA_LEFT,
                         fontSize=14,
                         fontName="Helvetica",
                         leading=16,
                         leftIndent=8,
                         )

    spd = ParagraphStyle('body',
                         alignment=TA_LEFT,
                         fontSize=12,
                         fontName="Helvetica",
                         leading=16,
                         leftIndent=14,
                         )

    flowables = []
    my_doc = SimpleDocTemplate(stream)
    flowables.append(im)
    flowables.append(Paragraph("Monthly Report for {}".format(datetime.now().strftime("%B")), sp))
    flowables.append(Spacer(0, 8 * mm))
    flowables.append(Paragraph("Project Name : " + project_name, bpd))
    flowables.append(Spacer(0, 6 * mm))

    flowables.append(Paragraph("Date : " + date, bpd))
    flowables.append(Spacer(0, 8 * mm))

    # Finished
    flowables.append(Paragraph("Events Completed in this Month : ", bpd))
    flowables.append(Spacer(0, 8 * mm))

    for stage, events_list in finished_stage_events_dict.items():
        if events_list == []:
            continue;
        flowables.append(Paragraph(stage.capitalize()+':', bp))
        flowables.append(Spacer(0, 6 * mm))
        for index, event in enumerate(events_list):
            string = str(index+1)+') '+event['event_name']+':'+' Completed on '+event['completed_on']+' by '+event['completed_by']+' and'+' approved by '+event['approved_by']
            flowables.append(Paragraph(string, spd))
            flowables.append(Paragraph('Status:' + 'ON TIME', spd))
        flowables.append(Spacer(0, 6 * mm))


    # On going
    flowables.append(Spacer(0, 10 * mm))
    flowables.append(Paragraph("Stages Ongoing : ", bpd))
    flowables.append(Spacer(0, 10 * mm))

    for k, v in in_progress_stages_dict.items():
        if v == []:
            continue;
        flowables.append(Paragraph(k.capitalize() + ' :', bpd))
        flowables.append(Spacer(0, 6 * mm))
        for index, i in enumerate(v):
            string = str(index+1) + ') ' + i['event_name'] + ':' + ' Expected Completion by ' + i['completion_time'] + ' by ' + i['completed_by']
            flowables.append(Paragraph(string, spd))
            flowables.append(Spacer(0, 2 * mm))
        flowables.append(Spacer(0, 6 * mm))

    flowables.append(Spacer(0, 8 * mm))

    flowables.append(Paragraph('Monthly Report for {} for {}.'.format(project_name, datetime.now().strftime("%B")), p))
    flowables.append(Spacer(0, 4 * mm))
    flowables.append(Paragraph('This is an Automatic Computer Generated Report.', p))

    my_doc.build(flowables)
    pdf_buffer = stream.getbuffer()
    file = open("monthly_reports.pdf", "wb")
    file.write(pdf_buffer)
    file.close()
    monthly_report.report = File(open("monthly_reports.pdf", "rb"))
    monthly_report.save()

    os.remove('monthly_reports.pdf')
