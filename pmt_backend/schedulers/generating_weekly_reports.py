from celery import Celery
from celery.schedules import crontab
from datetime import datetime, timedelta
from pmt_backend.models import Event, MonthlyReport, WeeklyReport
from pmt_backend.schedulers.weekly_reports_generated_file import generate_weekly_report_in_pdf
from pmt_backend.utils.formatting_time import formating_date_time
from django.apps import apps


app = Celery()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    active_projects = project_model.objects.filter(status='ACTIVE')

    for project in active_projects:
        sender.add_periodic_task(
            # Executes every Monday morning at 7:30 a.m.
            crontab(hour=7, minute=30, day_of_week=1),
            generating_weekly_reports.s(project, datetime.now()),
        )


@app.task
def generating_weekly_reports(project, start_date=None):
    helper_for_generating_weekly_reports(project, start_date)


def helper_for_generating_weekly_reports(project, start_date):
    project_name = project.name
    date = formating_date_time(datetime.now())

    finished_stage_events_dict = {
        'PRE-REQUISITES': [],
        'APPROVALS': [],
        'ENGINEERING': [],
        'PROCUREMENT': [],
        'MATERIAL HANDLING': [],
        'CONSTRUCTION': [],
        'SITE HAND OVER': []
    }

    if start_date:
        start_date = datetime.strptime(start_date, "%d/%m/%Y")
        end_date = start_date + timedelta(days=7)
        finished_stage_events = Event.objects.filter(
            project=project,
            stage_status='FINISHED',
            actual_completed_time__range=(start_date, end_date)
        )
    else:
        finished_stage_events = Event.objects.filter(project=project, stage_status='FINISHED')

    for event in finished_stage_events:
        finished_stage_events_dict[event.stage].append(
            {
                'event_name': event.name,
                'completed_on': formating_date_time(
                    event.actual_completed_time) if event.actual_completed_time else formating_date_time(
                    event.completed_time),
                'completed_by': event.default_spoc.name,
                'approved_by': event.default_approver.name
            }
        )

    in_progress_stages_dict = {
        'PRE-REQUISITES': [],
        'APPROVALS': [],
        'ENGINEERING': [],
        'PROCUREMENT': [],
        'MATERIAL HANDLING': [],
        'CONSTRUCTION': [],
        'SITE HAND OVER': []
    }
    in_progress_stage_events = Event.objects.filter(project=project, stage_status='PENDING')
    for event in in_progress_stage_events:
        in_progress_stages_dict[event.stage].append(
            {
                'event_name': event.name,
                'completion_time': formating_date_time(event.completed_time),
                'completed_by': event.default_spoc.name
            }
        )

    weekly_report = WeeklyReport.objects.create(project=project, date=date)
    generate_weekly_report_in_pdf(project_name, date, finished_stage_events_dict, in_progress_stages_dict,
                                  weekly_report)

