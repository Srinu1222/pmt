from django.apps import apps
from pmt.celery import app


@app.task
def update_prerequisite_status_based_on_completed_event(event_id):
    event_model = apps.get_model('pmt_backend.{}'.format('Event'))
    completed_event = event_model.objects.get(pk=event_id)
    completed_event_name = completed_event.name
    project_events = event_model.objects.filter(project=completed_event.project)
    for event in project_events:
        event_pre_requisites = event.Prerequisites
        for prerequisite in event_pre_requisites:
            if prerequisite['title'] == completed_event_name:
                prerequisite['status'] = True
        event.save()
