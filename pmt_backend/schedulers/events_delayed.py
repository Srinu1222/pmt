from celery import Celery
from celery.schedules import crontab
from datetime import datetime

from pmt_backend.mail.mail_tasks.customers.event_delayed import send_event_delayed
from pmt_backend.models import Project, Notification, ProjectRight
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer
from django.apps import apps


app = Celery()


@app.on_after_configure.connect
def event_delayed_periodic_task(sender, **kwargs):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    active_projects = project_model.objects.filter(status='ACTIVE')

    for project in active_projects:
        events = project.event_set.all()
        for event in events:
            if datetime.now() > event.completed_time:
                event.is_delayed = True
                event.save()

                # Executes every evening at 6:00 p.m.
                sender.add_periodic_task(
                    crontab(minute=0, hour=18),
                    event_delayed.s(event),
                )


@app.task
def event_delayed(event):
    # Create Notification
    project = event.project
    company = project.company
    title = 'Event Delayed'
    description = "{} is Delayed".format(event.name)
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='detailed_event'
    )

    project = event.project
    team_members = project.teammember_set.all()
    team_member_list = []
    for member in team_members:
        project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
        can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
        if can_view or can_work or can_edit or can_approve:
            team_member_list.append(member)

    notification.notified_to.add(*team_member_list)
    notification.save()

    # create mail
    send_event_delayed(event.id)
