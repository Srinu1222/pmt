from django.apps import apps
from celery.schedules import crontab
from datetime import datetime
from pmt_backend.models import DailyUpdate, DailyReport, ProjectRight
from pmt_backend.utils.daily_reports_generated_file import generate_daily_report_in_pdf
from pmt.celery import app


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    active_projects = project_model.objects.filter(status='ACTIVE')

    print('1. with on_after_finalize...tha', active_projects)

    for project in active_projects:
        sender.add_periodic_task(10.0, app.generating_daily_reports.s(project, datetime.now()), name='add every 10')
        # # Executes every evening at 6:00 p.m.
        # sender.add_periodic_task(
        #     crontab(minute=0, hour=18),
        #     generating_daily_reports.s(project, datetime.now()),
        # )


@app.task
def generating_daily_reports(project, start_date):
    print('2.celery periodic tasks are running....!')
    helper_for_generating_daily_reports(project, start_date)


def helper_for_generating_daily_reports(project, date):
    print('3.celery periodic tasks are running....!')

    project_name = project.name
    members = project.teammember_set.all()
    members_list = []
    filled_by = []
    not_filled_by = []
    for member in members:
        daily_update = DailyUpdate.objects.filter(member=member, date__year=date.year, date__month=date.month,
                                                  date__day=date.day, project=project).first()

        project_right = ProjectRight.objects.filter(project=project, team_member=member).first()
        role = project_right.roles if project_right else 'No role assigned'

        member_dict = {
            'name': member.name,
            'role': role if role else 'No role assigned',
            'completed': daily_update.today_completed_tasks if daily_update else [],
            'ongoing_tasks': daily_update.on_going_tasks if daily_update else [],
            'planned_for_tomorrow': daily_update.tomorrow_tasks if daily_update else [],
            'today_msg_list': daily_update.today_task_message if daily_update else [],
            'tomorrow_msg_list': daily_update.tomorrow_task_message if daily_update else [],
        }

        if daily_update:
            filled_by.append(member.name)
        else:
            not_filled_by.append(member.name)

        members_list.append(member_dict)

    daily_report = DailyReport.objects.create(project=project, filled_by=filled_by, not_filled_by=not_filled_by)
    generate_daily_report_in_pdf(project_name, date, members_list, daily_report)
