from pmt_backend.models import Event
from pmt_backend.serializers.events.create_event_objects_serializer import create_project_event_obj
from pmt_backend.serializers.events.create_mandatory_stage_events_serializer import stage_indexes


def create_selected_accessories_events(project, selected_accessories, impact_sheet, gantt_sheet):
    all_accessories = {}
    for row in impact_sheet[51:71]:
        all_accessories[row[0].value] = [row[1].value] if type(row[1].value) == int else list(
            map(int, row[1].value.split(',')))

    selected_accessories_events = []
    for accessorie in selected_accessories:
        for index in all_accessories[accessorie]:
            for stage, v in stage_indexes.items():
                try:
                    is_index_in_stage_range = v[0] <= index <= v[1]
                except:
                    is_index_in_stage_range = v[0] <= index

                if is_index_in_stage_range:
                    selected_accessories_events.append(
                        create_project_event_obj(project, stage, gantt_sheet, index, accessorie)
                    )
                    break

    Event.objects.bulk_create(selected_accessories_events, ignore_conflicts=True)
