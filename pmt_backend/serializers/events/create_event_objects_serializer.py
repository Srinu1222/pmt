from datetime import datetime, timedelta

from pmt_backend.models import Event, ProjectRight
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware
from openpyxl import load_workbook


def create_project_event_obj(project, stage, sheet, index, selected_name):
    name = sheet[index][1].value

    # start_time and completed_time
    # project_start_time = project.project_start_date
    start_time = convert_naive_to_aware(project.project_start_date + timedelta(days=sheet[index][8].value))
    completed_time = start_time + timedelta(days=sheet[index][9].value)

    project_right_queryset_spoc = ProjectRight.objects.filter(project=project, roles__contains=[sheet[index][4].value])
    spoc_list = [right.team_member for right in project_right_queryset_spoc]
    count = len(spoc_list)
    try:
        default_spoc = spoc_list[index % count]
    except:
        default_spoc = ProjectRight.objects.filter(project=project, roles__contains=['Project Manager']).first().team_member

    project_right_queryset_approver = ProjectRight.objects.filter(project=project, roles__contains=[sheet[index][5].value])
    approver_list = [right.team_member for right in project_right_queryset_approver]
    count = len(approver_list)
    try:
        default_approver = approver_list[index % count]
    except:
        default_approver = ProjectRight.objects.filter(project=project, roles__contains=['Project Manager']).first().team_member

    prerequisites = sheet[index][3].value
    prerequisites = [i[3:] for i in prerequisites.split("\n") if i != ""]
    prerequisites = [
        {
            "id": index + 1,
            "status": False,
            "title": title,
            'spoc': default_spoc.id,
            "comments": [],
            "due_on": completed_time.strftime('%d/%m/%Y')
        }
        for index, title in enumerate(prerequisites)
    ]

    checklists = sheet[index][6].value
    checklists = [i[3:] for i in checklists.split("\n") if i != ""]
    checklists = [
        {
            "id": index + 1,
            "status": False,
            "title": title,
            'spoc': default_spoc.id,
            "comments": [],
            "due_on": completed_time.strftime('%d/%m/%Y')
        }
        for index, title in enumerate(checklists)
    ]

    return Event(
            project=project,
            stage=stage,
            name=name,
            Prerequisites=prerequisites,
            default_spoc=default_spoc,
            default_approver=default_approver,
            checklists=checklists,
            days_needed=sheet[index][9].value,
            event_order_no=sheet[index][13].value,
            event_start_date=sheet[index][8].value,
            completed_time=completed_time,
            start_time=start_time,
            selected_names=sheet[index][12].value.split(',') if sheet[index][12].value else []
    )


def create_project_event_objs(project, stage, selected_drawings_event_indexes, sheet):
    event_objs_list = []
    for index, selected_name in selected_drawings_event_indexes:
        name = sheet[index][1].value

        # start_time and completed_time
        # project_start_time = project.project_start_date
        start_time = convert_naive_to_aware(project.project_start_date + timedelta(days=sheet[index][8].value))
        completed_time = start_time + timedelta(days=sheet[index][9].value)

        project_right_queryset_spoc = ProjectRight.objects.filter(project=project, roles__contains=[sheet[index][4].value])
        spoc_list = [right.team_member for right in project_right_queryset_spoc]
        count = len(spoc_list)
        try:
            default_spoc = spoc_list[index % count]
        except:
            default_spoc = ProjectRight.objects.filter(project=project, roles__contains=['Project Manager']).first().team_member

        project_right_queryset_approver = ProjectRight.objects.filter(project=project, roles__contains=[sheet[index][5].value])
        approver_list = [right.team_member for right in project_right_queryset_approver]
        count = len(approver_list)
        try:
            default_approver = approver_list[index % count]
        except:
            default_approver = ProjectRight.objects.filter(project=project, roles__contains=['Project Manager']).first().team_member

        prerequisites = sheet[index][3].value
        prerequisites = [i[3:] for i in prerequisites.split("\n") if i != ""]
        prerequisites = [
            {
                "id": index + 1,
                "status": False,
                "title": title,
                'spoc': default_spoc.id,
                "comments": [],
                "due_on": completed_time.strftime('%d/%m/%Y')
            }
            for index, title in enumerate(prerequisites)
        ]

        checklists = sheet[index][6].value
        checklists = [i[3:] for i in checklists.split("\n") if i != ""]
        checklists = [
            {
                "id": index + 1,
                "status": False,
                "title": title,
                'spoc': default_spoc.id,
                "comments": [],
                "due_on": completed_time.strftime('%d/%m/%Y')
            }
            for index, title in enumerate(checklists)
        ]
        event_objs_list.append(
            Event(
                project=project,
                stage=stage,
                name=name,
                Prerequisites=prerequisites,
                default_spoc=default_spoc,
                default_approver=default_approver,
                checklists=checklists,
                days_needed=sheet[index][9].value,
                event_order_no=sheet[index][13].value,
                event_start_date=sheet[index][8].value,
                completed_time=completed_time,
                start_time=start_time,
                selected_names=sheet[index][12].value.split(',') if sheet[index][12].value else []
            )
        )

    Event.objects.bulk_create(event_objs_list, ignore_conflicts=True)
