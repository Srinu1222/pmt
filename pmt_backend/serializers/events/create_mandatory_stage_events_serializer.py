from datetime import timedelta, datetime

from pmt_backend.models import Event, ProjectRight
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware

stages = {
    'Pre-Requisites': 'PRE-REQUISITES',
    'Approvals': 'APPROVALS',
    'Engineering': 'ENGINEERING',
    'Site Hand Over': 'SITE HAND OVER',
    'Procurement ': 'PROCUREMENT',
    'Material Handeling': 'MATERIAL HANDLING',
    'Construction': 'CONSTRUCTION',
}

STAGES_LIST_ACCORDING_TO_GANTT_SHEET = []
stage_indexes = {}


def create_mandatory_stage_events_serializer(sheet, project):
    events_objs_list = []
    for index, row in enumerate(sheet):
        if index == 0:
            continue
        else:
            if row[0].value:
                stage = stages[row[0].value]

                # To capture every stage sections based on indexing
                STAGES_LIST_ACCORDING_TO_GANTT_SHEET.append(stage)
                if stage == 'PRE-REQUISITES':
                    stage_indexes[stage] = [index]
                else:
                    stage_indexes[STAGES_LIST_ACCORDING_TO_GANTT_SHEET[-2]].append(index - 1)
                    stage_indexes[stage] = [index]

            if row[2].value == 'Yes':
                name = row[1].value

                # start_time and completed_time
                start_time = convert_naive_to_aware(project.project_start_date + timedelta(days=row[8].value))
                completed_time = start_time + timedelta(days=int(row[9].value))

                project_right_queryset_spoc = ProjectRight.objects.filter(project=project,
                                                                          roles__contains=[row[4].value])
                spoc_list = [right.team_member for right in project_right_queryset_spoc]
                count = len(spoc_list)
                try:
                    default_spoc = spoc_list[index % count]
                except:
                    default_spoc = ProjectRight.objects.filter(project=project,
                                                               roles__contains=['Project Manager']).first().team_member

                project_right_queryset_approver = ProjectRight.objects.filter(project=project,
                                                                              roles__contains=[row[5].value])
                approver_list = [right.team_member for right in project_right_queryset_approver]
                count = len(approver_list)
                try:
                    default_approver = approver_list[index % count]
                except:
                    default_approver = ProjectRight.objects.filter(project=project, roles__contains=[
                        'Project Manager']).first().team_member

                prerequisites = row[3].value
                prerequisites = [i[3:] for i in prerequisites.split("\n") if i != ""]
                prerequisites = [
                    {
                        "id": index + 1,
                        "status": False,
                        "title": title,
                        'spoc': default_spoc.id,
                        "comments": [],
                        "due_on": completed_time.strftime('%d/%m/%Y')
                    }
                    for index, title in enumerate(prerequisites)
                ]

                checklists = row[6].value
                checklists_lists = [i[3:] for i in checklists.split("\n") if i != ""]
                if 'List of Drawings' == row[1].value:
                    checklists = []
                    for index, title in enumerate(checklists_lists):
                        if title == 'List of All Drawings from Checklist':
                            project_spec = project.projectspecification
                            selected_drawings = project_spec.selected_drawings

                            specifications = []
                            for drawing in selected_drawings:
                                specifications.append(
                                    {
                                        "drawing": drawing,
                                        "spoc": default_spoc.id,
                                        "start_date": completed_time.strftime("%d/%m/%Y")
                                    }
                                )
                            checklists.append({
                                "id": index + 1,
                                "status": False,
                                "title": title,
                                'spoc': default_spoc.id,
                                "comments": [],
                                "due_on": completed_time.strftime('%d/%m/%Y'),
                                'specifications': specifications
                            })
                        else:
                            checklists.append({
                                "id": index + 1,
                                "status": False,
                                "title": title,
                                'spoc': default_spoc.id,
                                "comments": [],
                                "due_on": completed_time.strftime('%d/%m/%Y')
                            })
                else:
                    checklists = [
                        {
                            "id": index + 1,
                            "status": False,
                            "title": title,
                            'spoc': default_spoc.id,
                            "comments": [],
                            "due_on": completed_time.strftime('%d/%m/%Y')
                        }
                        for index, title in enumerate(checklists_lists)
                    ]

                events_objs_list.append(
                    Event(
                        project=project,
                        stage=stage,
                        name=name,
                        Prerequisites=prerequisites,
                        default_spoc=default_spoc,
                        default_approver=default_approver,
                        checklists=checklists,
                        days_needed=row[9].value,
                        event_order_no=row[13].value,
                        event_start_date=row[8].value,
                        start_time=start_time,
                        completed_time=completed_time,
                        selected_names=row[12].value.split(',') if row[12].value else []
                    )
                )

    Event.objects.bulk_create(events_objs_list, ignore_conflicts=True)
