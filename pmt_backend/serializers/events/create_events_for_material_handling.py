from datetime import datetime, timedelta

from pmt_backend.models import Event, ProjectRight, Gantt
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware
from openpyxl import load_workbook


def create_events_for_material_handling_stage(project, material_names):
    gantt_id = project.gantt.pk
    try:
        gantt_file = Gantt.objects.get(pk=gantt_id).gantt_file
    except:
        gantt_file = Gantt.objects.get(pk=23).gantt_file

    workbook = load_workbook(filename=gantt_file)
    sheet = workbook.active

    pdi_yes_events = []
    for name in material_names:
        name = "Pre-Dispatch Inspection" + "_" + name

        # start_time and completed_time
        # project_start_time = project.project_start_date
        start_time = convert_naive_to_aware(datetime.now() + timedelta(days=sheet[92][8].value))
        completed_time = start_time + timedelta(days=sheet[92][9].value)

        spoc = ProjectRight.objects.filter(project=project, roles__contains=[sheet[92][4].value]).first()
        temp_spoc = ProjectRight.objects.filter(project=project, roles__contains=['Project Manager']).first()
        default_spoc = spoc.team_member if spoc else temp_spoc.team_member

        approver = ProjectRight.objects.filter(project=project, roles__contains=[sheet[92][5].value]).first()
        temp_approver = ProjectRight.objects.filter(project=project, roles__contains=['Project Manager']).first()
        default_approver = approver.team_member if approver else temp_approver.team_member

        prerequisites = sheet[92][3].value
        prerequisites = [i[3:] for i in prerequisites.split("\n") if i != ""]
        prerequisites = [
            {
                "id": index + 1,
                "status": False,
                "title": title,
                'spoc': default_spoc.id,
                "comments": [],
                "due_on": completed_time.strftime('%d/%m/%Y')
            }
            for index, title in enumerate(prerequisites)
        ]

        checklists = sheet[92][6].value
        checklists = [i[3:] for i in checklists.split("\n") if i != ""]
        checklists = [
            {
                "id": index + 1,
                "status": False,
                "title": title,
                'spoc': default_spoc.id,
                "comments": [],
                "due_on": completed_time.strftime('%d/%m/%Y')
            }
            for index, title in enumerate(checklists)
        ]

        pdi_yes_events.append(
            Event(
                project=project,
                stage='MATERIAL HANDLING',
                name=name,
                Prerequisites=prerequisites,
                default_spoc=default_spoc,
                default_approver=default_approver,
                checklists=checklists,
                days_needed=sheet[92][9].value,
                event_order_no=sheet[92][12].value,
                event_start_date=sheet[92][8].value,
                completed_time=completed_time,
                start_time=start_time
            )
        )

    Event.objects.bulk_create(pdi_yes_events, ignore_conflicts=True)
