from openpyxl import load_workbook

from pmt_backend.models import Gantt
from pmt_backend.serializers.events.create_event_objects_serializer import create_project_event_objs

selected_termination_events = []


def helper_fun(sheet, index, selected_name):
    global selected_termination_events
    if type(sheet[index][1].value) == int:
        temp = [(sheet[index][1].value, selected_name)]
    else:
        temp = list(map(int, sheet[index][1].value.split(',')))
        temp = [(i, selected_name) for i in temp]
    selected_termination_events = temp + selected_termination_events


def create_selected_termination_events(project, project_spec, selected_termination, impact_sheet, gantt_sheet):
    if selected_termination == 'LT':
        selected_inverter_to_ACDB = project_spec.selected_inverter_to_ACDB
        selected_event_options_dict = {
            'Cable Tray': 76,
            'Cable Trench': 77,
            'HDPE Pipes': 78
        }
        for i in selected_inverter_to_ACDB:
            helper_fun(impact_sheet, selected_event_options_dict[i], i)

        selected_ACDB_to_Transformer = project_spec.selected_ACDB_to_Transformer
        selected_event_options_dict = {
            'Cable Tray': 81,
            'Cable Trench': 82,
            'HDPE Pipes': 83
        }
        for i in selected_ACDB_to_Transformer:
            helper_fun(impact_sheet, selected_event_options_dict[i], i)

    elif selected_termination == 'HT':
        selected_inverter_to_ACDB = project_spec.selected_inverter_to_ACDB
        selected_event_options_dict = {
            'Cable Tray': 88,
            'Cable Trench': 89,
            'HDPE Pipes': 90
        }
        for i in selected_inverter_to_ACDB:
            helper_fun(impact_sheet, selected_event_options_dict[i], i)

        selected_ACDB_to_Transformer = project_spec.selected_ACDB_to_Transformer
        selected_event_options_dict = {
            'Cable Tray': 93,
            'Cable Trench': 94,
            'HDPE Pipes': 95
        }
        for i in selected_ACDB_to_Transformer:
            helper_fun(impact_sheet, selected_event_options_dict[i], i)

        selected_Transformer_to_HT_Panel = project_spec.selected_Transformer_to_HT_Panel
        selected_event_options_dict = {
            'Cable Tray': 98,
            'Cable Trench': 99,
            'HDPE Pipes': 100
        }
        for i in selected_Transformer_to_HT_Panel:
            helper_fun(impact_sheet, selected_event_options_dict[i], i)

    create_project_event_objs(project, 'CONSTRUCTION', selected_termination_events, gantt_sheet)
