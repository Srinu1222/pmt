from pmt_backend.serializers.events.create_event_objects_serializer import create_project_event_objs


def create_selected_drawing_events(project, selected_drawings, impact_sheet, gantt_sheet):
    all_drawings = {}
    for row in impact_sheet[2:17]:
        all_drawings[row[0].value] = [row[1].value] if type(row[1].value) == int else list(
            map(int, row[1].value.split(',')))

    selected_drawings_events = []
    for drawing in selected_drawings:
        for index in all_drawings[drawing]:
            selected_drawings_events.append((index, drawing))

    create_project_event_objs(project, 'ENGINEERING', selected_drawings_events, gantt_sheet)
