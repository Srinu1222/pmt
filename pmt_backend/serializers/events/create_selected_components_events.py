from pmt_backend.models import Event
from pmt_backend.serializers.events.create_event_objects_serializer import create_project_event_obj


def create_selected_components_events(project, selected_components, impact_sheet, gantt_sheet):
    all_components = {}
    for row in impact_sheet[22:47]:
        all_components[row[0].value] = row[1].value if type(row[1].value) == int else list(
            map(int, row[1].value.split(',')))

    stage_indexes = {
        "PRE-REQUISITES": list(range(2, 4)),
        "APPROVALS": list(range(5, 9)),
        "ENGINEERING": list(range(11, 38)),
        "PROCUREMENT": list(range(39, 89)),
        "MATERIAL HANDLING": list(range(91, 141)),
        "CONSTRUCTION": list(range(143, 206)),
        "SITE HAND OVER": list(range(208, 211))
    }

    selected_components_events = []
    for component in selected_components:
        for index in all_components[component]:
            for k, v in stage_indexes.items():
                if index in v:
                    stage = k
                    selected_components_events.append(
                        create_project_event_obj(project, stage, gantt_sheet, index, component)
                    )
                    break

    Event.objects.bulk_create(selected_components_events, ignore_conflicts=True)
