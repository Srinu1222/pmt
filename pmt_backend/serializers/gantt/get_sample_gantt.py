from pmt_backend.utils.formatting_time import formating_date_time


def get_sample_gantt_serializer(company):
    gantt_queryset = company.gantt_set.all()
    serialized_gantt_list = []
    for gantt in gantt_queryset:
        remarks = gantt.remarks
        serialized_gantt_list.append(
            {
                'gantt_id': gantt.id,
                'created_by': gantt.created_by.name if gantt.created_by else '',
                'gantt_name': gantt.name,
                'date': formating_date_time(gantt.date),
                'remarks': remarks if remarks else '',
                'gantt_image': gantt.gantt_image.url if gantt.gantt_image else '',
                'tags': gantt.tags
            }
        )

    return serialized_gantt_list
