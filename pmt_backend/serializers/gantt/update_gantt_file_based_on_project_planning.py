from django.core.files import File
import os
from openpyxl import load_workbook
from pmt_backend.models import Event, TeamMember, ProjectRight
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware


def UpdateGanttFileBasedOnProjectPlanning(previous_gantt, new_gantt, project):
    gantt_file = previous_gantt.gantt_file
    workbook = load_workbook(filename=gantt_file)
    sheet = workbook.active

    for i, row in enumerate(sheet):
        if i == 0:
            continue

        event = Event.objects.filter(event_order_no=row[12].value, project=project).first()
        if event:
            team_member = TeamMember.objects.get(pk=event.default_spoc.id)
            role = ProjectRight.objects.filter(team_member=team_member, project=project).first().roles
            if len(role) > 0:
                row[4].value = role[0]
            row[9].value = event.days_needed
            row[8].value = (event.start_time - convert_naive_to_aware(project.project_start_date)).days
    workbook.save('output.xlsx')
    new_gantt.gantt_file = File(open('output.xlsx', 'rb'))
    new_gantt.save()
    os.remove('output.xlsx')
