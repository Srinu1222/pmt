from openpyxl import load_workbook
from django.core.files import File
import os


def update_specifications_based_on_project_specs(project, new_gantt):
    workbook = load_workbook(filename='pmt_backend/utils/default_specs_sheet.xlsx')
    sheet = workbook.active

    project_spec = project.projectspecification
    selected_drawings = project_spec.selected_drawings if project_spec.selected_drawings else []
    selected_accessories = project_spec.selected_accessories if project_spec.selected_accessories else []
    selected_components = project_spec.selected_components if project_spec.selected_components else []
    selected_area_of_installation = project_spec.selected_area_of_installation if project_spec.selected_area_of_installation else []
    selected_termination = project_spec.selected_termination[0] if project_spec.selected_termination else []
    selected_inverter_to_ACDB = project_spec.selected_inverter_to_ACDB if project_spec.selected_inverter_to_ACDB else []
    selected_ACDB_to_Transformer = project_spec.selected_ACDB_to_Transformer if project_spec.selected_ACDB_to_Transformer else []
    selected_Transformer_to_HT_Panel = project_spec.selected_Transformer_to_HT_Panel if project_spec.selected_Transformer_to_HT_Panel else []

    for row in sheet[2:17]:
        if row[0].value in selected_drawings:
            row[1].value = 'Yes'

    for row in sheet[22:47]:
        if row[0].value in selected_components:
            row[1].value = 'Yes'

    for row in sheet[51:71]:
        if row[0].value in selected_accessories:
            row[1].value = 'Yes'

    for row in sheet[104:106]:
        if row[0].value in selected_area_of_installation:
            row[1].value = 'Yes'

    if selected_termination == 'LT':

        sheet[74][1].value = 'Yes'

        for row in sheet[76:78]:
            if row[0].value in selected_inverter_to_ACDB:
                row[1].value = 'Yes'

        for row in sheet[81:83]:
            if row[0].value in selected_ACDB_to_Transformer:
                row[1].value = 'Yes'

    elif selected_termination == 'HT':

        sheet[86][1].value = 'Yes'

        for row in sheet[88:90]:
            if row[0].value in selected_inverter_to_ACDB:
                row[1].value = 'Yes'

        for row in sheet[93:95]:
            if row[0].value in selected_ACDB_to_Transformer:
                row[1].value = 'Yes'

        for row in sheet[98:100]:
            if row[0].value in selected_Transformer_to_HT_Panel:
                row[1].value = 'Yes'

    workbook.save('output.xlsx')

    new_gantt.specifications = File(open('output.xlsx', 'rb'))
    new_gantt.save()

    os.remove('output.xlsx')
