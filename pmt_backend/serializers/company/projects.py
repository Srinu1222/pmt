from datetime import datetime

from pmt_backend.models import Notification, TeamMember, Event, Customer, Project, SubContractorTeamMember, StakeHolder
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware
from pmt_backend.serializers.project.gantt_view import get_stage_data


def get_projects_serializer(project_queryset):
    serialized_projects_list = []
    for project in project_queryset:
        project_start_date = project.project_start_date
        project_start_date = project_start_date if project_start_date else convert_naive_to_aware(datetime.now())

        current_date = datetime.now()
        time_delta = convert_naive_to_aware(current_date) - project_start_date
        finished_events_days = time_delta.days

        completed_time_for_last_event = Event.objects.filter(project=project).order_by(
            '-completed_time').first().completed_time
        time_delta = completed_time_for_last_event - project_start_date
        total_days_needed = time_delta.days

        serialized_projects_list.append({
            'project_id': project.id,
            'project_name': project.name,
            'size': project.size,
            'location': project.location,
            'total_days_needed': total_days_needed,
            'Finished_events_days': finished_events_days,
            'score': project.score
        })

    return serialized_projects_list


def helper_fun_active_projects(active_projects, is_sub_contractor, member):
    active_projects_list = []
    for project in active_projects:
        project_events = project.event_set.all()

        project_start_date = project.project_start_date
        project_start_date = project_start_date if project_start_date else convert_naive_to_aware(datetime.now())
        current_date = datetime.now()
        time_delta = convert_naive_to_aware(current_date) - project_start_date
        finished_events_days = time_delta.days

        completed_time_for_last_event = Event.objects.filter(project=project).order_by(
            '-completed_time').first().completed_time
        time_delta = completed_time_for_last_event - project_start_date
        total_days_needed = time_delta.days

        last_activity = project_events.filter(status="FINISHED").first().name if project_events.filter(
            status="FINISHED").first() else 'N/A'
        current_activity = project_events.filter(status="IN-PROGRESS").first().name if project_events.filter(
            status="IN-PROGRESS").first() else 'N/A'

        notification_count = Notification.objects.filter(notified_to=member, is_seen=False, project=project).count()

        stages = ['PRE-REQUISITES', 'APPROVALS', 'ENGINEERING', 'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION',
                  'SITE HAND OVER']

        final_stages_list = []
        for stage in stages:
            start_event = Event.objects.filter(project=project, stage=stage).order_by('event_order_no').first()
            stage_schedule_date = start_event.start_time
            final_stages_list.append(
                get_stage_data(project, stage, stage_schedule_date)
            )

        serialized_gantt_view = {
            'pre_requisites': final_stages_list[0],
            'approvals': final_stages_list[1],
            'engineering': final_stages_list[2],
            'procurement': final_stages_list[3],
            'material_handling': final_stages_list[4],
            'construction': final_stages_list[5],
            'site_handover': final_stages_list[6],
        }

        active_projects_list.append({
            "id": project.id,
            "status": project.status,
            "stage": project.stage_reached,
            "total_days_needed": total_days_needed,
            "completed_days_needed": finished_events_days,
            "last_activity": last_activity,
            "current_activity": current_activity,
            "name": project.name,
            "size": project.size,
            "location": project.location,
            "image": project.image.url,
            "project_completion": project.project_complete,
            "is_sub_contractor": is_sub_contractor,
            "access_provided": project.access_provided,
            "subcontractor_project_planning": project.subcontractor_project_planning,
            'project_notification_count': notification_count,
            'stages': serialized_gantt_view,
        })
    return active_projects_list


def helper_fun_for_closed_projects(closed_projects, is_sub_contractor):
    closed_projects_list = []
    for project in closed_projects:
        project_events = project.event_set.all()

        project_start_date = project.project_start_date
        project_start_date = project_start_date if project_start_date else convert_naive_to_aware(datetime.now())

        current_date = datetime.now()
        time_delta = convert_naive_to_aware(current_date) - project_start_date
        finished_events_days = time_delta.days

        completed_time_for_last_event = Event.objects.filter(project=project).order_by(
            'completed_time').first().completed_time
        time_delta = completed_time_for_last_event - project_start_date
        total_days_needed = time_delta.days

        recent_completed_project_event = project_events.order_by('-actual_completed_time').first()
        last_activity = recent_completed_project_event.name if recent_completed_project_event else "No event is completed"
        current_activity = list(project.event_set.filter(status="IN-PROGRESS").values_list('name', flat=True))

        closed_projects_list.append({
            "id": project.id,
            "status": project.status,
            "stage": project.stage_reached,
            "last_activity": last_activity,
            "current_activity": current_activity,
            "total_days_needed": total_days_needed,
            "completed_days_needed": finished_events_days,
            "name": project.name,
            "size": project.size,
            "location": project.location,
            "image": project.image.url,
            "project_completion": project.project_complete,
            "is_sub_contractor": is_sub_contractor,
            "access_provided": project.access_provided,
            "subcontractor_project_planning": project.subcontractor_project_planning,
        })

    return closed_projects_list


def get_dashboard_serializer(user, first_login):
    if not user:
        return None
    team_member = None

    # ..................FOR ACTIVE PROJECTS..................
    if user.is_consumer:
        customer = Customer.objects.get(user=user)
        active_projects = Project.objects.filter(customer=customer, status='ACTIVE').order_by('-id')
        sub_contractor_active_projects = []
    elif user.user_type in ('Investor', 'Safearth Project Manager'):
        stake_holder = StakeHolder.objects.get(user=user, stakeholder_type=user.user_type)
        active_projects = Project.objects.filter(stakeholder=stake_holder, status='ACTIVE').order_by('-id')
        sub_contractor_active_projects = []
    else:
        team_member = TeamMember.objects.filter(user=user).first()
        active_projects = team_member.projects.all().filter(status='ACTIVE').order_by('-id')
        sub_contractor_member = SubContractorTeamMember.objects.filter(user=user).first()
        if sub_contractor_member:
            sub_contractor_active_projects = sub_contractor_member.projects.all().filter(status='ACTIVE').order_by(
                '-id')
            sub_contractor_active_projects = helper_fun_active_projects(sub_contractor_active_projects, True, sub_contractor_member)
        else:
            sub_contractor_active_projects = []

    active_projects_list = helper_fun_active_projects(active_projects, False, team_member)

    # ...............FOR CLOSED PROJECTS..........................
    if user.is_consumer:
        customer = Customer.objects.get(user=user)
        closed_projects = Project.objects.filter(customer=customer, status='CLOSED').order_by('-id')
        sub_contractor_closed_projects = []
    elif user.user_type in ('Investor', 'Safearth Project Manager'):
        stake_holder = StakeHolder.objects.get(user=user, stakeholder_type=user.user_type)
        closed_projects = Project.objects.filter(stakeholder=stake_holder, status='CLOSED').order_by('-id')
        sub_contractor_closed_projects = []
    else:
        team_member = TeamMember.objects.filter(user=user).first()
        closed_projects = team_member.projects.all().filter(status='CLOSED').order_by('-id')
        sub_contractor_member = SubContractorTeamMember.objects.filter(user=user).first()
        if sub_contractor_member:
            sub_contractor_closed_projects = sub_contractor_member.projects.all().filter(status='CLOSED').order_by(
                '-id')
            sub_contractor_closed_projects = helper_fun_for_closed_projects(sub_contractor_closed_projects, True)
        else:
            sub_contractor_closed_projects = []

    closed_projects_list = helper_fun_for_closed_projects(closed_projects, False)

    # NOTIFICATION COUNT LOGIC
    company = user.company
    notification_count = Notification.objects.filter(company=company, is_seen=False).count()


    serialized_project_dashboard = {
        'active_projects': active_projects_list+sub_contractor_active_projects,
        'closed_projects': closed_projects_list+sub_contractor_closed_projects,
        'notification_count': notification_count,
        'first_login': first_login
    }

    return serialized_project_dashboard
