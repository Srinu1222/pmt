from pmt_backend.utils.formatting_time import formating_date_time
from pmt_backend.models import Databank, Customer


def get_projects_databank_serializer(project_queryset, is_image, user):
    serialized_projects_list = []
    serialized_basic_docs_list = []

    # Handling DOCS visible for customer login
    customer = Customer.objects.filter(user=user).first()

    for project in project_queryset:
        data_bank_obj = Databank.objects.filter(project=project).first()
        if customer and not is_image:
            project_docs = data_bank_obj.document_set.filter(is_image=is_image, event__stage='ENGINEERING')
        else:
            project_docs = data_bank_obj.document_set.filter(is_image=is_image)
        total_files_count = project_docs.count()

        serialized_basic_docs_list.append(
            {
                'project_name': project.name,
                'total_files_count': total_files_count,
                'project_id': project.pk
            }
        )

        for doc in project_docs:
            serialized_projects_list.append(
                {
                    'project_id': project.pk,
                    'project_name': project.name,
                    'stage': doc.event.stage if doc.event else '',
                    'owner': doc.event.default_spoc.name if doc.event else '',
                    'date': formating_date_time(doc.upload_date),
                    'document_name': doc.title,
                    'file': doc.file.url if doc.file else None,
                    'total_files_count': total_files_count
                }
            )
    return serialized_projects_list, serialized_basic_docs_list
