from pmt_backend.models import Event


def get_teams_serializer(user, is_mobile=None):
    if not user:
        return None

    team_members = user.company.team_members.filter(is_active_member=True)

    team_members_list = []
    for team_member in team_members:
        active_project_list = team_member.projects.all().filter(status='ACTIVE').values_list('name')
        total_active_project_list = team_member.projects.all().values_list('name')
        project_list = []
        active_list = []
        for name in active_project_list:
            active_list.append(name)

        for name in total_active_project_list:
            project_list.append(name)

        number_of_projects = len(active_project_list)
        total_projects = len(total_active_project_list)
        events = Event.objects.filter(default_spoc=team_member, status="IN-PROGRESS")
        if is_mobile:
            event_list = [
                {
                    'event_name': event.name,
                    'project_name': event.project.name,
                    'stage_name': event.project.stage_reached
                }
                for event in events
            ]
        else:
            event_list = [event.name for event in events]

        team_members_list.append({
            'id': team_member.pk,
            'name': team_member.name,
            'department': team_member.department,
            'email': team_member.user.email,
            'designation': team_member.user.designation,
            'phone_number': team_member.user.phone_number,
            'number_of_projects': number_of_projects,
            'Total_projects': total_projects,
            'currently_working_on': event_list,
            'active_project_list': project_list,
            'total_project_list': active_list,
        })

    serialized_teams = {
        'team_members_list': team_members_list,
    }

    return serialized_teams


def get_team_members(user):
    if not user:
        return None

    team_members = user.company.team_members.filter(is_active_member=True)

    team_members_list = []
    for team_member in team_members:
        team_members_list.append({
            'id': team_member.pk,
            'name': team_member.name
        })

    serialized_teams = {
        'team_members_list': team_members_list,
    }

    return serialized_teams
