from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Inventory, Project, TeamMember
from pmt_backend.utils.reading_units_module import all_components_unit_dict
from django.db.models import Q


def get_inventory_serializer(company, is_sort, is_filter, operation_list):
    if is_sort == 'true':
        if operation_list[0] == 'A-Z':
            inventory_queryset = Inventory.objects.filter(company=company).order_by('preferred_brands')
        else:
            inventory_queryset = Inventory.objects.filter(company=company).order_by('-preferred_brands')

    elif is_filter == 'true':
        inventory_queryset = Inventory.objects.filter(
            Q(project__name__in=operation_list[0]) |
            Q(material_name__in=operation_list[1]) |
            Q(location__in=operation_list[2]),
            company=company,
        ).order_by('pk')

    else:
        inventory_queryset = Inventory.objects.filter(company=company).order_by('pk')

    stock_inventory = []
    consumed_inventory = []
    wasted_inventory = []

    # FOR SORTING AND FILTERING
    project_list = []
    category_list = []
    location_list = []

    for inventory in inventory_queryset:
        if inventory.project:
            project_id = inventory.project.pk
            project_name = inventory.project.name
        else:
            project_id = ''
            project_name = ''

        component_name = inventory.material_name
        inventory_id = inventory.pk
        location = inventory.location
        quantity_in_stock = inventory.stock_quantity
        used_quantity = inventory.used_quantity
        rate = inventory.target_price  # quantity is directly proportional to rate
        view_po = inventory.upload_po_file.url if inventory.upload_po_file else None
        product = inventory.preferred_brands

        value = quantity_in_stock * rate

        # For SORTING AND FILTERING
        if location not in location_list:
            location_list.append(location)
        if project_name not in project_list:
            project_list.append(project_name)
        if component_name not in category_list:
            category_list.append(component_name)

        # To get Units
        component_units_dict = all_components_unit_dict.get(component_name)
        if component_units_dict:
            format_dict = component_units_dict.get(inventory.quantity_format, component_units_dict['format1'])
            if format_dict:
                quantity_units = format_dict['unit_format']
            else:
                return send_fail_http_response(msg='This Inventory Format is not Entered.'), False
        else:
            return send_fail_http_response(msg='Component Name ({}) is Not Matching'.format(component_name)), False

        stock_inventory.append(
            {
                'product': product,
                "inventory_id": inventory_id,
                "project_id": project_id,
                "project_name": project_name,
                "category": component_name,
                "quantity": str(quantity_in_stock) + str(quantity_units),
                "rate": rate,
                "value": value,
                "view_po": view_po,
                "location": location,
                "status": inventory.status,
                'specification': inventory.specifications,
            }
        )

        value = used_quantity * rate
        if used_quantity > 0:
            consumed_inventory.append(
                {
                    'product': product,
                    "inventory_id": inventory_id,
                    "project_id": project_id,
                    "project_name": project_name,
                    "category": component_name,
                    "quantity": str(used_quantity) + str(quantity_units),
                    "rate": rate,
                    "value": value,
                    "view_po": view_po,
                    "location": location,
                    "status": inventory.status,
                    "data": inventory.target_delivery_date,
                    'specification': inventory.specifications,
                }
            )

        if inventory.is_wasted:
            value = inventory.quantity_wasted * rate
            member = TeamMember.objects.get(pk=inventory.spoc.id)
            wasted_inventory.append(
                {
                    'product': product,
                    "inventory_id": inventory_id,
                    "project_id": project_id,
                    "project_name": project_name,
                    "category": component_name,
                    "view_po": view_po,
                    "rate": rate,
                    "quantity": str(inventory.quantity_wasted) + str(quantity_units),
                    "value": value,
                    "reason": inventory.reason,
                    "spoc": {'name': member.name, 'id': member.pk},
                    "status": inventory.status,
                    'specification': inventory.specifications,
                }
            )

    serialized_inventory_details = {
        'stock': stock_inventory,
        'consumed': consumed_inventory,
        'wastage': wasted_inventory,
        'project_list': project_list,
        'category_list': category_list,
        'location_list': location_list,
        'operation_list': operation_list
    }

    return serialized_inventory_details, True
