from pmt_backend.models import Notification, User


def specifications_changed(event):
    project = event.project
    company = project.company
    description = 'Specification Changed'
    title = 'Specification Changed'
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='specification_page'
    )

    project = event.project
    company = project.company
    team_member_list=company.team_members.all()
    notification.notified_to.add(*list(team_member_list))
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)