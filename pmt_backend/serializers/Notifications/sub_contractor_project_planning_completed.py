from pmt_backend.models import Notification, User


def sub_contractor_project_planning_completed(project):
    company = project.company
    description = "Subcontractor Project Planning Completed for the project: {}".format(project.name)
    title = 'Subcontractor Project Planning Completed.'
    notification = Notification.objects.create(
        project=project, company=company, description=description, title=title, notification_type='gantt_view'
    )
    team_member_list=project.teammember_set.all()
    notification.notified_to.add(*list(team_member_list))
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)