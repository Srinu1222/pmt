from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Notification, ProjectRight


def request_for_access(requested_by, request_type, event):
    project = event.project
    company = project.company
    stage_name = event.stage.capitalize()
    description = "{} has requested {} for {}.".format(requested_by, request_type, stage_name)
    title = 'Request for Access'
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='pop_up'
    )
    project = event.project

    project_right = ProjectRight.objects.filter(project-project, roles__contains=['Project Manager']).first()
    if project_right:
        member = project_right.team_member
        notification.notified_to.add(member)
        notification.save()
    else:
        send_fail_http_response('No project manager for this project')


    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)