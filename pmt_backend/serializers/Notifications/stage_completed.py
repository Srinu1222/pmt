from pmt_backend.models import Notification, TeamMember, User


def stage_completed(user, event):
    member = TeamMember.objects.get(user=user).name
    project = event.project
    company = project.company
    title = 'Stage Completed'
    description = "{} has completed {} Stage".format(member, event.stage)
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='gantt_view'
    )
    team_member_list=project.teammember_set.all()
    notification.notified_to.add(*list(team_member_list))
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)