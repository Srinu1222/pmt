from pmt_backend.models import Notification, User


def new_team_member_added_to_project(project, member):
    company = project.company
    description = member.name + " added to a " + project.name
    title = 'New Team Member Added to Project'
    notification = Notification.objects.create(
        project=project, description=description, company=company, title=title, notification_type='team_page'
    )
    team_member_list=project.teammember_set.all()
    notification.notified_to.add(*list(team_member_list))
    notification.save()


    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)