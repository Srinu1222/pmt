from pmt_backend.models import Notification, ProjectRight, User
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer


def task_behind_schedule(task_name, event):
    project = event.project
    company = project.company
    title = 'Task Behind Schedule'
    description = "{} in {} is running behind schedule.".format(task_name, event.name)
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='detailed_event'
    )

    project = event.project
    team_members = project.teammember_set.all()
    team_member_list = []
    for member in team_members:
        project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
        can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
        if can_view or can_work or can_edit or can_approve:
            team_member_list.append(member)

    notification.notified_to.add(*team_member_list)
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)