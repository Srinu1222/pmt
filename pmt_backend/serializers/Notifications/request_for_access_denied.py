from pmt_backend.models import Notification


def request_for_access_denied(approved_by, requested_by, event):
    project = event.project
    company = project.company
    description = "{} has approved denied access made by {}".format(approved_by.name, requested_by)
    title = 'Request for Access Denied.'
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='non_clickable'
    )
    notification.notified_to.add(approved_by)
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)