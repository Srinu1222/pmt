from pmt_backend.models import Notification, TeamMember, User


def new_project_created(user, project):
    member = TeamMember.objects.get(user=user).name
    company = project.company
    description = "{} has created a new project called {}.".format(member, project.name)
    title = 'New Project Created'
    notification = Notification.objects.create(
        project=project, company=company, description=description, title=title, notification_type='project_gantt'
    )
    team_member_list=company.team_members.all()
    notification.notified_to.add(*list(team_member_list))
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)