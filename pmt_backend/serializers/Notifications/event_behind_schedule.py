from datetime import datetime

from pmt_backend.models import Notification, ProjectRight, User
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer

from celery import Celery
from celery.schedules import crontab
from pmt_backend.models import Project


app = Celery()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):

    active_projects = Project.objects.filter(status='ACTIVE')

    for project in active_projects:
        project_events = project.event_set.all()
        for event in project_events:
            # Executes every evening at 8:00 a.m.
            if event.completed_time < datetime.now() and event.actual_completed_time is None:
                sender.add_periodic_task(
                    crontab(minute=0, hour=8),
                    event_behind_schedule.s(event),
                )


@app.task
def event_behind_schedule(event):
    project = event.project
    company = project.company
    title = 'Event Behind Schedule'
    description = "{} is running behind schedule".format(event.name)
    notification = Notification.objects.create(
        event=event, company=company, description=description, title=title, notification_type='detailed_event'
    )

    project = event.project
    team_members = project.teammember_set.all()
    team_member_list = []
    for member in team_members:
        project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
        can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
        if can_view or can_work or can_edit or can_approve:
            team_member_list.append(member)

    notification.notified_to.add(*team_member_list)
    notification.save()

    # PUSH NOTIFICATION
    from pmt_backend.utils.push_notifications import send_push_notification
    users = User.objects.filter(teammember__in=team_member_list)
    send_push_notification(title, description, users)