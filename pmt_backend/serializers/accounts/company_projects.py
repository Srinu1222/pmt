from pmt_backend.utils.formatting_time import formating_date_time


def company_projects_serializer(company_projects):
    serialized_projects_list = []
    for project in company_projects:
        serialized_projects_list.append(
            {
                'id': project.id,
                'project_name': project.name,
                'size': project.size,
                'business_model': project.business_model,
                'location': project.location,
                'cod': formating_date_time(project.cod) if project.cod else '',
                'modules_used': project.modules_used,
                'inverters_used': project.inverters_used,
                '1st_year_KW_Generation': project.first_year_kw_generation
            }
        )

    return serialized_projects_list
