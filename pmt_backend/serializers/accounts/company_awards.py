from pmt_backend.utils.formatting_time import formating_date_time


def company_awards_serializer(company_awards):
    serialized_awards_list = []
    for award in company_awards:
        serialized_awards_list.append(
            {
                'id': award.id,
                'title': award.title,
                'date': formating_date_time(award.date),
                'image': award.image.url if award.image else None,
            }
        )

    return serialized_awards_list

