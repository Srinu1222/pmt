def company_testimonials(company_testimonals):
    serialized_testimonals_list = []

    for testimonal in company_testimonals:
        serialized_testimonals_list.append(
            {
                'id': testimonal.id,
                'description': testimonal.description,
                'person': testimonal.person,
                'rating': testimonal.rating
            }
        )

    return serialized_testimonals_list
