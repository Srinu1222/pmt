def company_key_personals_serializer(company_key_personnels):
    serialized_key_personnels_list = []
    for key in company_key_personnels:
        serialized_key_personnels_list.append(
            {
                'id': key.id,
                'name': key.name,
                'designation': key.designation,
                'qualification': key.qualification,
                'email': key.email,
                'phone_number': key.phone_number,
                'image': key.image.url,
            }
        )

    return serialized_key_personnels_list
