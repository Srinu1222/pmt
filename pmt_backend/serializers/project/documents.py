from pmt_backend.models import Databank, Customer
from pmt_backend.utils.formatting_time import formating_date_time
from django.db.models import F


def get_document_serializer(project, is_image, user):
    # Handling DOCS visible for customer login
    customer = Customer.objects.filter(user=user).first()

    project_databank = Databank.objects.filter(project=project).first()

    if not is_image:
        if customer:
            project_docs = project_databank.document_set.filter(is_image=is_image, event__stage='ENGINEERING')
        else:
            project_docs = project_databank.document_set.filter(is_image=is_image)

        Documents_list = []
        for doc in project_docs:
            Documents_list.append({
                "doc_id": doc.pk,
                "doc_name": doc.title,
                "stage": doc.event.stage if doc.event else project.stage_reached,
                "owner": doc.event.default_spoc.name if doc.event else project.event_set.all().first().default_spoc.name,
                "date": formating_date_time(doc.upload_date),
                "last_modified_date": formating_date_time(doc.modified_date),
                "file": doc.file.url if doc.file else None,
                'project_name': project.name,
                "description": doc.description,
                'event_id': doc.event.id if doc.event else None,
                'event_name': doc.event.name if doc.event else None,
            })

        serialized_Document_data = {
            'project_docs': Documents_list,
        }
        return serialized_Document_data
    else:
        project_images = project_databank.document_set.filter(is_image=is_image)

        stage_wise_images_dict = {}
        for image in project_images:
            stage = image.event.stage if image.event else project.stage_reached
            values_dict = {
                "doc_id": image.pk,
                "doc_name": image.title,
                "stage": stage,
                "owner": image.event.default_spoc.name if image.event else project.event_set.all().first().default_spoc.name,
                "date": formating_date_time(image.upload_date),
                "last_modified_date": formating_date_time(image.modified_date),
                "file": image.file.url if image.file else None,
                'project_name': project.name,
                "description": image.description,
                'event_id': image.event.id if image.event else None,
                'event_name': image.event.name if image.event else None,
            }

            if stage_wise_images_dict.get(stage):
                stage_wise_images_dict[stage].append(values_dict)
            else:
                stage_wise_images_dict[stage] = [values_dict]

        return stage_wise_images_dict


        # project_docs = project_databank.document_set.filter(is_image=is_image).values(
        #     'description', 'file',
        #     doc_name=F('title'), stage=F('event__stage'), owner=F('event__default_spoc__name'),
        #     date=formating_date_time(F('upload_date')), last_modified_date=F('modified_date'),
        #     project_name=F('event__project__name')
        # )
