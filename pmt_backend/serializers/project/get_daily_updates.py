from datetime import datetime

from pmt_backend.models import DailyUpdate, TeamMember


def get_daily_updates_serializer(project):
    if not project:
        return None

    team_members = TeamMember.objects.filter(project=project)

    filled_by = []
    not_filled_by = []

    for member in team_members:
        update = DailyUpdate.objects.filter(day=datetime.now(), member=member).first()

        if update:
            filled_by.append(member.name)
        else:
            not_filled_by.append(member.name)

    serialized_daily_update = {
        "day": datetime.now(),
        "filled_by": filled_by,
        "not_filled_by": not_filled_by
    }

    return serialized_daily_update
