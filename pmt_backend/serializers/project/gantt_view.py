from pmt_backend.models import Event
from pmt_backend.utils.formatting_time import formating_date_time


def get_stage_data(project, stage, stage_schedule_date):

    events_queryset = Event.objects.filter(project=project, stage=stage)

    stage_checklists_finished_count = 0
    stage_checklists_total_count = 0

    for event in events_queryset:
        events_checklists = event.checklists
        events_checklists_finished_count = 0
        event_checklists_total_count = 0
        if event.status == 'FINISHED':
            event_checklists_total_count += len(events_checklists)
            events_checklists_finished_count += len(events_checklists)
        else:
            for checklist in events_checklists:
                event_checklists_total_count += 1
                if checklist['status']:
                    events_checklists_finished_count += 1

        stage_checklists_finished_count += events_checklists_finished_count
        stage_checklists_total_count += event_checklists_total_count

    try:
        percentage = (stage_checklists_finished_count / stage_checklists_total_count) * 100
    except ZeroDivisionError:
        percentage = 0.0


    return {
        'stage': stage,
        'stage_schedule_date': formating_date_time(stage_schedule_date),
        'percentage': percentage
    }


def gantt_view_serializer(project):
    if not project:
        return None

    stages = ['PRE-REQUISITES', 'APPROVALS', 'ENGINEERING', 'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION',
              'SITE HAND OVER']

    is_default = True
    final_stages_list = []
    for stage in stages:
        start_event = Event.objects.filter(project=project, stage=stage).order_by('event_order_no').first()
        if is_default:
            default_event = start_event
            is_default = False
        stage_schedule_date = start_event.start_time
        final_stages_list.append(
            get_stage_data(project, stage, stage_schedule_date)
        )

    serialized_gantt_view = {
        'pre_requisites': final_stages_list[0],
        'approvals': final_stages_list[1],
        'engineering': final_stages_list[2],
        'procurement': final_stages_list[3],
        'material_handling': final_stages_list[4],
        'construction': final_stages_list[5],
        'site_handover': final_stages_list[6],
        'default_event_id': default_event.pk,
        'default_event_name': default_event.name
    }

    return serialized_gantt_view
