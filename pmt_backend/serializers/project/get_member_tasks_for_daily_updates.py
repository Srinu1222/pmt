from datetime import datetime, timedelta

from pmt_backend.models import Event
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware
from pmt_backend.utils.formatting_time import convert_string_to_datetime


def get_member_tasks_for_daily_updates(is_today, project, member, sub_contractor_member):
    today_date = convert_naive_to_aware(datetime.now())

    if is_today == 'true':
        if sub_contractor_member:
            event_queryset = Event.objects.filter(sub_contractor_spoc=sub_contractor_member, project=project,
                                                  start_time__lte=today_date).exclude(
                status='FINISHED')
            # event_which_is_just_completed_today
            event = Event.objects.filter(sub_contractor_spoc=sub_contractor_member, project=project,
                                         start_time__lte=today_date, status='FINISHED')
            event_queryset = list(event) + list(event_queryset)
        else:
            event_queryset = Event.objects.filter(project=project, default_spoc=member,
                                                  start_time__lte=today_date).exclude(
                status='FINISHED')

            # event_which_is_just_completed_today
            event = Event.objects.filter(project=project, default_spoc=member,
                                         start_time__lte=today_date, status='FINISHED')
            event_queryset = list(event) + list(event_queryset)
    else:
        if sub_contractor_member:
            event_queryset = Event.objects.filter(sub_contractor_spoc=sub_contractor_member, project=project,
                                                  start_time__lte=today_date).exclude(
                status='FINISHED')
        else:
            event_queryset = Event.objects.filter(default_spoc=member, project=project,
                                                  start_time__lte=today_date + timedelta(days=1)).exclude(
                status='FINISHED')

    tasks = []
    for event in event_queryset:
        checklists = event.checklists
        for checklist in checklists:
            # To check Checklist filled or not
            specifications = checklist.get('specifications', [])
            if len(specifications) == 0:
                is_specification_filled = False
            else:
                is_specification_filled = True

            if checklist.get('completed_time') and is_today:
                if checklist['status'] is True and convert_string_to_datetime(
                        checklist['completed_time']).day == datetime.now().day:
                    tasks.append(
                        {
                            'specification_filled': is_specification_filled,
                            'status': True,
                            'checklist_id': checklist['id'],
                            'title': checklist['title'],
                            'event_id': event.pk,
                            'event_name': event.name,
                            'stage_name': event.stage
                        }
                    )

            if checklist['status'] is False and int(checklist['spoc']) == int(member.id):
                tasks.append(
                    {
                        'specification_filled': is_specification_filled,
                        'status': False,
                        'checklist_id': checklist['id'],
                        'title': checklist['title'],
                        'event_id': event.pk,
                        'event_name': event.name,
                        'stage_name': event.stage
                    }
                )
    return tasks
