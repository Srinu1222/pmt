import math
import os

from pmt_backend.models import Event, Project
from django.core.files import File
import xlsxwriter


def generate_details_gantt(project):

    workbook = xlsxwriter.Workbook('details_gantt.xlsx', {'remove_timezone': True})
    worksheet = workbook.add_worksheet()

    # Adjust the column width.
    worksheet.set_column(0, 0, 25)
    worksheet.set_column(1, 1, 30)
    worksheet.set_column(2, 2, 20)
    worksheet.set_column(3, 17, 10)

    def merge_format(fg_color):
        return workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 10,
            'border': 0,
            'fg_color': fg_color})

    vertical_merge_format = workbook.add_format({
        'align': 'center',
        'valign': 'vcenter',
        'font_size': 20,
        'border': 1,
        'fg_color': '#E2EFD9'})

    heading_format = workbook.add_format({
        'align': 'center',
        'valign': 'vcenter',
        'font_size': 10,
        'border': 1,
        'fg_color': '#D9E2F3'})

    field_format = workbook.add_format()
    field_format.set_align('left')
    field_format.set_valign('vcenter')

    status_color = {
        'PENDING': '#B4C6E7',
        'IN-PROGRESS': '#FFE498',
        'FINISHED': '#A8D08D',
        'DELAYED': '#F4B083',
    }

    first_row = 0
    worksheet.write(first_row, 0, 'Stage', heading_format)
    worksheet.write(first_row, 1, 'Event', heading_format)
    worksheet.write(first_row, 2, 'SPOC Name', heading_format)
    for i in range(1, 17):
        worksheet.write(first_row, i + 2, 'Week' + str(i), heading_format)

    STAGES = (
        ('PRE-REQUISITES', 'Pre-Requisite'),
        ('APPROVALS', 'Approvals'),
        ('ENGINEERING', 'Engineering'),
        ('PROCUREMENT', 'Procurement'),
        ('MATERIAL HANDLING', 'Material-Handling'),
        ('CONSTRUCTION', 'Construction'),
        ('SITE HAND OVER', 'Hand-Over'),
    )

    start_row = 1
    for index, stage in enumerate(STAGES):
        event_queryset = list(Event.objects.filter(stage=stage[0], project=project).order_by('event_order_no'))
        end_row = start_row + len(event_queryset) - 1
        worksheet.merge_range(start_row, 0, end_row, 0, stage[1], vertical_merge_format)

        for index, i in enumerate(event_queryset):
            status = i.status
            worksheet.write(start_row + index, 1, i.name, field_format)
            worksheet.write(start_row + index, 2, i.default_spoc.name, field_format)

            days_required_to_complete_the_event = (i.completed_time - i.start_time).days
            weeks_required_to_complete_the_event = math.ceil(days_required_to_complete_the_event / 7)

            start_week_of_event = 3 + math.ceil((i.start_time - project.project_start_date).days / 7)

            worksheet.merge_range(
                start_row + index, start_week_of_event, start_row + index,
                start_week_of_event + weeks_required_to_complete_the_event,
                status.capitalize(),
                merge_format(status_color[status])
            )

        start_row = end_row + 2

    workbook.close()

    project.details_gantt_report = File(open('details_gantt.xlsx', "rb"))
    project.save()

    os.remove('details_gantt.xlsx')
