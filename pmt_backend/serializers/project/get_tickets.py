from pmt_backend.models import TeamMember
from pmt_backend.utils.formatting_time import formating_date_time, formating_time


def get_serialized_tickets(event):
    ticket_queryset = event.ticket_set.all()

    serialized_ticket_data = [
        {
            "id": ticket.id,
            "subject": ticket.subject,
            "comment": ticket.comment,
            "added_date": formating_date_time(ticket.added_date),
            "status": ticket.status,
            'stage': event.stage,
            'assigned_to': ticket.assigned_to.name,
            'date': formating_date_time(ticket.added_date),
            'time': formating_time(ticket.added_date),
            'raised_by': TeamMember.objects.get(user=ticket.raised_by).name
        }
        for ticket in ticket_queryset
    ]

    return serialized_ticket_data
