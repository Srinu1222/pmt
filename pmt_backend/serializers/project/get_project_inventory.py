from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Inventory, TeamMember
from pmt_backend.utils.reading_units_module import all_components_unit_dict


def get_inventory_serializer(project):
    inventory_queryset = Inventory.objects.filter(project=project).order_by('project_id', 'pk')
    inventory_list_in_stock = []
    inventory_list_consumed = []
    inventory_list_wastage = []
    category_list = []
    status_list = []

    for inventory in inventory_queryset:
        component_name = inventory.material_name
        # To get Units
        component_units_dict = all_components_unit_dict.get(component_name)
        if component_units_dict:
            format_dict = component_units_dict.get(inventory.quantity_format, component_units_dict['format1'])
            if format_dict:
                quantity_units = format_dict['unit_format']
            else:
                return send_fail_http_response('This Inventory Format is not Entered.'), False
        else:
            return send_fail_http_response('Component Name ({}) is Not Matching'.format(component_name)), False

        inventory_id = inventory.pk
        project_id = project.pk
        project_name = project.name
        category = inventory.material_name

        quantity_in_stock = inventory.stock_quantity
        used_quantity = inventory.used_quantity

        # total_quantity = inventory.stock_quantity
        # used_quantity = inventory.used_quantity
        # if total_quantity >= used_quantity:
        #     quantity_in_stock = total_quantity - used_quantity
        # else:
        #     quantity_in_stock = total_quantity

        rate = inventory.target_price  # quantity is directly proportional to rate

        value = quantity_in_stock * rate
        view_po = inventory.upload_po_file.url if inventory.upload_po_file else None
        location = project.location
        status = inventory.status
        product = inventory.preferred_brands

        # filtering purpose
        if category not in category_list:
            category_list.append(category)
        if status not in status_list:
            status_list.append(status)

        inventory_list_in_stock.append(
            {
                "inventory_id": inventory_id,
                "project_id": project_id,
                "project_name": project_name,
                "category": category,
                'product': product,
                "quantity": str(quantity_in_stock) + str(quantity_units),
                "rate": rate,
                "value": value,
                "view_po": view_po,
                "location": location,
                "status": status,
                'specification': inventory.specifications,
            }
        )

        value = used_quantity * rate

        if used_quantity > 0:
            inventory_list_consumed.append(
                {
                    "inventory_id": inventory_id,
                    "project_id": project_id,
                    "project_name": project_name,
                    "category": category,
                    'product': product,
                    "quantity": str(used_quantity) + str(quantity_units),
                    "rate": rate,
                    "value": value,
                    "view_po": view_po,
                    "location": location,
                    "status": inventory.status,
                    "data": inventory.target_delivery_date,
                    'specification': inventory.specifications,
                }
            )

        if inventory.is_wasted:
            value = inventory.quantity_wasted * rate
            member = TeamMember.objects.get(pk=inventory.spoc.id)

            inventory_list_wastage.append(
                {
                    "inventory_id": inventory_id,
                    "project_id": project_id,
                    "project_name": project_name,
                    "category": category,
                    "view_po": view_po,
                    "rate": rate,
                    "quantity": str(inventory.quantity_wasted) + str(quantity_units),
                    "value": value,
                    "reason": inventory.reason,
                    "spoc": {'name': member.name, 'id': member.pk},
                    "status": inventory.status,
                    'specification': inventory.specifications,
                    'product': product
                }
            )

    serialized_inventory_details = {
        'stock': inventory_list_in_stock,
        'consumed': inventory_list_consumed,
        'wastage': inventory_list_wastage,
        'category_list': category_list,
        'status_list': status_list,
    }

    return serialized_inventory_details, True


    # project_inventory_list = []
    # for inventory in inventory_queryset:
    #
    #     # To get Units
    #     component_units_dict = all_components_unit_dict.get(inventory.material_name)
    #     if component_units_dict:
    #         format_dict = component_units_dict.get(inventory.quantity_format, component_units_dict['format1'])
    #         if format_dict:
    #             quantity_units = format_dict['unit_format']
    #         else:
    #             return send_fail_http_response('This Inventory Format is not Entered.'), False
    #     else:
    #         return send_fail_http_response('Component Name is Not Matching'), False
    #
    #     stock_quantity = inventory.stock_quantity
    #     used_quantity = inventory.used_quantity
    #     quantity_in_stock = stock_quantity - used_quantity
    #
    #     rate = inventory.target_price
    #     value = quantity_in_stock * rate
    #     view_po = inventory.upload_po_file.url if inventory.upload_po_file else None
    #     location = project.location
    #
    #     project_inventory_list.append(
    #         {
    #             "inventory_id": inventory.id,
    #             "project_id": project.id,
    #             "project_name": project.name,
    #             "category": inventory.material_name,
    #             'product': inventory.preferred_brands,
    #             "quantity": str(quantity_in_stock)+str(quantity_units),
    #             "rate": rate,
    #             "value": value,
    #             "view_po": view_po,
    #             "location": location,
    #             'specification': inventory.specifications,
    #             "status": inventory.status,
    #         }
    #     )
    #
    # return project_inventory_list, True
