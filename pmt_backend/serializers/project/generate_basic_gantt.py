import math
import os

from pmt_backend.models import Event
from django.core.files import File
import xlsxwriter


def generate_basic_gantt(project):

    workbook = xlsxwriter.Workbook('basic_gantt.xlsx', {'remove_timezone': True})
    worksheet = workbook.add_worksheet()

    # Adjust the column width.
    worksheet.set_column(0, 0, 25)
    worksheet.set_column(1, 1, 17)
    worksheet.set_column(2, 16, 10)

    def merge_format(fg_color):
        return workbook.add_format({
                        'align': 'center',
                        'valign': 'vcenter',
                        'font_size': 10,
                        'border': 0,
                        'fg_color': fg_color})

    heading_format = workbook.add_format({
        'align': 'center',
        'valign': 'vcenter',
        'font_size': 10,
        'border': 1,
        'fg_color': '#D9E2F3'})

    field_format = workbook.add_format()
    field_format.set_align('center')
    field_format.set_valign('vcenter')

    first_row = 0
    worksheet.write(first_row, 0, 'Stage', heading_format)
    worksheet.write(first_row, 1, 'SPOC Name', heading_format)
    for i in range(1, 17):
        worksheet.write(first_row, i+1, 'Week'+str(i), heading_format)

    STAGES = (
        ('PRE-REQUISITES', 'Pre-Requisite'),
        ('APPROVALS', 'Approvals'),
        ('ENGINEERING', 'Engineering'),
        ('PROCUREMENT', 'Procurement'),
        ('MATERIAL HANDLING', 'Material-Handling'),
        ('CONSTRUCTION', 'Construction'),
        ('SITE HAND OVER', 'Hand-Over'),
    )

    status_color = {
        'PENDING': '#B4C6E7',
        'IN-PROGRESS': '#FFE498',
        'FINISHED': '#A8D08D',
        'DELAYED': '#F4B083',
    }

    for row in range(1, 8):
        stage = STAGES[row-1][0]
        events_list = list(Event.objects.filter(stage=stage, project=project).order_by('event_order_no'))
        first_event = events_list[0]
        last_event = events_list[-1]
        spoc = first_event.default_spoc.name

        status=first_event.stage_status
        days_required_to_complete_the_stage = (last_event.completed_time-first_event.start_time).days
        weeks_required_to_complete_the_stage = math.ceil(days_required_to_complete_the_stage/7)

        worksheet.write(row, 0, STAGES[row-1][1], field_format)
        worksheet.write(row, 1, spoc, field_format)

        start_week_of_event = 2 + math.ceil((first_event.start_time - project.project_start_date).days / 7)

        worksheet.merge_range(
            row, start_week_of_event, row, start_week_of_event+weeks_required_to_complete_the_stage,
            status.capitalize(),
            merge_format(status_color[status])
        )

    workbook.close()

    project.basic_gantt_report = File(open('basic_gantt.xlsx', "rb"))
    project.save()

    os.remove('basic_gantt.xlsx')
