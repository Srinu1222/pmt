from pmt_backend.models import Event, DailyReport
from pmt_backend.utils.formatting_time import formating_date_time


def get_dashboard_serializer(project):
    if not project:
        return None

    project_stage = project.stage_reached
    project_events = Event.objects.filter(project=project, stage=project_stage)
    total_number_of_events = project_events.count()
    pending_events = project_events.filter(status='PENDING').count()
    in_progress_events = project_events.filter(status='IN-PROGRESS').count()
    uncompleted_events = pending_events+in_progress_events

    active_events = Event.objects.filter(
        project=project, status="IN-PROGRESS").order_by('-completed_time')
    closed_events = Event.objects.filter(
        project=project, status="FINISHED").order_by('-actual_completed_time')

    recent_active_events = [
        {
            "event_id": event.id,
            "event_name": event.name,
            "date": formating_date_time(event.completed_time),
        }
        for event in active_events
    ]

    recent_closed_events = [
        {
            "event_id": event.id,
            "event_name": event.name,
            "date": formating_date_time(event.actual_completed_time),
        }
        for event in closed_events
    ]

    if not project.customer:
        daily_reports_queryset = DailyReport.objects.filter(project=project).order_by('-date')
        daily_reports = []
        for report in daily_reports_queryset:
            daily_reports.append(
                {
                    'date': report.date,
                    'filled_by': report.filled_by,
                    'not_filled_by': report.not_filled_by,
                    'report': report.report.url if report.report else None
                }
            )

        serialized_dashboard = {
            'project_name': project.name,
            'project_stage': project_stage,
            'total_number_of_events': total_number_of_events,
            'uncompleted_events': uncompleted_events,
            'recent_active_events': recent_active_events,
            'recent_closed_events': recent_closed_events,
            'daily_reports': daily_reports,
            "project_image": project.image.url
        }
    else:
        serialized_dashboard = {
            'project_name': project.name,
            'project_stage': project_stage,
            'total_number_of_events': total_number_of_events,
            'uncompleted_events': uncompleted_events,
            'recent_active_events': recent_active_events,
            'recent_closed_events': recent_closed_events,
            "project_image": project.image.url
        }

    return serialized_dashboard
