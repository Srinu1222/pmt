from pmt_backend.models import Event


def serialized_comments(name, comments_list):
    serialized_comments_list = []
    for comment in comments_list:
        if comment['file']:
            attachment = True
        else:
            attachment = False

        serialized_comments_list.append(
            {
                'name': name,
                'id': comment['id'],
                'data': comment['data'],
                'date': comment['date'],
                'attachment': attachment
            }
        )
    return serialized_comments_list


def get_comments_serializer(name, event, pre_requisite_id=None, checklist_id=None):
    if not event:
        return None

    if pre_requisite_id:
        event_pre_requisites = event.Prerequisites

        for prequisite in event_pre_requisites:
            if str(prequisite['id']) == pre_requisite_id:
                comments_list = prequisite['comments']
                serialized_comments_list = serialized_comments(name, comments_list)

    elif checklist_id:
        event_checklists = event.checklists

        serialized_comments_list = []
        for checklist in event_checklists:
            if str(checklist['id']) == checklist_id:
                comments_list = checklist['comments']
                serialized_comments_list = serialized_comments(name, comments_list)

    else:
        serialized_comments_list = []

    serialized_comment_data = {
        "comments": serialized_comments_list
    }

    return serialized_comment_data
