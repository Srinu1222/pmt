from pmt_backend.models import ProjectRight
from pmt_backend.utils.project_rights import can_view, can_approve, can_work, can_edit


def create_project_right(roles, team_member, project):
    can_view_stages_list = []
    for role in roles:
        can_view_stages_list += can_view[role]

    can_work_stages_list = []
    for role in roles:
        can_work_stages_list += can_work[role]

    can_edit_stages_list = []
    for role in roles:
        can_edit_stages_list += can_edit[role]

    can_approve_stages_list = []
    for role in roles:
        can_approve_stages_list += can_approve[role]

    ProjectRight.objects.create(
        team_member=team_member,
        project=project,
        roles=roles,
        can_view=list(set(can_view_stages_list)),
        can_edit=list(set(can_edit_stages_list)),
        can_work=list(set(can_work_stages_list)),
        can_approve=list(set(can_approve_stages_list))
    )
