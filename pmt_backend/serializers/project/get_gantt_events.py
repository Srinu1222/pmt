from pmt_backend.models import Event


def gantt_events_serializer(project, stage_name, is_sub_contractor, user):
    if not project:
        return None

    gantt_view_stage_events = Event.objects.filter(
        project=project, stage=stage_name).order_by('stage', 'event_order_no')

    stage_events = []

    for event in gantt_view_stage_events:
        is_sub_contractor_event = False
        if is_sub_contractor and event.is_sub_contractor_event:
            company = event.sub_contractor_spoc.user.company
            if user.company == company:
                is_sub_contractor_event = True
        else:
            is_sub_contractor_event = event.is_sub_contractor_event
        stage_events.append({'id': event.id, 'name': event.name, 'status': event.status,
                             'is_sub_contractor_event': is_sub_contractor_event})

    serialized_gant_events_data = {
        'gantt_view_stage_events': stage_events,
    }

    return serialized_gant_events_data
