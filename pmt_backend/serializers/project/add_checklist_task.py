from datetime import datetime

from pmt_backend.utils.formatting_time import formating_date_time


def add_checklist_task_serializer(event, status, spoc, title, due_date):
    event_checklists = event.checklists
    id = len(event_checklists) + 1

    event_checklists.append(
        {
            "id": id,
            "title": title,
            "status": False,
            "spoc": spoc.id,
            "comments": [],
            "due_on": due_date
        }
    )

    event.save()

    return id
