from pmt_backend.models import Event, SubContractorTeamMember, TeamMember
from pmt_backend.utils.formatting_time import formating_date_time


def get_serialized_stage_events_data(user, project, stage):
    if not project:
        return None

    is_sub_contractor = True if SubContractorTeamMember.objects.filter(user=user).first() else False

    events_queryset = Event.objects.filter(project=project, stage=stage).order_by('stage', 'event_order_no')

    events_list = []

    for event in events_queryset:
        additional_team_members = []

        # Subcontractor event logic
        is_sub_contractor_event = False
        if event.is_sub_contractor_event:
            company = event.sub_contractor_spoc.user.company
            if user.company == company:
                is_sub_contractor_event = True

        if event.is_sub_contractor_event:
            team_member = event.sub_contractor_spoc
            name = team_member.user.company.name
        else:
            team_member = event.default_spoc
            name = team_member.name

        if event.additional_members:
            for member_id in event.additional_members:
                member = TeamMember.objects.filter(pk=member_id).first()
                if member:
                    additional_team_members.append({'name': member.name, 'id': member.pk})

        events_list.append(
            {
                'id': event.id,
                'name': event.name,
                'default_spoc': {'name': name, 'id': team_member.pk, 'is_sub_contractor': event.is_sub_contractor_event},
                'start_time': formating_date_time(event.start_time),
                'days_needed': event.days_needed,
                'is_sub_contractor': is_sub_contractor,
                'is_sub_contractor_event': is_sub_contractor_event,
                "access_provided": event.project.access_provided,
                'status': event.status,
                'additional_team_members': additional_team_members,
            }
        )

    serialized_stage_event_data = {
        'stage_events': events_list,
    }

    return serialized_stage_event_data
