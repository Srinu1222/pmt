import datetime

from pmt_backend.models import CommentFile


def fun(event, event_attributes, id, data, file):
    for attribute in event_attributes:
        if str(attribute['id']) == id:
            comments_list = attribute['comments']
            comment_id = len(comments_list) + 1
            comment_file = CommentFile.objects.create(
                event_id=event.id, prerequisite_id=id,
                comment_id=comment_id, file=file
            )
            if file:
                file = comment_file.file.url

            comments_list.append(
                {"id": comment_id, 'date': datetime.datetime.now().strftime("%H:%M:%p"), 'data': data, 'file': file}
            )

            break

    event.save()

    return comment_id


def add_comment_serializer(event, id, data, file, type=type):

    if not event:
        return None

    if type == 'PRE_REQUISITE':
        event_pre_requisites = event.Prerequisites
        comment_id = fun(event, event_pre_requisites, id, data, file)

    elif type == 'CHECKLIST':
        event_checklists = event.checklists
        comment_id = fun(event, event_checklists, id, data, file)

    serialized_response = {"comment_id": comment_id}
    return serialized_response
