def get_rights_serializer(project_right, event_stage):
    can_view = project_right.can_view
    can_work = project_right.can_work
    can_edit = project_right.can_edit
    can_approve = project_right.can_approve

    can_view = True if event_stage in can_view else False
    can_work = True if event_stage in can_work else False
    can_edit = True if event_stage in can_edit else False
    can_approve = True if event_stage in can_approve else False
    return can_view, can_work, can_edit, can_approve
