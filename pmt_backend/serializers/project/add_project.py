from pmt_backend.models import (
    Project, Customer, Databank, Document, ProjectSpecification, User, Company, PaymentInfo, StakeHolder
)
from datetime import datetime, timedelta


def create_project_serializer(user, project_dict, image):
    if not user:
        return None
    name = project_dict['name']
    size = project_dict['size']
    location = project_dict['location']
    image = image
    score = project_dict.get('score')
    date = project_dict.get('date')

    if date:
        start_date = datetime.strptime(date, "%d/%m/%Y") + timedelta(hours=5, minutes=30)
    else:
        start_date = datetime.now()

    project = Project.objects.create(
        company=user.company,
        name=name,
        size=size,
        location=location,
        image=image,
        score=score,
        project_start_date=start_date,
    )

    return project


def create_customer_serializer(project, customer_dict):
    if not project:
        return None
    is_customer_info = customer_dict['is_customer_info']
    if is_customer_info:
        property_type = customer_dict['property_type']
        customer_name = customer_dict['customer_name']
        company_name = customer_dict['company_name']
        primary_email = customer_dict['primary_email']
        phone_number = customer_dict['phone_number']
        emails = customer_dict.get('emails', [])

        user, is_new = User.objects.get_or_create(
            email=primary_email,
            phone_number=phone_number,
            username=primary_email,
            is_consumer=True
        )

        if is_new:
            company = Company.objects.create(name=company_name)
            PaymentInfo.objects.create(company=company)
            user.set_password(str(customer_name[:4]) + str(phone_number[:4]))
            user.company = company
            user.save()

            customer = Customer.objects.create(
                user=user,
                is_customer_info=is_customer_info,
                property_type=property_type,
                customer_name=customer_name,
                company_name=company_name,
                primary_email=primary_email,
                phone_number=phone_number,
                emails=emails,
            )
        else:
            customer = Customer.objects.get(user=user)

        project.customer = customer
        project.save()


def create_stakeholder_serializer(project, stakeholder_dict):
    if not project:
        return None
    is_stake_holder_info = stakeholder_dict['is_stake_holder_info']
    if is_stake_holder_info and stakeholder_dict['stakeholder_type'] == 'Investor':
        stakeholder_type = stakeholder_dict['stakeholder_type']
        property_type = stakeholder_dict['property_type']
        stakeholder_name = stakeholder_dict['stakeholder_name']
        company_name = stakeholder_dict['company_name']
        primary_email = stakeholder_dict['primary_email']
        phone_number = stakeholder_dict['phone_number']
        emails = stakeholder_dict.get('emails', [])

        user, is_new = User.objects.get_or_create(
            email=primary_email,
            phone_number=phone_number,
            username=primary_email,
            user_type=stakeholder_type
        )

        if is_new:
            company = Company.objects.create(name=company_name)
            PaymentInfo.objects.create(company=company)
            user.set_password(str(stakeholder_name[:4]) + str(phone_number[:4]))
            user.company = company
            user.save()

            stakeholder = StakeHolder.objects.create(
                user=user,
                stakeholder_type=stakeholder_type,
                property_type=property_type,
                name=stakeholder_name,
                company_name=company_name,
                primary_email=primary_email,
                phone_number=phone_number,
                emails=emails,
            )
        else:
            stakeholder = StakeHolder.objects.get(user=user)

        project.stakeholder = stakeholder
        project.save()


def create_ProjectSpecification_serializer(project, project_spec_dict):
    if not project:
        return None
    selected_project_types = project_spec_dict.get('selected_project_types', [])
    selected_drawings = project_spec_dict.get('selected_drawings', [])
    selected_accessories = project_spec_dict.get('selected_accessories', [])
    selected_components = project_spec_dict.get('selected_components', [])
    selected_area_of_installation = project_spec_dict.get('selected_area_of_installation', [])
    selected_termination = project_spec_dict.get('selected_termination', [])
    selected_inverter_to_ACDB = project_spec_dict.get('selected_inverter_to_ACDB', [])
    selected_ACDB_to_Transformer = project_spec_dict.get('selected_ACDB_to_Transformer', [])
    selected_Transformer_to_HT_Panel = project_spec_dict.get('selected_Transformer_to_HT_Panel', [])

    ProjectSpecification.objects.create(
        project=project,
        selected_project_types=selected_project_types,
        selected_drawings=selected_drawings,
        selected_accessories=selected_accessories,
        selected_components=selected_components,
        selected_area_of_installation=selected_area_of_installation,
        selected_termination=selected_termination,
        selected_inverter_to_ACDB=selected_inverter_to_ACDB,
        selected_ACDB_to_Transformer=selected_ACDB_to_Transformer,
        selected_Transformer_to_HT_Panel=selected_Transformer_to_HT_Panel
    )


def create_databank_serializer(project, request):
    if not project:
        return None

    purchase_order_file = request.FILES.get('purchase_order_file', None)
    purchase_order_file_description = request.POST.get('purchase_order_file_description', '')

    electricity_bill_file = request.FILES.get('electricity_bill_file', None)
    electricity_bill_file_description = request.POST.get('electricity_bill_file_description', '')

    autocad_file = request.FILES.get('autocad_file', None)
    autocad_file_description = request.POST.get('autocad_file_description', '')

    electrical_SLD_file = request.FILES.get('electrical_SLD_file', None)
    electrical_SLD_file_description = request.POST.get('electrical_SLD_file_description', '')

    bom_file = request.FILES.get('bom_file', None)
    bom_file_description = request.POST.get('bom_file_description', '')

    site_checklist_file = request.FILES.get('site_checklist_file', None)
    site_checklist_file_description = request.POST.get('site_checklist_file_description', '')

    escalation_matrix_file = request.FILES.get('escalation_matrix_file', None)
    escalation_matrix_file_description = request.POST.get('escalation_matrix_file_description', '')

    data_bank_list = []
    if purchase_order_file:
        data_bank_list.append({
            "title": 'Purchase Order',
            'file': purchase_order_file,
            "description": purchase_order_file_description
        })
    if electricity_bill_file:
        data_bank_list.append({
            "title": 'Electricity Bill',
            'file': electricity_bill_file,
            "description": electricity_bill_file_description
        })
    if autocad_file:
        data_bank_list.append({
            "title": 'AutoCad',
            'file': autocad_file,
            "description": autocad_file_description
        })
    if electrical_SLD_file:
        data_bank_list.append({
            "title": 'Electrical SLD',
            'file': electrical_SLD_file,
            "description": electrical_SLD_file_description
        })
    if bom_file:
        data_bank_list.append({
            "title": 'BOM',
            'file': bom_file,
            "description": bom_file_description
        })
    if site_checklist_file:
        data_bank_list.append({
            "title": 'Site Checklist',
            'file': site_checklist_file,
            "description": site_checklist_file_description
        })
    if escalation_matrix_file:
        data_bank_list.append({
            "title": 'Escalation Matrix',
            'file': escalation_matrix_file,
            "description": escalation_matrix_file_description
        })

    data_bank = Databank.objects.create(project=project)

    data_bank_objs_list = [
        Document(
            databank=data_bank,
            title=document['title'],
            file=document['file'],
            description=document['description']
        )
        for document in data_bank_list
    ]

    Document.objects.bulk_create(data_bank_objs_list, ignore_conflicts=True)
