from pmt_backend.models import Document, TeamMember, ProjectRight, SubContractorTeamMember, Customer, StakeHolder
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer
from pmt_backend.serializers.project.get_tickets import get_serialized_tickets
from pmt_backend.utils.formatting_time import formating_date_time


def get_serialized_event_data(user, event, is_sub_contractor):
    if not event:
        return None

    is_sub_contractor_event = False
    if is_sub_contractor:
        company = event.sub_contractor_spoc.user.company
        if user.company == company:
            is_sub_contractor_event = True
    else:
        is_sub_contractor_event = event.is_sub_contractor_event

    # Handling DOCS visible for customer login
    customer = Customer.objects.filter(user=user).first()
    if customer:
        event_docs = Document.objects.filter(event__stage='ENGINEERING', is_image=False)
    else:
        event_docs = Document.objects.filter(event=event, is_image=False)

    event_documents = [
        {
            'doc_id': document.pk,
            'name': document.title,
            'file': document.file.url,
            'description': document.description
        }
        for document in event_docs
    ]

    event_images = Document.objects.filter(event=event, is_image=True)
    event_images_list = [
        {
            'doc_id': image.pk,
            'name': image.title,
            'file': image.file.url,
            'description': image.description
        }
        for image in event_images
    ]

    start_date_time = event.start_time
    start_date = formating_date_time(start_date_time)

    end_date_time = event.completed_time
    end_date = formating_date_time(end_date_time)

    tickets = get_serialized_tickets(event)

    event_pre_requisites = event.Prerequisites
    for pre_requisite in event_pre_requisites:
        pre_requisite["comments"] = len(pre_requisite["comments"])
        member = TeamMember.objects.get(pk=pre_requisite['spoc'])
        pre_requisite['spoc'] = {'id': member.id, 'name': member.name}

    event_checklists = event.checklists
    for checklist in event_checklists:
        checklist["comments"] = len(checklist["comments"])
        status = checklist.get('specifications')
        checklist['empty_spec'] = True if status else False
        member = TeamMember.objects.get(pk=checklist['spoc'])
        checklist['spoc'] = {'id': member.id, 'name': member.name}

    # Handled customer and stakeholder
    customer = Customer.objects.filter(user=user).first()
    if customer:
        can_view, can_work, can_edit, can_approve = True, False, False, False
    else:
        stakeholder = StakeHolder.objects.filter(user=user).first()
        if stakeholder:
            can_view, can_work, can_edit, can_approve = True, False, False, False
        else:
            # Handled TeamMember and Sub-Contractor
            if not is_sub_contractor:
                member = TeamMember.objects.get(user=user)
                project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
                can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
                if event.additional_members:
                    if member.id in event.additional_members:
                        can_work = True
            else:
                sub_contractor_member = SubContractorTeamMember.objects.get(user=user)
                project_right = ProjectRight.objects.filter(sub_contractor_team_member=sub_contractor_member,
                                                            project=event.project).first()
                can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)

    team_member = event.default_spoc

    try:
        roles = project_right.roles if project_right else []
    except:
        roles = []

    event_dict = {
        'event_name': event.name,
        'status': event.status,
        'approval_status': event.approval_status,
        'is_requested': event.is_requested,
        'start_date': start_date,
        'end_date': end_date,
        'event_approval_date': formating_date_time(
            event.actual_completed_time) if event.actual_completed_time else 'Not yet Approved',
        'pre_requisites': event_pre_requisites,
        "spoc": event.default_spoc.name,
        "checklist": event_checklists,
        "approval": event.default_approver.name,
        'documents': event_documents,
        'photos': event_images_list,
        'tickets': tickets,
        'can_view': can_view,
        'can_work': can_work,
        'can_edit': can_edit,
        'can_approve': can_approve,
        'team_member_id': team_member.id,
        'role': roles,
        'upload_boq_file_status': event.project.upload_boq_file_status,
        'customer_approval_required': event.customer_approval_required,
        'is_sub_contractor_event': is_sub_contractor_event,
        'sub_contractor_spoc': event.sub_contractor_spoc.name if event.sub_contractor_spoc else '',
        'sub_contractor_approver': event.sub_contractor_approver.name if event.sub_contractor_approver else '',
        "access_provided": event.project.access_provided,
        'inventory': event.selected_names[0] if len(event.selected_names) > 0 else '',
    }

    serialized_event_data = {
        'event': event_dict
    }

    return serialized_event_data
