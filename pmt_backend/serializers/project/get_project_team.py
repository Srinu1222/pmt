from pmt_backend.models import Event, ProjectRight


def get_team_serializer(project):
    if not project:
        return None

    # Normal TeamMembers
    teammembers_queryset = project.teammember_set.all()

    TeamMembers_list = []
    for teammember in teammembers_queryset:
        active_projects_count = teammember.projects.filter(status='ACTIVE').count()

        events_list = list(
            Event.objects.values_list('name', flat=True).filter(default_spoc=teammember, status="IN-PROGRESS")
        )

        project_right = ProjectRight.objects.filter(team_member=teammember, project=project).first()

        additional_rights = project_right.additional_rights

        TeamMembers_list.append({
            "id": teammember.id,
            "name": teammember.name,
            "designation": teammember.user.designation,
            "role": project_right.roles,
            "additional_rights": additional_rights,
            "active_projects": active_projects_count,
            "currently_working_on": events_list
        })


    # SubContractor Teammembers
    sub_contractor_team_members_queryset = project.subcontractorteammember_set.all()

    sub_contractor_team_members_list = []
    for teammember in sub_contractor_team_members_queryset:
        active_projects_count = teammember.projects.filter(status='ACTIVE').count()

        events_list = list(
            Event.objects.values_list('name', flat=True).filter(sub_contractor_spoc=teammember, status="IN-PROGRESS")
        )

        project_right = ProjectRight.objects.filter(sub_contractor_team_member=teammember, project=project).first()

        additional_rights = project_right.additional_rights

        sub_contractor_team_members_list.append({
            "id": teammember.id,
            "name": teammember.name,
            "designation": teammember.user.designation,
            "role": project_right.roles,
            "additional_rights": additional_rights,
            "active_projects": active_projects_count,
            "currently_working_on": events_list
        })


    serialized_team_data = {
        'TeamMembers_list': TeamMembers_list,
        'sub_contractor_team_members_list': sub_contractor_team_members_list
    }
    return serialized_team_data
