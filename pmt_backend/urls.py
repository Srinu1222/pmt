from django.urls import path

# user
from pmt_backend.api.accounts.add_credits import AddCredits, verify_payment
from pmt_backend.api.accounts.change_user_status import ChangeUserStatus
from pmt_backend.api.accounts.delete_user import DeleteUser
from pmt_backend.api.accounts.get_stakeholder_details import GetStakeholderDetails
from pmt_backend.api.accounts.reset_password import ResetPassword
from pmt_backend.api.accounts.update_billing_info import UpdateBillingInfo
from pmt_backend.api.company.add_material import AddMaterial
from pmt_backend.api.company.add_members_to_a_company import AddTeamMembers
from pmt_backend.api.company.add_report_issue import AddReportIssue
from pmt_backend.api.accounts.add_secondary_email import AddSecondaryEmails
from pmt_backend.api.company.daily_tasks_list import GetDailyTasks
from pmt_backend.api.company.delete_inventory import DeleteInventory
from pmt_backend.api.company.delete_project import DeleteProject
from pmt_backend.api.company.inventory_assign import InventoryAssign
from pmt_backend.api.accounts.remove_secondary_email import RemoveSecondaryEmail
from pmt_backend.api.company.request_demo import AddRequestDemo
from pmt_backend.api.events.add_event_delay_info import AddEventDelayInfo
from pmt_backend.api.events.change_event_docs import ChangeEventDoc
from pmt_backend.api.events.delete_event_docs import DeleteEventDoc
from pmt_backend.api.events.get_event_delayed_info import GetEventDelayedInfo
from pmt_backend.api.gantt.delete_event import DeleteEvent
from pmt_backend.api.gantt.delete_task import DeleteTask
from pmt_backend.api.company.add_report_wastage import AddInventoryWastage
from pmt_backend.api.project.get_event_specifications import GetEventSpecification
from pmt_backend.api.project.get_gantt_chart_stage_wise_data import GetGanttChartDataForStages
from pmt_backend.api.project.get_stage_events_basic_details import GetStageEventsBasicDetails
from pmt_backend.api.project.inventory_reassign import InventoryReAssign
from pmt_backend.api.project.sub_contractor_project_planning_completion import SubContractorProjectPlanningCompletion
from pmt_backend.api.project.update_event_status import ChangeEventStatus
from pmt_backend.api.user.signup import verify_otp
from pmt_backend.api.user.update_department import UpdateDepartment

# accounts
from pmt_backend.api.accounts.get_billing import Billing
from pmt_backend.api.accounts.manage_subscriptions import ManageSubscription
from pmt_backend.api.accounts.delete_project import DeleteProject
from pmt_backend.api.accounts.password_change import PasswordChange
from pmt_backend.api.accounts.edit_project import EditProject
from pmt_backend.api.accounts.delete_awards import DeleteAward
from pmt_backend.api.accounts.delete_keypersonnel import DeleteKeyPersonnel
from pmt_backend.api.accounts.delete_testimonals import DeleteTestimonial
from pmt_backend.api.accounts.add_basic_info import AddBasicInfo
from pmt_backend.api.accounts.add_company_profile import AddProfile
from pmt_backend.api.accounts.get_company_profile import GetProfile
from pmt_backend.api.accounts.add_award import AddAward
from pmt_backend.api.accounts.add_keypersonnel import AddKeyPersonnel
from pmt_backend.api.accounts.add_testimonials import AddTestimonial

# Company-Level
from pmt_backend.api.company.add_sub_contractor import AddSubContractor
from pmt_backend.api.company.get_all_sub_contarcors import GetAllSubContractors
from pmt_backend.api.company.get_subcontractors import GetSubContractors
from pmt_backend.api.company.get_default_rights import GetDefaultProjectRights
from pmt_backend.api.company.get_notifications import GetNotifications
from pmt_backend.api.company.get_all_projects import GetAllProjects
from pmt_backend.api.company.get_dashboard import GetDashboard
from pmt_backend.api.company.get_company_projects_databank import GetCompanyProjectsDataBank
from pmt_backend.api.company.add_team import AddTeam
from pmt_backend.api.company.delete_team_member import DeleteMembers
from pmt_backend.api.company.get_teams import GetTeams
from pmt_backend.api.company.get_team_members import GetAllMembers
from pmt_backend.api.company.get_inventory import GetInventory

# Project Level
from pmt_backend.api.project.get_reports import GetReports
from pmt_backend.api.project.get_member_tasks_for_daily_update import GetMemberTasks
from pmt_backend.api.project.access_provided import AccessProvided
from pmt_backend.api.project.download_gantt import DownloadGanttChart
from pmt_backend.api.project.is_event_requested import IsEventRequested
from pmt_backend.api.project.add_project_material import AddProjectMaterial
from pmt_backend.api.project.add_member_to_project import AddMemberToProject
from pmt_backend.api.project.add_ticket_comment import UpdateTicketComment
from pmt_backend.api.project.get_gantt_chart_data import GetGanttChartData
from pmt_backend.api.project.event_approval_request import EventRequestApproval
from pmt_backend.api.project.create_daily_update import CreateDailyUpdate
from pmt_backend.api.project.get_daily_update_projects import GetDailyUpdateProject
from pmt_backend.api.project.get_spoc import GetSpoc
from pmt_backend.api.project.get_tickets import GetTicket
from pmt_backend.api.project.change_ticket_status import ChangeTicketStatus
from pmt_backend.api.project.create_new_right import CreateNewRight
from pmt_backend.api.project.add_project import CreateProject
from pmt_backend.api.project.project_completion import ChangeProjectCompletion
from pmt_backend.api.project.get_project_docs import GetProjectDocs
from pmt_backend.api.project.get_project_dashboard import GetProjectDashboard
from pmt_backend.api.project.get_project_team import GetProjectTeams
from pmt_backend.api.project.get_project_stages import GetProjectStages
from pmt_backend.api.project.gantt_view import GanttView
from pmt_backend.api.project.gantt_view_events import GanttViewEvents
from pmt_backend.api.project.get_stage_events import GetStageEvents
from pmt_backend.api.project.update_stage_events import ChangeStageEvent
from pmt_backend.api.project.delete_stage_event import DeleteStageEvent
from pmt_backend.api.project.get_event_data import GetEvent
from pmt_backend.api.project.add_comment import add_comment
from pmt_backend.api.project.delete_comment import DeleteComment
from pmt_backend.api.project.get_comments import GetComments
from pmt_backend.api.project.get_comment_file import GetCommentFile
from pmt_backend.api.project.create_ticket import CreateTicket
from pmt_backend.api.project.add_prerequiste_task import AddPreRequisiteTask
from pmt_backend.api.project.update_prerequisite_task import ChangePreRequisiteTask
from pmt_backend.api.project.add_checklist_task import AddChecklistTask
from pmt_backend.api.project.update_checklist_task import ChangeChecklistTask
from pmt_backend.api.project.delete_event_task import DeleteEventTask
from pmt_backend.api.project.get_project_inventory import GetProjectInventory
from pmt_backend.api.project.get_specification_events import GetSpecificationEvents

# checklist spec api's
from pmt_backend.api.checklists_specs.cable_calculations_edit_specifaction import CreateCableSpec
from pmt_backend.api.checklists_specs.client_approval_edit_specifaction import CreateClientApproval
from pmt_backend.api.checklists_specs.installation_of_walkways_edit_spec import CreateInstallationOfWalkways
from pmt_backend.api.checklists_specs.internal_design_review_edit_specification import CreateInternalReviewDesignSpec
from pmt_backend.api.checklists_specs.list_of_drawings_edit_specifications import CreateListOfDrawings
from pmt_backend.api.checklists_specs.pre_dispatch_inspection_edit_spec import CreatePreDispatchInspectionSpec
from pmt_backend.api.checklists_specs.procurement_final_spec_edit_specifications import CreateProcurementFinalSpec
from pmt_backend.api.checklists_specs.procurement_plan_edit_specifications import CreateProcurementPlan
from pmt_backend.api.checklists_specs.procurement_spec_edit_specifications import CreateProcurementSpec
from pmt_backend.api.checklists_specs.view_specifaction import GetChecklistSpecification
from pmt_backend.api.checklists_specs.view_spec_for_lod import GetChecklistSpecificationforlod
from pmt_backend.api.checklists_specs.uploaded_final_boq import UploadBoQFile
from pmt_backend.api.checklists_specs.get_drawings import GetDrawings

# Events
from pmt_backend.api.events.follow_up import FollowUpForTask
from pmt_backend.api.events.add_event_docs import AddEventDoc
from pmt_backend.api.events.get_event_docs import GetEventDocs
from pmt_backend.api.events.checklist_linked_to import ChecklistLinkedTo

# Gantt
from pmt_backend.api.gantt.update_remarks_in_gantt import UpdateRemarks
from pmt_backend.api.gantt.delete_gantt import DeleteGantt
from pmt_backend.api.gantt.get_sample_gantts import GetSampleGantt
from pmt_backend.api.gantt.get_gantt_events import GetGanttEvents
from pmt_backend.api.gantt.update_gantt_events import UpdateGanttEvent
from pmt_backend.api.gantt.get_gantt_data_specifications import GetGanttSpec
from pmt_backend.api.gantt.get_gantt_details_for_a_project import GetGanttDetailsForProject
from pmt_backend.api.gantt.update_project_spec import UpdateProjectSpec

# Finance
from pmt_backend.api.finance.calculate_p_and_l import calculate_p_and_l
from pmt_backend.api.finance.upload_finance_file import UploadFinanceFile
from pmt_backend.api.finance.get_actual import GetActual
from pmt_backend.api.finance.get_budgeted import GetBudgeted
from pmt_backend.api.finance.update_budjeted_side import UpdateBudgeted

# Customer_Events
from pmt_backend.api.customer_events.get_all_sample_events import GetAllSampleEvents
from pmt_backend.api.customer_events.create_new_event import AddNewEvent
from pmt_backend.api.customer_events.get_sample_event import GetSampleEvent
from pmt_backend.api.accounts.get_email_confirmation import send_email_confirmation

urlpatterns = [

    # User
    path("verify_otp/", verify_otp, name="verify_otp"),
    path("send_email_confirmation/", send_email_confirmation, name="send_email_confirmation"),
    path("update_department/", UpdateDepartment.as_view(), name="update_department"),

    # accounts
    path("add_secondary_email/", AddSecondaryEmails.as_view(), name="add_secondary_email"),
    path("remove_secondary_email/", RemoveSecondaryEmail.as_view(), name="remove_secondary_email"),
    path("get_stakeholder_details/", GetStakeholderDetails.as_view(), name="get_stakeholder_details"),
    path("get_company_profile/", GetProfile.as_view(), name="get_company_profile"),
    path("add_company_profile/", AddProfile.as_view(), name="add_company_profile"),
    path("add_award/", AddAward.as_view(), name="add_award"),
    path("change_user_status/", ChangeUserStatus.as_view(), name="change_user_status"),
    path("add_basic_info/", AddBasicInfo.as_view(), name="add_basic_info"),
    path("add_key_personnel/", AddKeyPersonnel.as_view(), name="add_key_personnel"),
    path("add_testimonial/", AddTestimonial.as_view(), name="add_testimonial"),
    path("delete_award/", DeleteAward.as_view(), name="delete_award"),
    path("delete_user/", DeleteUser.as_view(), name="delete_user"),
    path("reset_password/", ResetPassword.as_view(), name="reset_password"),
    path("delete_key_personnel/", DeleteKeyPersonnel.as_view(), name="delete_key_personnel"),
    path("delete_testimonial/", DeleteTestimonial.as_view(), name="delete_testimonial"),
    path("delete_project/", DeleteProject.as_view(), name="delete_project"),
    path("edit_project/", EditProject.as_view(), name="edit_project"),
    path("password_change/", PasswordChange.as_view(), name="password_change"),
    path("manage_subscription/", ManageSubscription.as_view(), name="manage_subscription"),
    path("get_billing/", Billing.as_view(), name="get_billing"),
    path("add_credit/", AddCredits.as_view(), name="add_credit"),
    path("update_billing_info/", UpdateBillingInfo.as_view(), name="update_billing_info"),
    path("verify_payment/", verify_payment, name="verify_payment"),

    # Company-Level
    path("add_company_material/", AddMaterial.as_view(), name="add_company_material"),
    path("report_issue/", AddReportIssue.as_view(), name="report_issue"),
    path("inventory_assign/", InventoryAssign.as_view(), name="inventory_assign"),
    path("add_report_wastage/", AddInventoryWastage.as_view(), name="add_report_wastage"),
    path("get_daily_tasks/", GetDailyTasks.as_view(), name="get_daily_tasks"),
    path("request_demo/", AddRequestDemo, name="request_demo"),
    path("add_sub_contractor/", AddSubContractor.as_view(), name="add_sub_contractor"),
    path("get_all_sub_contractors/", GetAllSubContractors.as_view(), name="get_all_sub_contractors"),
    path("get_company_sub_contractors/", GetSubContractors.as_view(), name="get_company_sub_contractors"),
    path("get_default_project_rights/", GetDefaultProjectRights.as_view(), name="get_default_project_rights"),
    path("get_projects/", GetDashboard.as_view(), name="get_projects"),
    path("get_all_projects/", GetAllProjects.as_view(), name="get_all_projects"),
    path("get_company_projects_databank/", GetCompanyProjectsDataBank.as_view(), name="get_company_projects_databank"),
    path("add_team/", AddTeam.as_view(), name="add_team"),
    path("add_team_members/", AddTeamMembers.as_view(), name="add_team_members"),
    path("delete/members/", DeleteMembers.as_view(), name="delete_member"),
    path("delete/inventories/", DeleteInventory.as_view(), name="delete_inventories"),
    path("delete/project/", DeleteProject.as_view(), name="delete_project"),
    path("get_teams/", GetTeams.as_view(), name="get_teams"),
    path("get_all_members/", GetAllMembers.as_view(), name="get_all_members"),
    path("get_inventory/", GetInventory.as_view(), name="get_inventory"),
    path("get_notifications/", GetNotifications.as_view(), name="get_notifications"),

    # Project-Level
    path("inventory_Reassign/", InventoryReAssign.as_view(), name="inventory_Reassign"),
    path("get_reports/", GetReports.as_view(), name="get_reports"),
    path("get_member_tasks_for_daily_updates/", GetMemberTasks.as_view(), name="get_member_tasks_for_daily_updates"),
    path("is_event_requested/", IsEventRequested.as_view(), name="is_event_requested"),
    path("event_approval/", EventRequestApproval.as_view(), name="event_approval"),
    path("sub_contractor_project_planning/", SubContractorProjectPlanningCompletion.as_view(),
         name="sub_contractor_project_planning"),
    path("access_provided/", AccessProvided.as_view(), name="access_provided"),
    path("download_gantt/", DownloadGanttChart.as_view(), name="download_gantt"),
    path("get_event_specifications/", GetEventSpecification.as_view(), name="get_event_specifications"),
    path("get_specification_events/", GetSpecificationEvents.as_view(), name="get_specification_events"),
    path("add_member_to_project/", AddMemberToProject.as_view(), name="add_member_to_project"),
    path("update_ticket_comment/", UpdateTicketComment.as_view(), name="update_ticket_comment"),
    path("get_gantt_data/", GetGanttChartData.as_view(), name="get_gantt_data_actual"),
    path("get_gantt_stages_data/", GetGanttChartDataForStages.as_view(), name="get_gantt_stages_data"),
    path("get_tickets/", GetTicket.as_view(), name="get_tickets"),
    path("change_ticket_status/", ChangeTicketStatus.as_view(), name="change_ticket_status"),
    path("get_spoc/", GetSpoc.as_view(), name="get_spoc"),
    path("create_right/", CreateNewRight.as_view(), name="create_right"),
    path("add_project/", CreateProject.as_view(), name="add_project"),
    path("get_project_dashboard/", GetProjectDashboard.as_view(), name="get_project_dashboard"),
    path("get_project_databank/", GetProjectDocs.as_view(), name='get_project_databank'),
    path("get_project_team/", GetProjectTeams.as_view(), name='get_project_team'),
    path("gantt_view/", GanttView.as_view(), name="gantt_view"),
    path("gantt_view_events/", GanttViewEvents.as_view(), name="gantt_view_events"),
    path("get_project_stages/", GetProjectStages.as_view(), name='get_project_stages'),
    path("get_stage_events/", GetStageEvents.as_view(), name='get_stage_events'),
    path("get_stage_events_basic_details/", GetStageEventsBasicDetails.as_view(), name='get_stage_events_basic_details'),
    path("change/event/<int:event_id>/", ChangeStageEvent.as_view(), name='change_event'),
    path("change_event_status/", ChangeEventStatus.as_view(), name='change_event_status'),
    path("delete/event/<int:event_id>", DeleteStageEvent.as_view(), name='delete_event'),
    path("get_event/<int:event_id>/", GetEvent.as_view(), name='get_event'),
    path("add_comment/", add_comment.as_view(), name="add_comment"),
    path("delete_comment/", DeleteComment.as_view(), name="delete_comment"),
    path("get_comments/", GetComments.as_view(), name="get_comments"),
    path("get_comment_file/", GetCommentFile.as_view(), name="get_comment_file"),
    path("add_ticket/", CreateTicket.as_view(), name="add_ticket"),
    path("add/prerequiste_task/", AddPreRequisiteTask.as_view(), name='add_prerequisite_task'),
    path("change/prerequiste_task/", ChangePreRequisiteTask.as_view(), name='change_prerequisite_task'),
    path("add/checklist_task/", AddChecklistTask.as_view(), name='add_checklist_task'),
    path("change/checklist_task/", ChangeChecklistTask.as_view(), name='change_checklist_task'),
    path("delete/event_task/", DeleteEventTask.as_view(), name='change_prerequisite_task'),
    path("get_project_inventory/", GetProjectInventory.as_view(), name="get_inventory"),
    path("change_project_completion/", ChangeProjectCompletion.as_view(), name="change_project_completion"),
    path("create_daily_update/", CreateDailyUpdate.as_view(), name="create_daily_update"),
    path("get_daily_update_projects/", GetDailyUpdateProject.as_view(), name="get_daily_update_projects"),
    path("add_project_material/", AddProjectMaterial.as_view(), name="add_project_material"),

    # checklist specs
    path("view_specifications/", GetChecklistSpecification.as_view(), name="view_specifications"),
    path("view_specifications_for_lod/", GetChecklistSpecificationforlod.as_view(), name="view_specifications_for_lod"),
    path("cable_calculation_edit_spec/", CreateCableSpec.as_view(), name="cable_calculation_edit_spec"),
    path("client_approval_edit_spec/", CreateClientApproval.as_view(), name="client_approval_edit_spec"),
    path("installation_of_walkways_edit_spec/", CreateInstallationOfWalkways.as_view(),
         name="installation_of_walkways"),
    path("internal_design_review_edit_spec/", CreateInternalReviewDesignSpec.as_view(), name="internal_design_review"),
    path("list_of_drawings_edit_spec/", CreateListOfDrawings.as_view(), name="list_of_drawings_edit_spec"),
    path("pre_dispatch_inspection_edit_spec/", CreatePreDispatchInspectionSpec.as_view(),
         name="pre_dispatch_inspection"),
    path("procurement_final_edit_spec/", CreateProcurementFinalSpec.as_view(), name="procurement_final_edit_spec"),
    path("procurement_plan_edit_spec/", CreateProcurementPlan.as_view(), name="procurement_plan_edit_spec"),
    path("procurement_spec_edit_spec/", CreateProcurementSpec.as_view(), name="procurement_spec_edit_spec"),
    path("upload_boq_file/", UploadBoQFile.as_view(), name="upload_boq_file"),
    path("list_of_drawings/", GetDrawings.as_view(), name="list_of_drawings"),

    # Events
    path("add_event_doc/", AddEventDoc.as_view(), name="add_event_doc"),
    path("add_event_delay_info/", AddEventDelayInfo.as_view(), name="add_event_delay_info"),
    path("delete_event_doc/", DeleteEventDoc.as_view(), name="delete_event_doc"),
    path("change_event_doc/", ChangeEventDoc.as_view(), name="change_event_doc"),
    path("get_event_docs/", GetEventDocs.as_view(), name="get_event_docs"),
    path("get_event_delayed_info/", GetEventDelayedInfo.as_view(), name="get_event_delayed"),
    path("checklist_linked_to/", ChecklistLinkedTo.as_view(), name="checklist_linked_to"),
    path("follow_up_for_task/", FollowUpForTask.as_view(), name="follow_up_for_task"),

    # Gantt
    path("delete_event/", DeleteEvent.as_view(), name="delete_event"),
    path("delete_task/", DeleteTask.as_view(), name="delete_task"),
    path("update_remarks/", UpdateRemarks.as_view(), name="update_remarks"),
    path("delete_gantt/", DeleteGantt.as_view(), name="delete_gantt"),
    path("get_sample_gantt/", GetSampleGantt.as_view(), name="get_sample_gantt"),
    path("get_gantt_spec/", GetGanttSpec.as_view(), name="get_gantt_spec"),
    path("get_gantt_events/", GetGanttEvents.as_view(), name="get_gantt_events"),
    path("update_gantt_events/", UpdateGanttEvent.as_view(), name="update_gantt_events"),
    path("update_project_spec/", UpdateProjectSpec.as_view(), name="update_project_spec"),
    path("get_gantt_details_for_a_project/", GetGanttDetailsForProject.as_view(),
         name="get_gantt_details_for_a_project"),

    # Finance
    path("calculate_p_and_l/", calculate_p_and_l.as_view(), name="calculate_p_and_l"),
    path("get_budgeted/", GetBudgeted.as_view(), name="get_budgeted"),
    path("get_actual/", GetActual.as_view(), name="get_actual"),
    path("upload_finance_file/", UploadFinanceFile.as_view(), name="upload_finance_file"),
    path("update_budgeted/", UpdateBudgeted.as_view(), name="update_budgeted"),

    # Customer Events
    path("add_new_event/", AddNewEvent.as_view(), name="add_new_event"),
    path("get_all_sample_events/", GetAllSampleEvents.as_view(), name="get_all_sample_events"),
    path("get_sample_event/", GetSampleEvent.as_view(), name="get_sample_event"),
]
