from datetime import datetime


def formating_date_time(date_time):
    return date_time.strftime("%d/%m/%Y")


def formating_time(date_time):
    return date_time.strftime("%H:%M%p")


def convert_string_to_datetime(date_string):
    return datetime.strptime(date_string, "%d/%m/%Y")

