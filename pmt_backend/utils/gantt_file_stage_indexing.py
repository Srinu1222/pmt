def gantt_file_stage_indexing(sheet):
    stage_indexes_dict = {}
    for index, row in enumerate(sheet):
        if index == 0:
            continue

        if row[0].value:
            stage = row[0].value
            stage_indexes_dict[stage] = []

        try:
            stage_indexes_dict[stage].append(index + 1)
        except:
            continue

    return stage_indexes_dict
