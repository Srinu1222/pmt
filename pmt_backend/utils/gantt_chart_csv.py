def writing_to_csv(events_list):

    import csv

    # field names
    fields = ['id', 'name', 'start_time', 'completed_time']

    # data rows of csv file
    rows = events_list

    # name of csv file
    filename = "generated_gantt_chart.csv"

    # writing to csv file
    with open(filename, 'w') as csvfile:
        # creating a csv writer object
        csvwriter = csv.writer(csvfile)

        # writing the fields
        csvwriter.writerow(fields)

        # writing the data rows
        csvwriter.writerows(rows)

