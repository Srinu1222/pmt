from openpyxl import load_workbook


def read_impact_file(impact, start, stop):
    workbook = load_workbook(filename=impact)
    sheet = workbook.active

    values_dict = {}
    for row in sheet[start:stop]:
        values_dict[row[0].value] = [row[1].value] if type(row[1].value) == int else list(
            map(int, row[1].value.split(',')))

    return values_dict
