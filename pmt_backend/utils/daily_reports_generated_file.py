import os
import xlsxwriter
from reportlab.platypus import Paragraph, Table, TableStyle, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate
from reportlab.lib.units import mm, inch
from reportlab.lib.enums import TA_CENTER, TA_LEFT
import io
from django.core.files import File

from pmt_backend.models import Event


def generate_daily_report_in_pdf(project_name, date, members_list, daily_report):
    stream = io.BytesIO()
    im = Image('https://safearth-static.s3.ap-south-1.amazonaws.com/logo.png.png', 2 * inch, 1 * inch)
    im.hAlign = 'LEFT'
    sp = ParagraphStyle('Title',
                        alignment=TA_CENTER,
                        fontSize=12,
                        fontName="Helvetica-Bold",
                        textColor="#5a79af",
                        leading=8
                        )
    p = ParagraphStyle('body',
                       alignment=TA_CENTER,
                       fontSize=10,
                       fontName="Helvetica",
                       leading=8,
                       )
    bp = ParagraphStyle('body',
                        alignment=TA_LEFT,
                        fontSize=12,
                        fontName="Helvetica",
                        leading=16,
                        leftIndent=8,
                        )

    flowables = []
    my_doc = SimpleDocTemplate(stream)
    flowables.append(im)
    flowables.append(Paragraph("Daily Report", sp))
    flowables.append(Spacer(0, 8 * mm))
    flowables.append(Paragraph("Project Name : " + project_name, bp))
    flowables.append(Spacer(0, 6 * mm))

    flowables.append(Paragraph("Date : " + date.strftime('%d/%m/%Y'), bp))
    flowables.append(Spacer(0, 8 * mm))

    for member in members_list:
        flowables.append(Paragraph("Name : " + member['name'], bp))
        flowables.append(Paragraph("Roles : " + ",".join(member['role']), bp))
        flowables.append(Spacer(0, 6 * mm))

        if member['completed']:
            flowables.append(Paragraph("Completed : ", bp))
            for index, item in enumerate(member['completed']):
                flowables.append(Spacer(0, 6 * mm))
                string = str(index+1) + ') ' + item['event_name'] + '->' + item['title']
                flowables.append(Paragraph(string, bp))
                flowables.append(Spacer(0, 8 * mm))

        if member['ongoing_tasks']:
            flowables.append(Paragraph("Ongoing Tasks: ", bp))
            for index, item in enumerate(member['ongoing_tasks']):
                flowables.append(Spacer(0, 6 * mm))
                string = str(index+1) + ') ' + item['event_name'] + '->' + item['title']
                flowables.append(Paragraph(string, bp))
                flowables.append(Spacer(0, 8 * mm))

        if member['planned_for_tomorrow']:
            flowables.append(Paragraph("Planned for Tomorrow: ", bp))
            for index, item in enumerate(member['planned_for_tomorrow']):
                flowables.append(Spacer(0, 6 * mm))
                string = str(index+1) + ') ' + item['event_name'] + '->' + item['title']
                flowables.append(Paragraph(string, bp))
                flowables.append(Spacer(0, 8 * mm))

        if member['today_msg_list']:
            flowables.append(Paragraph("Work done today: ", bp))
            for index, item in enumerate(member['today_msg_list']):
                flowables.append(Spacer(0, 6 * mm))
                string = str(index+1) + ') ' + item
                flowables.append(Paragraph(string, bp))
                flowables.append(Spacer(0, 8 * mm))

        if member['tomorrow_msg_list']:
            flowables.append(Paragraph("Work to be done tomorrow: ", bp))
            for index, item in enumerate(member['tomorrow_msg_list']):
                flowables.append(Spacer(0, 6 * mm))
                string = str(index+1) + ') ' + item
                flowables.append(Paragraph(string, bp))
                flowables.append(Spacer(0, 8 * mm))

    flowables.append(Spacer(0, 8 * mm))

    flowables.append(Paragraph('Daily Report for {} for {}.'.format(project_name, date), p))
    flowables.append(Spacer(0, 4 * mm))
    flowables.append(Paragraph('This is an Automatic Computer Generated Report.', p))

    my_doc.build(flowables)
    pdf_buffer = stream.getbuffer()
    file = open("daily_reports.pdf", "wb")
    file.write(pdf_buffer)
    file.close()
    daily_report.report = File(open("daily_reports.pdf", "rb"))
    daily_report.save()

    os.remove('daily_reports.pdf')
