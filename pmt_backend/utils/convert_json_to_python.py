import json


def json_to_python(json_string):
    return json.loads(json_string)
