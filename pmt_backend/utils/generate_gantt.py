from pmt_backend.models import Event
from pmt_backend.utils.formatting_time import formating_date_time
from pmt_backend.utils.gantt_chart_csv import writing_to_csv

import pyexcel as p
from django.core.files import File


def generate_gantt(project):
    events_queryset = Event.objects.filter(project=project).order_by('stage', 'event_order_no')

    events_list = []
    for event in events_queryset:
        events_list.append(
            [event.id, event.name, formating_date_time(event.start_time),
             formating_date_time(event.completed_time)]
        )

    writing_to_csv(events_list)
    p.embed = True
    p.save_as(file_name='generated_gantt_chart.csv',
              dest_file_name='generated_gantt_chart.html')

    # project.generated_gantt_file = File(open('generated_gantt_chart.html', "rb"))
    project.save()
