from openpyxl import load_workbook
workbook = load_workbook(filename="pmt_backend/utils/Responsibility_Matrix.xlsx")
sheet = workbook.active

can_view = {}
can_work = {}
can_approve = {}
can_edit = {}


def section(section_values):
    if section_values == "All":
        sections = ['PRE-REQUISITES', 'Specifications', 'ENGINEERING', 'APPROVALS',
                    'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER']
        return sections
    else:
        return section_values.split(',')


for row in sheet[3:18]:
    designation = row[0].value
    can_view[designation] = section(row[1].value)
    can_work[designation] = section(row[2].value)
    can_approve[designation] = section(row[3].value)
    can_edit[designation] = section(row[3].value)

