# Getting Dict in a jsonfield by giving event prerequisites or event checklists
def get_event_task_dict(event_types, id):
    for type_dict in event_types:
        if str(type_dict['id']) == str(id):
            return type_dict


# Getting Dict in a jsonfield by giving event prerequisites or event checklists
def get_event_task_index(event_types, id):
    for index, type_dict in enumerate(event_types):
        if str(type_dict['id']) == str(id):
            return index
