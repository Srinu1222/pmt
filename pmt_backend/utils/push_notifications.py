from firebase_admin.messaging import Message, Notification
from fcm_django.models import FCMDevice


def send_push_notification(title, body, users):
    devices = FCMDevice.objects.filter(user__in=users)
    print('push notifications....', title, body, users, devices)
    message = Message(notification=Notification(title=title, body=body))
    devices.send_message(message)
