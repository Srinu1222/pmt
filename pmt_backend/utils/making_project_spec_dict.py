from openpyxl import load_workbook


def making_project_spec_dict(gantt):
    workbook = load_workbook(filename=gantt.specifications)
    sheet = workbook.active

    selected_drawings = []
    for row in sheet[2:17]:
        if row[1].value == 'Yes':
            selected_drawings.append(row[0].value)

    selected_components = []
    for row in sheet[22:47]:
        if row[1].value == 'Yes':
            selected_components.append(row[0].value)

    selected_accessories = []
    for row in sheet[51:71]:
        if row[1].value == 'Yes':
            selected_accessories.append(row[0].value)

    selected_area_of_installation = []
    for row in sheet[104:106]:
        if row[1].value == 'Yes':
            selected_area_of_installation.append(row[0].value)

    selected_termination = []
    if sheet[74][1].value == 'Yes':
        selected_termination.append('LT')
    else:
        selected_termination.append('HT')

    selected_inverter_to_ACDB = []
    selected_ACDB_to_Transformer = []
    selected_Transformer_to_HT_Panel = []

    if selected_termination[0] == "LT":
        for row in sheet[76:78]:
            if row[1].value == 'Yes':
                selected_inverter_to_ACDB.append(row[0].value)

        for row in sheet[81:83]:
            if row[1].value == 'Yes':
                selected_ACDB_to_Transformer.append(row[0].value)

    elif selected_termination[0] == "HT":
        for row in sheet[88:90]:
            if row[1].value == 'Yes':
                selected_inverter_to_ACDB.append(row[0].value)

        for row in sheet[93:95]:
            if row[1].value == 'Yes':
                selected_ACDB_to_Transformer.append(row[0].value)

        for row in sheet[98:100]:
            if row[1].value == 'Yes':
                selected_Transformer_to_HT_Panel.append(row[0].value)

    project_specification = {
        'selected_project_types': ['Rooftop'],
        'selected_drawings': selected_drawings,
        'selected_accessories': selected_accessories,
        'selected_components': selected_components,
        'selected_area_of_installation': selected_area_of_installation,
        'selected_termination': selected_termination,
        'selected_inverter_to_ACDB': selected_inverter_to_ACDB,
        'selected_ACDB_to_Transformer': selected_ACDB_to_Transformer,
        'selected_Transformer_to_HT_Panel': selected_Transformer_to_HT_Panel
    }

    return project_specification
