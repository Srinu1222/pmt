from openpyxl import load_workbook
from django.core.files import File
import os


def update_specifications(gantt, project_spec_dict):
    workbook = load_workbook(filename='pmt_backend/utils/default_specs_sheet.xlsx')
    sheet = workbook.active

    selected_drawings = project_spec_dict.get('selected_drawings', [])
    selected_accessories = project_spec_dict.get('selected_accessories', [])
    selected_components = project_spec_dict.get('selected_components', [])
    selected_area_of_installation = project_spec_dict.get('selected_area_of_installation', [])
    selected_termination = project_spec_dict['selected_termination'][0]
    selected_inverter_to_ACDB = project_spec_dict.get('selected_inverter_to_ACDB', [])
    selected_ACDB_to_Transformer = project_spec_dict.get('selected_ACDB_to_Transformer', [])
    selected_Transformer_to_HT_Panel = project_spec_dict.get('selected_Transformer_to_HT_Panel', [])

    for row in sheet[2:17]:
        if row[0].value in selected_drawings:
            row[1].value = 'Yes'

    for row in sheet[22:47]:
        if row[0].value in selected_components:
            row[1].value = 'Yes'

    for row in sheet[51:71]:
        if row[0].value in selected_accessories:
            row[1].value = 'Yes'

    for row in sheet[104:106]:
        if row[0].value in selected_area_of_installation:
            row[1].value = 'Yes'

    if selected_termination == 'LT':

        sheet[74][1].value = 'Yes'

        for row in sheet[76:78]:
            if row[0].value in selected_inverter_to_ACDB:
                row[1].value = 'Yes'

        for row in sheet[81:83]:
            if row[0].value in selected_ACDB_to_Transformer:
                row[1].value = 'Yes'

    elif selected_termination == 'HT':

        sheet[86][1].value = 'Yes'

        for row in sheet[88:90]:
            if row[0].value in selected_inverter_to_ACDB:
                row[1].value = 'Yes'

        for row in sheet[93:95]:
            if row[0].value in selected_ACDB_to_Transformer:
                row[1].value = 'Yes'

        for row in sheet[98:100]:
            if row[0].value in selected_Transformer_to_HT_Panel:
                row[1].value = 'Yes'

    workbook.save('output.xlsx')

    gantt.specifications = File(open('output.xlsx', 'rb'))
    gantt.save()

    os.remove('output.xlsx')
