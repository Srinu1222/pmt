from django.utils.timezone import make_aware


def convert_naive_to_aware(naive_date):
    naive_datetime = naive_date
    aware_datetime = make_aware(naive_datetime)
    return aware_datetime
