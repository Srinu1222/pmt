css = """
@import url('https://fonts.googleapis.com/css?family=Lato');
        @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css');
        body {
            margin:0 auto;
            padding: 0;
            max-width: 400px;
            min-width: 350px;
            border-radius: 4rem;
        }
        
        .grid-container {
            margin: 0;
            margin-right: 1rem;
        }
        .item1 {
            display: block;
            color: #fff;
            padding: 7% 5%;
            background-color: #3d87f1;
            border-radius: 0.3rem;
            font-family: 'Source Sans Pro', sans-serif;
        }
        .wish {
            width: 20rem;
            font-size: 24px;
        }
        .item2 {
            display: block;
            padding: 0% 5%;
 border: 1px solid;
  border-color: #ededed;
  border-radius: 0.2rem;
        }
        .details {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 12px;
            font-weight: 600;
            padding: 10px 0;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.29;
            letter-spacing: normal;
            text-align: left;
            color: #b3b3b3;
            border-bottom: 1px solid #eaeaea;
        }
        .projects {
            padding: 0;
        }
        .Project-Name {
            display: inline-block;
            width: 32%;
            height: 3rem;
            vertical-align: text-top;
            text-decoration: none;
            list-style: none;
            font-family: 'Source Sans Pro', sans-serif;
            margin: 10px 0;
        }
        .Project-Name>p {
            font-weight: bold;
            padding: 0;
            margin: 0;
            font-family: 'Source Sans Pro', sans-serif;
        }
        .button {
            font-family: 'Source Sans Pro', sans-serif;
            background-color: #3d87f1;
            border-color: #3d87f1;
            border-radius: 2px;
            color: white;
            padding: 7px 25px;
            text-align: center;
            border-radius: 3px;
            font-size: 16px;
            cursor: pointer;
            outline: none;
            font-weight: bold;
        }
        .free-space {
            display: block;
            height: 25px;
        }
        .support {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 14px;
            font-weight: normal;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.29;
            letter-spacing: normal;
            text-align: left;
            color: #b3b3b3;
            border-top: 1px solid #eaeaea;
            padding: 20px 0;
        }
        .click-here {
            text-decoration: none;
            color: #3d87f1;
        }
        .regards {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 14px;
            font-weight: normal;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.29;
            letter-spacing: normal;
            text-align: left;
            color: #b3b3b3;
            padding-bottom: 10px;
        }
        .grid-cont {
            background-color: #3d87f1;
            padding: 2% 5%;
            margin: 0;
            border-radius: 0.3rem;
            margin-right: 1rem;
        }
        .social-icons {
            padding: 0;
            margin: 0;
        }
        .social-icons>li {
            display: inline-block;
            width: 2rem;
            list-style: none;
            text-decoration: none;
            margin: 0;
            padding: 0;
            
        }
        .social-icons>li>a {
            color: #fff;
        }
        .foot1 {
            vertical-align: middle;
            width: 60%;
            display: inline-block;
            font-family: Lato !important;
        }
        .foot2 {
            height: 5rem;
            vertical-align: middle;
            width: 30%;
            display: inline-block;
        }
        .rights {
            font-size: 13px;
            font-family: 'Source Sans Pro', sans-serif;
        }
        
        .copyright {
                width: 15px;
                height: 15px;
            }
            
        .foot1>.rights {
            color: #fff;
        }
        .foot2 div {
            width: 6rem;
            height: 0.5rem;
        }
        .foot2 img {
            width: 100%;
        }
"""
