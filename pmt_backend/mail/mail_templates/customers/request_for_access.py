request_for_access = """

    <!DOCTYPE html>
    <html>
    <head>
    <title>Safearth</title>
     <style text="css/html">

            .block1 {
            	width: 95%;
                padding: 3%;
            	 background-color: #3d87f1;
              font-family: Lato !important;
            }
            .SafEarth-Logo {
            	margin-top: 10px;
            	width: 100px;
              height: 40px;
              object-fit: contain;
            }
            .user{
            	margin-top: 30px;
              width: 250px;
              height: 23px;
              font-family: lato;
              font-size: 18px;
              font-weight: normal;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.28;
              letter-spacing: normal;
              text-align: left;
              color: #ffffff;
            }
            .wish{
            	margin-top: 30px;
              width: 220px;
              height: 23px;
              font-family: lato;
              font-size: 18px;
              font-weight: normal;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.28;
              letter-spacing: normal;
              text-align: left;
              color: #ffffff;
            }

            .subject {

            	margin-top: 30px;
              width: 250px;
              height: 100%;
              font-family: lato;
              font-size: 25px;
              font-weight: normal;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.25;
              letter-spacing: normal;
              text-align: left;
              color: #ffffff;
            }

            .block2 {
            	width: 95%;
            	height: 100%;
                padding: 3%;
            }

            .details {
              font-family: lato;
              font-size: 14px;
              font-weight: 600;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.29;
              letter-spacing: normal;
              text-align: left;
              color: #b3b3b3;
            }

            .projects {
            	margin-left: -40px;
            	list-style-type: None;
                display: flex;
                justify-content: space-between;
               font-family: lato;
              font-size: 14px;
              font-weight: bold;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.29;
              letter-spacing: normal;
              text-align: left;
              color: var(--black);
            }

            .-KW-Rajasthan {
              width: 107px;
              height: 18px;
              margin: 4px 0 0;
              font-family: lato;
              font-size: 14px;
              font-weight: normal;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.29;
              letter-spacing: normal;
              text-align: left;
              color: var(--black);
            }

            .View-Project {
              width: 78px;
              height: 18px;
              font-family: lato;
              font-size: 14px;
              font-weight: bold;
              font-stretch: normal;
              font-style: normal;
              line-height: 0.86;
              letter-spacing: normal;
              text-align: center;
              color: #ffffff;
            }

            .button {
      			background-color: #3d87f1; 
                border-color: #3d87f1;
                border-radius: 2px;
                color: white;
                padding: 6px 28px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 2px 2px;
                cursor: pointer;
              }

              .button2 {
                font-family: lato;
                font-size: 14px;
                font-weight: bold;
                font-stretch: normal;
                font-style: normal;
                line-height: 0.86;
                letter-spacing: normal;
                text-align: center;
                color: #ffffff; 
                text-decoration: none;
    			}
                .support {
                height: 36px;
                font-family: lato;
                font-size: 14px;
                font-weight: normal;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.29;
                letter-spacing: normal;
                text-align: left;
                color: #b3b3b3;
              }

                .click-here {
                	text-decoration: none;
      				color: #3d87f
                }

                .regards {
    				margin-top: 40px;
                    width: 109px;
                    height: 54px;
                    font-family: lato;
                    font-size: 14px;
                    font-weight: normal;
                    font-stretch: normal;
                    font-style: normal;
                    line-height: 1.29;
                    letter-spacing: normal;
                    text-align: left;
                    color: #b3b3b3;
                }

                .block3 {
            	width: 95%;
                padding: 3%;
                height: 90%;
            	 background-color: #3d87f1;
              font-family: Lato;

            }

              .social-icons{
                 list-style-type: None;
                  display: flex;
                  justify-content: start;
               		margin-left: -50px;
              }

              .footer-right {
                 list-style-type: None;
                  display: flex;
                  justify-content: space-between;
               		margin-left: -50px;
              }

              .social-icon {
                  padding: 10px
              }


              .social-icons a {
                  color: #fff;
                  text-decoration: none;
              }

            .app-store {
            margin-top: 20px;
            width: 50px;
            height: 19px;
          }

          .google-play {
          margin-top: -20px;
                  width: 50px;
            height: 56px;
          }

          .rights {      
              opacity: 0.7;
              font-family: lato;
              font-size: 12px;
              font-weight: normal;
              font-stretch: normal;
              font-style: normal;
              line-height: 1.29;
              letter-spacing: normal;
              text-align: center;
              color: #ffffff;
            }
            .copyright {
              width: 14px;
              height: 10px;

              object-fit: contain;
              opacity: 0.7;
            }

    		.container{
        display: flex;
        justify-content: space-between
    }

     </style>
    </head>
    <body>
            <div class="block1">
            	<img src="https://cdn.zeplin.io/5f4e2f410a6046b8ff0024ed/assets/484774f9-9f78-44a5-bed8-9aa2fb6de9ee.svg"
                 class="SafEarth-Logo">
                 <p class="user"> Dear {},</p>
                 <p class="wish">We hope you are doing well.</p>
                 <p class="subject">{}</p>

            </div>
                    <div class="block2">
        			<p class="details">Project Details</p>

                    <ul class="projects">
                         <li class="Project-Name">project name<br>
                        <span class="-KW-Rajasthan">{}</span>
                        </li>   
                        <li class="Project-Name"><br>
                        <li class="Project-Name">Date<br>
                        <span class="-KW-Rajasthan">{}</span>
                        </li>    
                 	</ul>

                    <button class="button"><a href="#" class="button2">View project</a></button>
        			<p class="support">For any assistance, please feel free to reach out to our Support Team.<br>
    To contact Support<a href="#" class="click-here"> Click Here</a></p>

    				       <p class="regards">Regards,<br>
                            <span class="">Team Safearth</span><br>
                            <span style="color: black">#Clean Tomorrow</span>
                            </p>

            </div>

            <div class="block3">
            	<div class="container">
                    <div>
                          <ul class="social-icons">
                        <li><a href="#" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>

                    <div>
                        <img src="https://jaguzafarm.com/wp-content/uploads/revslider/site1-slider1/appstore.png" class="app-store">
                    </div>
          		 </div>

                <div class="container">
                        <div> <i class="rights"><img src="https://cdn.zeplin.io/5f4e2f410a6046b8ff0024ed/assets/bceb3926-e2dd-4047-a4f3-ba97973aa399.svg"
                 class="copyright">2021 SafEarth Clean Technologies. All rights reserved</i>
                    </div>

                    <div>
                        <img src="https://www.freepnglogos.com/uploads/google-play-png-logo/new-get-it-on-google-play-png-logo-20.png" class="google-play">
                    </div>


    				</div>
        </div>

    </body>
    </html>
"""
