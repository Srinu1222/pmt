welcome_mail_template = """
    <!DOCTYPE html>
<html lang="en">
   <head>
      <title>Welcome to Safearth!</title>
      <style>
         {{}}
      </style>
   </head>
   <body>
      <div class="grid-container">
         <div class="Header">
            <img src='https://i.ibb.co/B4Y8HT8/Saf-Earth-Logo-Blue-with-White-Hashtag.png' class="SafEarth-Logo">
            <img src='https://i.ibb.co/mG7DDQH/Solar-Flow-1024x-3x.png' width='60' height='35' class="SolarFlow-Logo">
         </div>
         <div class="item1">
            <p class="Welcome">Welcome<br>
               <span class="Thank-you-for-joining-Solar-Flow">
               Thank you for joining
               <span class="text-style-1">Solar Flow</span>
               </span>
            </p>
            <p class="space"></p>
         </div>
         <div class="item2">
            <p class='Dear'>Dear {},<br>
               <span class='we-glad'>We are glad to have you join Solar Flow. The tool has been designed to ensure that you are able to do more solar projects with lesser time, costs and errors.</span><br><span class='we-glad'>Your account has been added to the company {}</span><br><span class='we-glad'>We now request you to sign into your account to begin using the platform.</span>
            </p>
            <p class="account">
               Account Name: {}<br>
               Account Password: {}<br>
               Please note: The password is case sensitive.
            </p>
            <p class='bio'>
               For eg. if your name is
               <span class="text-style-1">Ravi Kumar</span>
               and your phone number is
               <span class="text-style-1">9876543210</span>, then your password is
               <span class="text-style-1">Ravi9876</span>
            </p>
            <span class="bio">
            We look forward to your engagement on Solar Flow.
            </span>
         </div>
         <div class="Footer1">
            <a href="http://www.safearth.in">
            <button class='button'>Login Now</button>
            </a>
         </div>
         <div class="Footer2">
            <p class="rightsText">
               <img src="https://i.ibb.co/FmVNbYX/copyright-24px.png"
                  width='12' height='8' class="copyrightImage" />
               2021 SafEarth Clean Technologies. All rights reserved
            </p>
            <ul class="social-icons">
               <li><a href="#" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
               <li><a href="#" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
               <li><a href="#" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
               <li><a href="#" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
            </ul>
         </div>
      </div>
   </body>
</html>
"""
