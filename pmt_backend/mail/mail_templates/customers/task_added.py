task_added = """
<!DOCTYPE html>
<html lang="en">
<head>
   <title>Task Added</title>
    <style>
        {{}}
    </style>
</head>
<body>

    <div class="grid-container">
        <div class="item1">
            <img src="https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/safearth_logo.png"
                class="SafEarth-Logo">
            <p class="user">Dear Team,</p>
            <p class="wish">{}</p>
        </div>
        
        <div class="item2">
            <p class="details">Project Details</p>
            <ul class="projects">
                <li class="Project-Name">
                    <p>Project Name</p>
                    <span class="-KW-Rajasthan">{}</span>
                </li>
                
                <li class="Project-Name">
                    <p>Stage</p>
                    <span class="-KW-Rajasthan">{}</span>
                </li>
                
                <li class="Project-Name">
                    <p>Event Name</p>
                    <span class="-KW-Rajasthan">{}</span>
                </li>
                
                <li class="Project-Name">
                    <p>Task Name</p>
                    <span class="-KW-Rajasthan">{}</span>
                </li>
                
                <li class="Project-Name">
                    <p>SPOC</p>
                    <span class="-KW-Rajasthan">{}</span>
                </li>
                
            </ul>
            <a href="http://safearth-pmt.s3-website-us-east-1.amazonaws.com/login" class="button">
                View project
            </a>
            <a class="free-space">
            </a>
            <p class="support">For any assistance, please feel free to reach out to our Support Team.<br>
                To contact Support<a href="#" class="click-here"> Click Here</a></p>
            <p class="regards">Regards,</br>
                <span class="">Team Safearth</span></br>
                <span style="color: black">#cleantomorrow</span>
            </p>
        </div>
    </div>
    
    <div class="grid-cont">
        <div class="foot1">
            <ul class="social-icons">
                <li><a href="#" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
            </ul>
            <i class="rights">
                <img src="https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/copyright-24px%403x.png"
                    class="copyright">
                2021 SafEarth Clean Technologies. All rights reserved
            </i>
        </div>
        <div class="foot2">
            <div>
                <img src="https://jaguzafarm.com/wp-content/uploads/revslider/site1-slider1/appstore.png"
                    class="app-store">
            </div>
            <div>
                <img src="https://www.freepnglogos.com/uploads/google-play-png-logo/new-get-it-on-google-play-png-logo-20.png"
                    class="google-play">
            </div>
        </div>
    </div>
    
</body>
</html>
"""
