from datetime import datetime

from django.http import HttpResponse
from celery.decorators import task
from premailer import transform


from pmt_backend.utils.formatting_time import formating_date_time


def templatefun(request):
    s = """<!DOCTYPE html>
    <html lang="en">
    <head>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Lato');
            @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css');
            body {
                margin: 0;
                padding: 0;
            }
            .grid-container {
                margin: 0;
            }
            .item1 {
                display: block;
                color: #fff;
                padding: 7% 5%;
                background-color: #3d87f1;
                font-family: Lato !important;
            }
            .wish {
                width: 20rem;
                font-size: 24px;
            }
            .item2 {
                display: block;
                padding: 0% 5%;
            }
            .details {
                font-family: Lato !important;
                font-size: 12px;
                font-weight: 600;
                padding: 10px 0;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.29;
                letter-spacing: normal;
                text-align: left;
                color: #b3b3b3;
                border-bottom: 1px solid #eaeaea;
            }
            .projects {
                padding: 0;
            }
            .Project-Name {
                display: inline-block;
                width: 32%;
                height: 3rem;
                vertical-align: text-top;
                text-decoration: none;
                list-style: none;
                font-family: Lato !important;
                margin: 10px 0;
            }
            .Project-Name>p {
                font-weight: bold;
                padding: 0;
                margin: 0;
                font-family: Lato !important;
            }
            .button {
                font-family: Lato !important;
                background-color: #3d87f1;
                border-color: #3d87f1;
                border-radius: 2px;
                color: white;
                padding: 7px 25px;
                text-align: center;
                border-radius: 3px;
                font-size: 16px;
                cursor: pointer;
                outline: none;
                font-weight: bold;
            }
            .free-space {
                display: block;
                height: 25px;
            }
            .support {
                font-family: Lato !important;
                font-size: 14px;
                font-weight: normal;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.29;
                letter-spacing: normal;
                text-align: left;
                color: #b3b3b3;
                border-top: 1px solid #eaeaea;
                padding: 20px 0;
            }
            .click-here {
                text-decoration: none;
                color: #3d87f1;
            }
            .regards {
                font-family: Lato !important;
                font-size: 14px;
                font-weight: normal;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.29;
                letter-spacing: normal;
                text-align: left;
                color: #b3b3b3;
                padding-bottom: 10px;
            }
            .grid-cont {
                background-color: #3d87f1;
                padding: 2% 5%;
                margin: 0;
            }
            .social-icons {
                padding: 0;
                margin: 0;
            }
            .social-icons>li {
                display: inline-block;
                width: 2rem;
                list-style: none;
                text-decoration: none;
                margin: 0;
                padding: 0;
            }
            .social-icons>li>a {
                color: #fff;
            }
            .foot1 {
                vertical-align: middle;
                width: 60%;
                display: inline-block;
                font-family: Lato !important;
            }
            .foot2 {
                height: 5rem;
                vertical-align: middle;
                width: 39%;
                display: inline-block;
            }
            .rights {
                font-size: 13px;
                font-family: Lato !important;
            }
            .copyright {
                width: 15px;
                height: 15px;
            }
            .foot1>.rights {
                color: #fff;
            }
            .foot2 div {
                width: 6rem;
                height: 0.5rem;
            }
            .foot2 img {
                width: 100%;
            }
        </style>
    </head>
    <body>

        <div class="grid-container">
            <div class="item1">
                <img src="https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/safearth_logo.png"
                    class="SafEarth-Logo">
                <p class="user">Dear Team,</p>
                <p class="wish">{}</p>
            </div>

            <div class="item2">
                <p class="details">Project Details</p>
                <ul class="projects">
                    <li class="Project-Name">
                        <p>Project Name</p>
                        <span class="-KW-Rajasthan">{}</span>
                    </li>

                </ul>
                <a class="button">
                    View project
                </a>
                <a class="free-space">
                </a>
                <p class="support">For any assistance, please feel free to reach out to our Support Team.<br>
                    To contact Support<a href="#" class="click-here"> Click Here</a></p>
                <p class="regards">Regards,</br>
                    <span class="">Team Safearth</span></br>
                    <span style="color: black">#cleantomorrow</span>
                </p>
            </div>
        </div>

        <div class="grid-cont">
            <div class="foot1">
                <ul class="social-icons">
                    <li><a href="#" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
                </ul>
                <i class="rights">
                    <img src="https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/copyright-24px%403x.png"
                        class="copyright">
                    2021 SafEarth Clean Technologies. All rights reserved
                </i>
            </div>
            <div class="foot2">
                <div>
                    <img src="https://jaguzafarm.com/wp-content/uploads/revslider/site1-slider1/appstore.png"
                        class="app-store">
                </div>
                <div>
                    <img src="https://www.freepnglogos.com/uploads/google-play-png-logo/new-get-it-on-google-play-png-logo-20.png"
                        class="google-play">
                </div>
            </div>
        </div>

    </body>
    </html>
    """

    name = "srinu"
    subject = 'We are pleased to see that you have created a new project on the SafEarth Platform.'
    project_name = 'project1'
    html = transform(s, keep_style_tags=False).format(name, subject, project_name)
    response = HttpResponse(html)
    return response

