sub_contractor_created = """
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sub Contractor Created</title>
	<style>
		{{}}
	</style>
</head>
<body>
    <div class="grid-container">
        <div class="item1">
            <img src="https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/safearth_logo.png"
                class="SafEarth-Logo">
            <p class="user">Dear Sir/Ma`m,</p>
            <p class="wish">{}</p>
        </div>

        <div class="item2">
            <p class="details" style="color:rgb(68, 67, 67);" >Your company has been added as a sub-contractor by {} on the SafEarth Platform. The SafEarth platform is used by Solar Installers to efficiently deploy solar assets. </p>
                        <p class="details" style="color:rgb(68, 67, 67);" >We request you to kindly sign up on the Platform so that you can begin working on these projects at the earliest.</p>
                        <a href="http://safearth-pmt.s3-website-us-east-1.amazonaws.com/login" class="button">
                Sign in
            </a>
            <p class="support">In case you wish to take a demo of our platform, please feel free to 
                To contact Support<a href="#" class="click-here"> Click Here</a></p>
            <p class="support" style="color:rgb(143, 143, 143);">We look forward to working with you to accelerate the world’s transition to clean energy.</p>
            <p class="regards">Regards,</br>
                <span class="">Team Safearth</span></br>
                <span style="color: black">#cleantomorrow</span>
            </p>
        </div>
    </div>
    <div class="grid-cont">
        <div class="foot1">
            <ul class="social-icons">
                <li><a href="#" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
            </ul>
            <i class="rights">
                <img src="https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/copyright-24px%403x.png"
                    class="copyright">
                2021 SafEarth Clean Technologies. All rights reserved
            </i>
        </div>
        <div class="foot2">
            <div>
                <img src="https://jaguzafarm.com/wp-content/uploads/revslider/site1-slider1/appstore.png"
                    class="app-store">
            </div>
            <div>
                <img src="https://www.freepnglogos.com/uploads/google-play-png-logo/new-get-it-on-google-play-png-logo-20.png"
                    class="google-play">
            </div>
        </div>
    </div>

</body>
</html>
"""
