welcome_mail_css =\
    """
    @import url('https://fonts.googleapis.com/css?family=Lato');
         @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css');
         body {
         margin:0 auto;
         padding: 0;
         max-width: 300px;
         min-width: 350px;
         border-radius: 4rem;
         }
         .grid-container {
         margin: 0;
         margin-right: 1rem;
         }
         .item1 {
         display: block;
         color: #fff;
         padding: 7% 5%;
         background-color: #3d87f1;
         border-radius: 0.3rem;
         font-family: 'Source Sans Pro', sans-serif;
         }
         .item2 {
         display: block;
         padding: 0% 5%;
         border: 1px solid;
         border-color: #ededed;
         border-radius: 0.2rem;
         }
         .grid-cont {
         background-color: #3d87f1;
         padding: 2% 5%;
         margin: 0;
         border-radius: 0.3rem;
         margin-right: 1rem;
         }
         .social-icons>li {
         display: inline-block;
         width: 1rem;
         list-style: none;
         text-decoration: none;
         margin: 0;
         padding: 0;
         }
         .social-icons>li>a {
         color: black;
         }
         div .item1{
         background-image:		                     url('https://i.ibb.co/7JgtbJC/Welcome.png');
         background-size: contain;
         background-repeat:no-repeat; 
         background-size: 100% 100%;
         } 
         .space {
         height: 10px;
         }
         .Dear {
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 12px;
         font-weight: 300;
         font-stretch: normal;
         font-style: normal;
         line-height: 1.39;
         letter-spacing: normal;
         text-align: left;
         color: #757575;
         }
         .we-glad {
         display: inline-block;
         margin-top: 3%;
         }
         .account {
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 12px;
         font-weight: normal;
         font-stretch: normal;
         font-style: normal;
         line-height: 1.6;
         letter-spacing: normal;
         text-align: left;
         color: #444343;
         }
         .bio {
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 12px;
         font-weight: 300;
         font-stretch: normal;
         font-style: normal;
         line-height: 1.39;
         letter-spacing: normal;
         text-align: left;
         color: #757575;
         }
         .bio .text-style-1 {
         font-weight: normal;
         color: #444343;
         }
         .Footer1 {
         width: 100%;
         height: 100%;
         padding-top: 5%;
         }
         div .Footer1{
         background-image:		                     url('https://i.ibb.co/VL4q6Dy/Flow-Illustartion.png');
         background-size: contain;
         background-repeat:no-repeat; 
         background-size: 100% 100%;
         } 
         .button {
         font-family: 'Source Sans Pro', sans-serif;
         background-color: #3d87f1;
         color: #fff;
         padding: 0.5em 2em;
         text-align: center;	
         text-decoration: none;
         font-size: 0.6em;	
         display: inline-block;	
         width: 35%; 
         margin: 0em 2em;
         margin-bottom: 5em;
         border: 0px;
         }
         .Footer2 {
         background-color: #f2f2f2;
         width: 95%;
         height: 2%;
         padding: 0.5em;
         vertical-align: center;
         }
         .social-icons {
         padding: 0;
         margin: 0;
         float: right;
         display: inline;
         }
         .rightsText{
         color: #757575;
         font-size: 0.58em;
         font-family: 'Source Sans Pro', sans-serif;
         text-decoration: none;
         width: 50%;
         display: inline;
         }
         .copyrightImage {
         object-fit:cover;
         }
         .Header {
         width: 90%;
         color: #fff;
         padding: 2% 5%;
         background-color: #fff;
         border-radius: 0.1rem;
         font-family: 'Source Sans Pro', sans-serif;
         }
         .SafEarth-Logo{
         width: 20%;
         display: inline;
         }
         .SolarFlow-Logo{
         float: right;
         object-fit:cover;
         }
         .Welcome {
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 30px;
         font-weight: bold;
         font-stretch: normal;
         font-style: normal;
         line-height: 1.43;
         letter-spacing: 2px;
         text-align: center;
         color: #fff;
         }
         .Thank-you-for-joining-Solar-Flow {
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 15px;
         font-weight: 300;
         font-stretch: normal;
         font-style: normal;
         line-height: 1;
         letter-spacing: normal;
         text-align: center;
         color: #BBD5FA;
         }
         .Thank-you-for-joining-Solar-Flow .text-style-1 {
         font-weight: normal;
         color: #fff
         }
    """
