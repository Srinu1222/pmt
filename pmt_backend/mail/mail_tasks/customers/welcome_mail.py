from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform
from pmt_backend.mail.welcomemailcss import welcome_mail_css
from pmt_backend.mail.mail_templates.customers.welcome_mail import welcome_mail_template
from pmt.celery import app


@app.task
def send_welcome_mail(name, company_name, account_name, password, sent_mail):

    emails = [sent_mail]

    subject = 'Welcome to Safearth.'

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = welcome_mail_template.format(name, company_name, account_name, password)

    css_format = formatted_html.format(welcome_mail_css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")

