from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.team_member_removed_from_a_project import \
    team_member_removed_from_a_project
from pmt.celery import app


@app.task
def send_team_member_removed_from_a_project(project_id, member, role):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    project = project_model.objects.get(pk=project_id)

    project_name = project.name
    person_name = member
    role = role

    team_member_list = project.teammember_set.all()
    emails = [member.user.email for member in team_member_list]

    subject = 'New team member added to a {}'.format(project_name)

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = team_member_removed_from_a_project.format(subject, project_name, person_name, ','.join(role))

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")

