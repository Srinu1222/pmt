from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from pmt.celery import app
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.new_team_member_added_to_a_project import \
    new_team_member_added_to_a_project


@app.task
def send_new_team_member_added_to_a_project(project_id, member, roles):

    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    project = project_model.objects.get(pk=project_id)

    project_name = project.name
    person_name = member

    team_member_list = project.teammember_set.all()
    emails = [member.user.email for member in team_member_list]

    subject = 'New team member added to a {}'.format(project_name)
    # if project.customer:
    #     for e in project.customer.emails:
    #         emails.append(e)
    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = new_team_member_added_to_a_project.format(subject, project_name, person_name, ','.join(roles))

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()
