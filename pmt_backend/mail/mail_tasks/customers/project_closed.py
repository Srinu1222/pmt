from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.project_closed import project_closed
from pmt.celery import app


@app.task
def send_project_closed(project_id):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    project = project_model.objects.get(pk=project_id)

    member_list = project.teammember_set.all()
    emails = [member.user.email for member in member_list]

    if project.customer:
        for e in project.customer.emails:
            emails.append(e)
    subject = 'Project Closed on the SafEarth Platform'

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = project_closed.format(subject, project.name)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")
