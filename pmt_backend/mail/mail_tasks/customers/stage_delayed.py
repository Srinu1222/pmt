from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.stage_delayed import stage_delayed
from pmt.celery import app



@app.task
def send_stage_delayed(project_id):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    project = project_model.objects.get(pk=project_id)

    project_name = project.name
    stage = project.stage_reached

    team_member_list = project.teammember_set.all()

    emails = [member.user.email for member in team_member_list]
    # if project.customer:
    #     for e in project.customer.emails:
    #         emails.append(e)
    subject = 'Stage delayed on {}'.format(project_name)

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = stage_delayed.format(subject, project_name, stage)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")

