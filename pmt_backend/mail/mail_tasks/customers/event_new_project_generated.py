from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.event_new_project_generated import event_new_project_generated
from pmt.celery import app


@app.task
def send_event_new_project_generated(project_id):
    project_model = apps.get_model('pmt_backend.{}'.format('Project'))
    project = project_model.objects.get(pk=project_id)

    team_member_list = project.teammember_set.all()
    emails = [member.user.email for member in team_member_list]

    if project.customer:
        for e in project.customer.emails:
            emails.append(e)

    # emails = ['srinivasktpl1222@gmail.com', 'harshit@safearth.in', 'kumawatdevesh99@gmail.com', 'kaustubh@safearth.in']

    subject = 'New Project Created on the SafEarth Platform'

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = event_new_project_generated.format(subject, project.name)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")