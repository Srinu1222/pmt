from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.mail_templates.customers.event_approved import event_approved
from pmt_backend.models import ProjectRight
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer
from pmt_backend.mail.css import css
from pmt.celery import app

@app.task
def send_event_approved(event_id, spoc):
    event_model = apps.get_model('pmt_backend.{}'.format('Event'))
    event = event_model.objects.get(pk=event_id)

    project = event.project
    project_name = project.name
    stage = project.stage_reached
    event_name = event.name
    approver = event.default_approver.name

    team_member_list = []
    if event.is_sub_contractor_event:
        sub_contractor_team_members = project.subcontractorteammember_set.all()
        for member in sub_contractor_team_members:
            project_right = ProjectRight.objects.filter(sub_contractor_team_member=member, project=event.project).first()
            can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
            if can_view or can_work or can_edit or can_approve:
                team_member_list.append(member)
    else:
        team_members = project.teammember_set.all()
        for member in team_members:
            project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
            can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
            if can_view or can_work or can_edit or can_approve:
                team_member_list.append(member)

    emails = [member.user.email for member in team_member_list]

    # if project.customer:
    #     for e in project.customer.emails:
    #         emails.append(e)

    # emails = ['srinivasktpl1222@gmail.com']

    subject = 'Event Approved on Project {}'.format(project_name)

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = event_approved.format(subject, project_name, stage, event_name, spoc, approver)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")
