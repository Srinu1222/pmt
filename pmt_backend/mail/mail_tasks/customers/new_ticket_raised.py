from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.new_ticket_raised import new_ticket_raised
from pmt_backend.models import ProjectRight
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer
from pmt.celery import app


@app.task
def send_new_ticket_raised(event_id, raised_by, comments):

    event_model = apps.get_model('pmt_backend.{}'.format('Event'))
    event = event_model.objects.get(pk=event_id)

    project = event.project
    project_name = project.name
    stage = project.stage_reached
    event_name = event.name
    spoc = event.default_spoc.name
    comments = comments

    team_members = project.teammember_set.all()
    team_member_list = []
    for member in team_members:
        project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
        can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
        if can_view or can_work or can_edit or can_approve:
            team_member_list.append(member)

    emails = [member.user.email for member in team_member_list]
    # if project.customer:
    #     for e in project.customer.emails:
    #         emails.append(e)
    subject = 'New ticket raised on {}'.format(project_name)

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = new_ticket_raised.format(subject, project_name, stage, event_name, spoc, raised_by, comments)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")

