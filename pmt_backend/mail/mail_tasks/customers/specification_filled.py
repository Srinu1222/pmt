from datetime import datetime

from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from django.http import HttpResponse

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.event_new_project_generated import event_new_project_generated
from premailer import transform
from pmt_backend.models import TeamMember
from pmt_backend.utils.formatting_time import formating_date_time
from pmt.celery import app


@app.task
def send_new_project_generated(user, project):
    # user.email, 'kaustubh@safearth.in',
    emails = ['srinivasktpl1222@gmail.com']
    msg = EmailMultiAlternatives('New Project Created on the SafEarth Platform.', '',
                                 '"Safearth" <info@safearth.in>', emails)


    user = TeamMember.objects.get(user=user).name
    subject = 'We are pleased to see that you have created a new project on the SafEarth Platform.'
    project_name = project.name
    project_start_date = project.project_start_date
    project_start_date = formating_date_time(project_start_date)


    formatted_html = event_new_project_generated.format(
                            user, subject, project_name, project_start_date)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")
