from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform
from pmt_backend.mail.mail_templates.customers.event_delayed import event_delayed
from pmt_backend.models import ProjectRight
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer
from pmt.celery import app


@app.task
def send_event_delayed(event_id):
    event_model = apps.get_model('pmt_backend.{}'.format('Event'))
    event = event_model.objects.get(pk=event_id)

    project = event.project
    project_name = project.name
    stage = project.stage_reached
    event_name = event.name
    spoc = event.default_spoc.name

    team_member_list = []
    if event.is_sub_contractor_event:
        sub_contractor_team_members = project.subcontractorteammember_set.all()
        for member in sub_contractor_team_members:
            project_right = ProjectRight.objects.filter(sub_contractor_team_member=member, project=event.project).first()
            can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
            if can_view or can_work or can_edit or can_approve:
                team_member_list.append(member)
    else:
        team_members = project.teammember_set.all()
        for member in team_members:
            project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
            can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
            if can_view or can_work or can_edit or can_approve:
                team_member_list.append(member)

    emails = [member.user.email for member in team_member_list]
    # if project.customer:
    #     for e in project.customer.emails:
    #         emails.append(e)
    subject = 'Event Delayed on {}'.format(project_name)

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = event_delayed.format(subject, project_name, stage, event_name, spoc)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")
