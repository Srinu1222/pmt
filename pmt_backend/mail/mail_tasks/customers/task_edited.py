from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform

from pmt_backend.mail.css import css
from pmt_backend.mail.mail_templates.customers.task_edited import task_edited
from pmt_backend.models import ProjectRight
from pmt_backend.serializers.project.get_project_rights import get_rights_serializer
from pmt.celery import app


@app.task
def send_task_edit(event_id, spoc, old_task_name, new_task_name):
    event_model = apps.get_model('pmt_backend.{}'.format('Event'))
    event = event_model.objects.get(pk=event_id)

    project = event.project
    project_name = project.name
    stage = project.stage_reached
    event_name = event.name
    old_task_name = old_task_name
    new_task_name = new_task_name

    team_members = project.teammember_set.all()
    team_member_list = []
    for member in team_members:
        project_right = ProjectRight.objects.filter(team_member=member, project=event.project).first()
        can_view, can_work, can_edit, can_approve = get_rights_serializer(project_right, event.stage)
        if can_view or can_work or can_edit or can_approve:
            team_member_list.append(member)

    emails = [member.user.email for member in team_member_list]

    subject = 'Task edited on {}'.format(project_name)

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = task_edited.format(subject, project_name, stage, event_name, spoc, old_task_name, new_task_name)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")

