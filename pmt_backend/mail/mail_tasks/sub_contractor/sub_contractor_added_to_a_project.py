from django.core.mail import EmailMultiAlternatives
from celery.decorators import task
from premailer import transform
from pmt_backend.mail.mail_templates.sub_contractor.sub_contractor_added_to_a_project import \
    sub_contractor_added_to_project
from pmt_backend.mail.css import css
from pmt.celery import app


@app.task
def send_sub_contractor_added_to_project(company_name, emails):

    subject = 'Sub-Contractor Added to Project.'

    msg = EmailMultiAlternatives(subject, '', '"Safearth" <info@safearth.in>', emails)

    formatted_html = sub_contractor_added_to_project.format(subject, company_name)

    css_format = formatted_html.format(css)

    html = transform(css_format, keep_style_tags=False)

    msg.attach_alternative(html, "text/html")

    msg.send()

    print("Mail sent successfully")
