from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import SampleEvent


class GetSampleEvent(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        sample_event = SampleEvent.objects.filter(pk=request.GET['sample_event_id']).first()

        sample_event = {
            'id': sample_event.id,
            'stage': sample_event.stage,
            'event': sample_event.event,
            'is_mandatory': sample_event.is_mandatory,
            'approver': {'id': sample_event.sample_event_approver.pk,
                         'name': sample_event.sample_event_approver.name},
            'spoc': {'id': sample_event.sample_event_spoc.pk, 'name': sample_event.sample_event_spoc.name},
            'pre_requisites': sample_event.pre_requisites,
            'checklists': sample_event.checklists,
            'days_needed': sample_event.days_needed
        }

        return JsonResponse({'sample_event': sample_event})
