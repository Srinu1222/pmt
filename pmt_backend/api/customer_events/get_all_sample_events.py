from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.api.customer_events.migrate_data_to_sample_event import migrate_data_for_sample_events
from pmt_backend.models import SampleEvent


class GetAllSampleEvents(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']

        sample_event_queryset = SampleEvent.objects.all()

        if sample_event_queryset.count() == 0:
            migrate_data_for_sample_events(project_id)

        sample_events_list = []
        for sample_event in sample_event_queryset:
            sample_events_list.append(
                {
                    'id': sample_event.id,
                    'stage': sample_event.stage,
                    'event': sample_event.event,
                    'is_mandatory': sample_event.is_mandatory,
                    'approver': {'id': sample_event.sample_event_approver.pk,
                                 'name': sample_event.sample_event_approver.name},
                    'spoc': {'id': sample_event.sample_event_spoc.pk, 'name': sample_event.sample_event_spoc.name},
                    'pre_requisites': sample_event.pre_requisites,
                    'checklists': sample_event.checklists,
                    'days_needed': sample_event.days_needed
                }
            )
        return JsonResponse({'sample_events_list': sample_events_list})
