from pmt_backend.models import SampleEvent, Project, ProjectRight
from openpyxl import load_workbook


def migrate_data_for_sample_events(project_id):
    workbook = load_workbook(filename="pmt_backend/utils/customer_scope_works.xlsx")
    sheet = workbook.active

    project = Project.objects.get(pk=project_id)

    for index, row in enumerate(sheet):
        if index == 0:
            continue

        if row[0].value:
            spoc = ProjectRight.objects.filter(
                project=project, roles__contained_by=['Customer', 'Project Manager']).first().team_member

            project_right_queryset_approver = ProjectRight.objects.filter(
                project=project, roles__contains=[row[5].value])
            approver_list = [right.team_member for right in project_right_queryset_approver]
            count = len(approver_list)
            try:
                approver = approver_list[index % count]
            except:
                approver = ProjectRight.objects.filter(
                    project=project, roles__contains=['Project Manager']).first().team_member

            stage = row[0].value
            event = row[1].value
            is_mandatory = False if row[2].value == 'No' else True
            pre_requisites = [i[3:] for i in row[3].value.split('\n')]
            checklists = [i[3:] for i in row[6].value.split('\n')]
            days_needed = row[8].value

            SampleEvent.objects.create(
                stage=stage,
                event=event,
                is_mandatory=is_mandatory,
                sample_event_approver=approver,
                sample_event_spoc=spoc,
                pre_requisites=pre_requisites,
                checklists=checklists,
                days_needed=days_needed
            )

