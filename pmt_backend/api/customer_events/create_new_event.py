from datetime import timedelta, datetime

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, Project, TeamMember


class AddNewEvent(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        project = Project.objects.get(pk=project_id)
        stage = request.POST['stage']
        name = request.POST['name']
        event_name = request.POST['event_name']
        spoc = request.POST['spoc']
        head_of_event_id = request.POST['head_of_event_id']

        days_needed = int(request.POST['days_needed'])
        date = request.POST['date']
        date = datetime.strptime(date, "%d/%m/%Y")

        pre_requisites = request.POST.getlist('pre_requisites')
        checklists = request.POST.getlist('checklists')

        prerequisites = [
            {
                "id": index + 1,
                "status": False,
                "title": title,
                'spoc': head_of_event_id,
                "comments": [],
                "due_on": date.strftime('%d/%m/%Y')
            }
            for index, title in enumerate(pre_requisites)
        ]

        checklists = [
            {
                "id": index + 1,
                "status": False,
                "title": title,
                'spoc': head_of_event_id,
                "comments": [],
                "due_on": date.strftime('%d/%m/%Y')
            }
            for index, title in enumerate(checklists)
        ]

        # start_time and completed_time
        start_time = date
        completed_time = start_time + timedelta(days=days_needed)

        event_order_no = project.event_set.all().order_by('-event_order_no').first().event_order_no

        member = TeamMember.objects.get(pk=head_of_event_id)
        spoc_member = TeamMember.objects.get(pk=spoc)

        Event.objects.create(
            project=project,
            stage=stage,
            name=event_name,
            Prerequisites=prerequisites,
            is_customer=True,
            default_approver=member,
            default_spoc=spoc_member,
            checklists=checklists,
            days_needed=days_needed,
            event_order_no=event_order_no+1,
            completed_time=completed_time,
            start_time=start_time
        )
        return JsonResponse({})
