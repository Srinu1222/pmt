from datetime import datetime

from django.http import JsonResponse
from pmt_backend.models import Project, Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.utils.formatting_time import formating_date_time


def actual_date(date):
    if date:
        return formating_date_time(date)
    else:
        return "Not yet started"


class GetGanttChartDataForStages(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        project_id = request.GET['project_id']
        project = Project.objects.filter(pk=project_id).first()

        stages = ['PRE-REQUISITES', 'APPROVALS', 'ENGINEERING',
                  'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER']

        project_stage_list = []
        for stage in stages:
            events_list = list(Event.objects.filter(stage=stage, project=project).order_by('event_order_no'))
            first_event = events_list[0]
            last_event = events_list[-1]
            status = first_event.stage_status
            actual_start_date = actual_date(first_event.actual_start_time)
            actual_completed_time = actual_date(last_event.actual_completed_time)

            project_stage_list.append(
                {
                    'status': status,
                    "stage": stage,
                    "start_time": formating_date_time(first_event.start_time),
                    "completed_time": formating_date_time(last_event.completed_time),
                }
            )

            if first_event.status == 'IN-PROGRESS':
                project_stage_list.append(
                    {
                        'status': status,
                        "stage": stage,
                        "start_time": actual_start_date,
                        "completed_time": formating_date_time(datetime.now())
                    }
                )
            else:
                project_stage_list.append(
                    {
                        'status': status,
                        "stage": stage,
                        "start_time": actual_start_date,
                        "completed_time": actual_completed_time
                    }
                )

        return JsonResponse({"project_stages_list": project_stage_list})
