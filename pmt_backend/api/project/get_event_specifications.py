from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event


class GetEventSpecification(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        list_of_specifications = []
        for checklist in event_checklists:
            if checklist.get("specifications"):
                list_of_specifications = checklist['specifications']
                break

        return JsonResponse({"list_of_specifications": list_of_specifications})
