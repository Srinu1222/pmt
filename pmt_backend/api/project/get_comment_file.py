from django.http import JsonResponse
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from django.http import FileResponse
from pmt_backend.models import Event, CommentFile


class GetCommentFile(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        pre_requisite_id = request.GET.get('pre_requisite_id')
        checklist_id = request.GET.get('checklist_id')
        comment_id = int(request.GET['comment_id'])

        if pre_requisite_id:
            comment_file = CommentFile.objects.filter(
                event_id=event_id, prerequisite_id=pre_requisite_id, comment_id=comment_id
            ).first()
        elif checklist_id:
            comment_file = CommentFile.objects.filter(
                event_id=event_id, checklist_id=checklist_id, comment_id=comment_id
            ).first()

        file = comment_file.file.url
        return JsonResponse({"file": file})
