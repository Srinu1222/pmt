from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Project


class AccessProvided(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        otp = request.POST['otp']
        project = Project.objects.get(pk=project_id)
        if int(project.six_digit_otp) == int(otp):
            project.access_provided = True
            project.save()
        return JsonResponse({"access_provided": project.access_provided})
