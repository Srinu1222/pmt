from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.ticket_closed import send_ticket_closed
from pmt_backend.models import Event, Ticket, TeamMember, Customer
from pmt_backend.serializers.Notifications.ticket_closed import ticket_closed


class ChangeTicketStatus(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        ticket_id = request.POST['ticket_id']

        event = Event.objects.filter(pk=event_id).first()
        ticket = Ticket.objects.filter(pk=ticket_id, event=event).first()

        if ticket.status:
            status = False
        else:
            status = True

        ticket.status = status
        ticket.save()

        if ticket.status:
            # create notification and send mail for ticket closed
            ticket_closed(request.user, event)
            raised_by = TeamMember.objects.get(user=request.user).name
            if event.default_spoc:
                spoc = event.default_spoc.name
            else:
                spoc = event.project.customer.customer_name
            send_ticket_closed.delay(event.id, spoc, raised_by, raised_by)

        return JsonResponse({})
