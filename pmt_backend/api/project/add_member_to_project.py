from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import TeamMember, User, Project
from pmt_backend.serializers.Notifications.new_team_member_added_to_project import new_team_member_added_to_project
from pmt_backend.serializers.project.create_project_right import create_project_right
from pmt_backend.mail.mail_tasks.customers.new_team_member_added_to_a_project import \
    send_new_team_member_added_to_a_project
from pmt_backend.utils.convert_json_to_python import json_to_python


class AddMemberToProject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        team_member_id = request.POST['team_member_id']
        roles = json_to_python(request.POST['roles'])

        project = Project.objects.get(pk=project_id)
        member = TeamMember.objects.get(pk=team_member_id)
        project.teammember_set.add(member)
        project.save()

        # create Notification for add new member to a project
        new_team_member_added_to_project(project, member)
        send_new_team_member_added_to_a_project.delay(project.id, member.name, roles)

        # Adding project Rights
        create_project_right(roles, member, project)

        return JsonResponse({})
