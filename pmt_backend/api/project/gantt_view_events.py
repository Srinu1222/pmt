from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Project

from pmt_backend.serializers.project.get_gantt_events import gantt_events_serializer


class GanttViewEvents(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        stage_name = request.GET['stage_name']
        project_id = request.GET['project_id']
        sub_contractor_mode = True if request.GET.get('sub_contractor_mode') == 'true' else False
        project = Project.objects.filter(pk=project_id).first()
        serialized_gant_events_data = gantt_events_serializer(project, stage_name, sub_contractor_mode, request.user)

        if not serialized_gant_events_data:
            msg = "project id does not exist"
            return send_fail_http_response(msg=msg)
        else:
            return JsonResponse(serialized_gant_events_data)
