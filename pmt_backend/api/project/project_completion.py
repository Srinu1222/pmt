import os
from datetime import datetime

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Project, TeamMember, Gantt
from pmt_backend.serializers.gantt.update_gantt_file_based_on_project_planning import \
    UpdateGanttFileBasedOnProjectPlanning
from pmt_backend.serializers.gantt.update_specs_based_on_project_spec import \
    update_specifications_based_on_project_specs


class ChangeProjectCompletion(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        status = request.POST['project_completion']
        generate_template = request.POST.get('generate_template')

        status = True if status == 'True' else False

        project = Project.objects.filter(pk=project_id).first()
        project.project_complete = status

        project.project_start_date = datetime.now()
        member = TeamMember.objects.get(user=request.user)
        project.created_by = member

        if generate_template == 'True':
            previous_gantt = project.gantt

            new_gantt = Gantt.objects.create(project_impact=previous_gantt.project_impact,
                                             specifications=previous_gantt.specifications,
                                             gantt_file=previous_gantt.gantt_file, company=request.user.company)

            update_specifications_based_on_project_specs(project, new_gantt)
            UpdateGanttFileBasedOnProjectPlanning(previous_gantt, new_gantt, project)

            project.gantt = new_gantt

        project.save()
        return JsonResponse({})
