from django.http import JsonResponse
from pmt_backend.models import Project, TeamMember, Event, SubContractorTeamMember
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from datetime import datetime, timedelta

from pmt_backend.serializers.project.get_member_tasks_for_daily_updates import get_member_tasks_for_daily_updates


class GetMemberTasks(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        is_today = request.GET.get('is_today')
        project_id = request.GET['project_id']
        user = request.user

        member = TeamMember.objects.filter(user=user).first()
        project = Project.objects.filter(pk=project_id).first()
        sub_contractor_member = SubContractorTeamMember.objects.filter(user=user, projects=project).first()

        tasks = get_member_tasks_for_daily_updates(is_today, project, member, sub_contractor_member)

        return JsonResponse({'tasks': tasks})
