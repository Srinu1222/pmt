from django.http import JsonResponse

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class AddPreRequisiteTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()

        status = request.POST['status']
        spoc = request.POST['spoc']
        title = request.POST['title']

        event_pre_requisites = event.Prerequisites
        id = len(event_pre_requisites)+1

        event_pre_requisites.append(
            {
                "id": id,
                "title": title,
                "status": status,
                "spoc": spoc,
                "comments": []
            }
        )

        event.save()

        return JsonResponse({"id": id})
