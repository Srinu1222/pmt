from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Project, Event
from pmt_backend.serializers.project.gantt_view import gantt_view_serializer


class GanttView(APIView):

    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request,  format=None):
        project_id = request.GET['project_id']
        project = Project.objects.get(pk=project_id)
        serialized_gantt_view = gantt_view_serializer(project)
        return JsonResponse(serialized_gantt_view, safe=False)
