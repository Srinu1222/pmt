from datetime import datetime
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.spoc_changed_for_task import send_spoc_change
from pmt_backend.mail.mail_tasks.customers.task_completion import send_task_completion
from pmt_backend.mail.mail_tasks.customers.task_edited import send_task_edit
from pmt_backend.models import Event, TeamMember, SubContractorTeamMember, Inventory
from pmt_backend.serializers.Notifications.spoc_change import spoc_change
from pmt_backend.serializers.Notifications.task_completion import task_completion
from pmt_backend.serializers.Notifications.task_edit import task_edited
from pmt_backend.utils.dict_for_linking_matrial_name_with_event import dict_for_linking_material_name_with_event
from pmt_backend.utils.formatting_time import formating_date_time
from pmt_backend.utils.get_Jsonfield_dict import get_event_task_dict, get_event_task_index


class ChangeChecklistTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']
        event = Event.objects.filter(pk=event_id).first()
        status = request.POST.get('status').capitalize()
        spoc = request.POST.get('spoc')
        is_sub_contractor = request.POST['is_sub_contractor']

        # checklist status update
        event_checklists = event.checklists
        checklist_dict = get_event_task_dict(event_checklists, checklist_id)
        checklist_index = get_event_task_index(event_checklists, checklist_id)
        title = checklist_dict['title']

        if status == 'True':
            checklist_dict['status'] = True
            checklist_dict['completed_time'] = formating_date_time(datetime.now())

            # To UPDATE INVENTORY STATUS
            data = {
                        'Material Dispatched – Expected Date of Delivery: Input': 'IN_TRANSIT',
                        'Material Allowed In': 'DELIVERED_ON_SITE',
                        'Material Stored': 'STORED_ON_SITE',
                    }
            if data.get(title):
                material_names = event.selected_names
                if material_names:
                    inventory = Inventory.objects.filter(
                        project=event.project, material_name__in=material_names).first()
                    if inventory:
                        inventory.status = data[title]
                        inventory.save()

            if is_sub_contractor == 'false':
                member = TeamMember.objects.get(user=request.user)
                checklist_dict['spoc'] = member.id
            event.status = "IN-PROGRESS"
            event.actual_start_time = datetime.now()
            event.checklists[checklist_index] = checklist_dict
            # linked_to_status_update_for_every_checklist
            if checklist_dict.get('linked_to'):
                linked_to_dict = checklist_dict['linked_to']
                linked_to_event = Event.objects.get(pk=linked_to_dict['event_id'])
                task_id = linked_to_dict['task_id']
                task_type = linked_to_dict['task_type']
                if task_type == 'CHECKLISTS':
                    linked_to_event_checklists = linked_to_event.checklists
                    linked_to_checklist_dict = get_event_task_dict(linked_to_event_checklists, task_id)
                    if linked_to_checklist_dict:
                        linked_to_checklist_dict['status'] = True
                        linked_to_checklist_dict['completed_time'] = formating_date_time(datetime.now())
                        if is_sub_contractor == 'false':
                            linked_to_checklist_dict['spoc'] = member.id
                elif task_type == 'PRE-REQUISITES':
                    linked_to_event_pre_requisites = linked_to_event.Prerequisites
                    linked_to_prerequisite_dict = get_event_task_dict(linked_to_event_pre_requisites, task_id)
                    if linked_to_prerequisite_dict:
                        linked_to_prerequisite_dict['status'] = True
                        if is_sub_contractor == 'false':
                            linked_to_prerequisite_dict['spoc'] = member.id
        elif spoc:
            if is_sub_contractor == 'false':
                checklist_dict['spoc'] = spoc
            event.checklists[checklist_index] = checklist_dict

            # create notification for spoc change
            if is_sub_contractor == 'false':
                changed_by = TeamMember.objects.get(user=request.user).name
                changed_to = TeamMember.objects.get(pk=spoc).name
            else:
                changed_by = SubContractorTeamMember.objects.get(user=request.user).name
                changed_to = SubContractorTeamMember.objects.get(pk=spoc).name

            spoc_change(changed_by, changed_to, event)

            # create Notification for send email for spoc change
            send_spoc_change.delay(event.id, changed_by, changed_to)

            # create Notification and send mail for task edited
            task_edited(request.user, event, title)
            if event.default_spoc:
                spoc = event.default_spoc.name
            else:
                spoc = event.project.customer.customer_name
            send_task_edit.delay(event.id, spoc, title, title)

        event.save()

        # create Notification and send mail for task completion
        if status:
            task_completion(request.user, event, title)
            if is_sub_contractor == 'false':
                if event.default_spoc:
                    spoc = event.default_spoc.name
                else:
                    spoc = event.project.customer.customer_name
            else:
                if event.sub_contractor_spoc:
                    spoc = event.sub_contractor_spoc.name
                else:
                    spoc = event.project.customer.customer_name

            send_task_completion.delay(event.id, spoc, title)

        return JsonResponse({})
