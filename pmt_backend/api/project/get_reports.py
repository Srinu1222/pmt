from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Project, DailyReport, MonthlyReport, WeeklyReport
from pmt_backend.serializers.project.generate_basic_gantt import generate_basic_gantt
from pmt_backend.serializers.project.generate_details_gantt import generate_details_gantt


class GetReports(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        project_id = request.GET['project_id']
        project = Project.objects.get(pk=project_id)

        all_updates_list = [
            {
                'title': 'Daily Updates',
                'queryset': DailyReport.objects.filter(project=project)
            },
            {
                'title': 'Weekly Updates',
                'queryset': WeeklyReport.objects.filter(project=project)
            },
            {
                'title': 'Monthly Updates',
                'queryset': MonthlyReport.objects.filter(project=project)
            },
        ]

        all_reports_list = []
        for i in all_updates_list:
            data = []
            for j in i['queryset']:
                data.append(
                        {
                            'id': j.id,
                            'image_url': project.image.url if project.image else None,
                            'project_name': project.name,
                            'date': j.date,
                            'report': j.report.url if j.report else None,
                        }
                    )
            all_reports_list.append(
                {
                    'title': i['title'],
                    'count': len(data),
                    'data': data
                }
            )

        return JsonResponse({
            'reports_list': all_reports_list,
            'basic_gantt_url': project.basic_gantt_report.url if project.basic_gantt_report else None,
            'details_gantt_url': project.details_gantt_report.url if project.details_gantt_report else None,
        })
