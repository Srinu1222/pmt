from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import TeamMember, DailyUpdate, Project, Event, Databank, Document
from pmt_backend.utils.convert_json_to_python import json_to_python
from pmt_backend.utils.formatting_time import formating_date_time
from datetime import datetime
from pmt_backend.serializers.Notifications.task_completion import task_completion
from pmt_backend.mail.mail_tasks.customers.task_completion import send_task_completion


class CreateDailyUpdate(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        today_completed_tasks = json_to_python(request.POST['today_completed_tasks'])
        on_going_tasks = json_to_python(request.POST['on_going_tasks'])
        tomorrow_tasks = json_to_python(request.POST['tomorrow_tasks'])
        documents_list = json_to_python(request.POST['documents_list'])
        document_files = request.FILES.getlist('document_files')
        pictures_list = json_to_python(request.POST['pictures_list'])
        picture_files = request.FILES.getlist('picture_files')
        today_msg_list = request.POST.getlist('today_msg_list')
        tomorrow_msg_list = request.POST.getlist('tomorrow_msg_list')

        # DAILY TASKS
        team_member = TeamMember.objects.get(user=request.user)
        project = Project.objects.get(pk=project_id)

        update = DailyUpdate.objects.create(
            member=team_member,
            today_completed_tasks=today_completed_tasks,
            on_going_tasks=on_going_tasks,
            tomorrow_tasks=tomorrow_tasks,
            project=project,
            tomorrow_task_message=tomorrow_msg_list,
            today_task_message=today_msg_list,
        )
        # FOR EVENT DOC UPLOAD
        doc_objects_list = []
        for index, i in enumerate(documents_list):
            event = Event.objects.get(pk=i['event_id'])
            databank = Databank.objects.get(project=event.project)
            doc_objects_list.append(
                Document(
                    databank=databank, event=event, is_image=False,
                    description=i['description'],
                    title=i['title'],
                    file=document_files[index],
                    daily_update_pk=update.pk,
                )
            )
            Document.objects.bulk_create(doc_objects_list)

        # FOR EVENT PICTURE UPLOAD
        picture_objects_list = []
        for index, i in enumerate(pictures_list):
            event = Event.objects.get(pk=i['event_id'])
            databank = Databank.objects.get(project=event.project)
            picture_objects_list.append(
                Document(
                    databank=databank, event=event, is_image=True,
                    description=i['description'],
                    title=i['title'],
                    file=picture_files[index],
                    daily_update_pk=update.pk,
                )
            )
            Document.objects.bulk_create(picture_objects_list)

        for task in today_completed_tasks:
            event = Event.objects.get(pk=task['event_id'])
            for checklist in event.checklists:
                if int(task['checklist_id']) == int(checklist['id']):
                    checklist['status'] = True
                    checklist['completed_time'] = formating_date_time(datetime.now())
                    task_completion(request.user, event, checklist['title'])
                    spoc = team_member.name
                    send_task_completion.delay(event.id, spoc, checklist['title'])

            if event.status == "PENDING":
                event.status = 'IN-PROGRESS'
            event.save()

        return JsonResponse({})
