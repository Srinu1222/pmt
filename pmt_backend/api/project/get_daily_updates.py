from django.http import JsonResponse
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.project.get_daily_updates import get_daily_updates_serializer


class GetProjectDailyUpdates(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.data['project_id']
        project = Project.objects.get(pk=project_id)
        serialized_Daily_updates_data = get_daily_updates_serializer(project)
        return JsonResponse(serialized_Daily_updates_data)

