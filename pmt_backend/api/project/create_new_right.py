from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import ProjectRight, Project, TeamMember


class CreateNewRight(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        team_member_id = request.POST['team_member_id']
        right_type = request.POST['right_type']
        stage = request.POST['stage']

        project = Project.objects.get(pk=project_id)
        team_member = TeamMember.objects.get(pk=team_member_id)

        project_right = ProjectRight.objects.filter(team_member=team_member, project=project).first()

        right_type_dict = {
            "can_view": project_right.can_view,
            "can_work": project_right.can_work,
            "can_approve": project_right.can_approve,
            "can_edit": project_right.can_edit
        }

        right_type_dict[right_type].append(stage)

        # Tracking additional Rights
        if project_right.additional_rights is None:
            project_right.additional_rights = [{"type": right_type, "stage": stage.upper()}]
        else:
            project_right.additional_rights.append({"type": right_type, "stage": stage.upper()})

        project_right.save()


        return JsonResponse({})
