from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Project
from pmt_backend.serializers.Notifications.sub_contractor_project_planning_completed import \
    sub_contractor_project_planning_completed


class SubContractorProjectPlanningCompletion(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        project = Project.objects.filter(pk=project_id).first()
        project.subcontractor_project_planning = True
        project.save()

        # create Notification for Subcontractor Project Planning Completed.
        sub_contractor_project_planning_completed(project)

        return JsonResponse({})
