from django.http import JsonResponse
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.project.get_project_team import get_team_serializer


class GetProjectTeams(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        project = Project.objects.get(pk=project_id)
        serialized_team_data = get_team_serializer(project)
        if not serialized_team_data:
            msg = "project id does not exist"
            return send_fail_http_response(msg=msg)
        else:
            return JsonResponse(serialized_team_data)
