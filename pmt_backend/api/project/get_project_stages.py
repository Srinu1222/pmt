from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class GetProjectStages(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        stages = [
            'PRE-REQUISITES', 'APPROVALS', 'ENGINEERING',
            'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER',
        ]
        return JsonResponse({'stages': stages})


