from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView

from pmt_backend.models import Project
from pmt_backend.serializers.project.get_project_inventory import get_inventory_serializer


class GetProjectInventory(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):

        project_id = request.GET['project_id']

        project = Project.objects.filter(pk=project_id).first()

        response, response_status = get_inventory_serializer(project)
        if response_status:
            return JsonResponse(response)
        else:
            return response
