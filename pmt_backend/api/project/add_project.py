from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.mail.mail_tasks.customers.event_new_project_generated import send_event_new_project_generated
from pmt_backend.mail.mail_tasks.customers.new_team_member_added_to_a_project import \
    send_new_team_member_added_to_a_project
from pmt_backend.models import TeamMember, Gantt, SubContractorTeamMember, Company, ProjectRight
from pmt_backend.api.events.create_project_events import Create_Project_events
from pmt_backend.serializers.Notifications.new_project_created import new_project_created
from pmt_backend.serializers.Notifications.new_team_member_added_to_project import new_team_member_added_to_project
from pmt_backend.serializers.Notifications.sub_contractor_added import sub_contractor_added
from pmt_backend.serializers.project.add_project import (
    create_project_serializer, create_customer_serializer,
    create_ProjectSpecification_serializer, create_databank_serializer, create_stakeholder_serializer
)
from pmt_backend.serializers.project.create_project_right import create_project_right
from pmt_backend.utils.convert_json_to_python import json_to_python
from pmt_backend.utils.to_generate_random_6_digits_otp import generate_six_digit_otp


class CreateProject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):

        user = request.user

        # Check for Credits
        company = user.company
        if company.credits < 5:
            send_fail_http_response('At least 5 credits is required to add a project. Buy more credits!')

        # Project Creation
        json_data = request.POST["project"]
        project_dict = json_to_python(json_data)
        image = request.FILES['project_image']
        project = create_project_serializer(user, project_dict, image)

        try:
            # To create 6 digit otp for a project
            project.six_digit_otp = generate_six_digit_otp()

            # Decreasing the credits after successfull creation of project
            company.credits -= 5
            project.credits_used += 5

            # project right for the teammember adding the project.
            member = TeamMember.objects.get(user=user)
            project.teammember_set.add(member)
            create_project_right(['Project Manager'], member, project)

            # customer creation
            customer = request.POST["customer"]
            json_data = customer
            customer_dict = json_to_python(json_data)
            if customer_dict['is_customer_info']:
                create_customer_serializer(project, customer_dict)

            # stake holder creation
            stakeholder_json_data = request.POST.get("stakeholders")
            if stakeholder_json_data:
                stakeholders_list = json_to_python(stakeholder_json_data)
                for stakeholder_dict in stakeholders_list:
                    if stakeholder_dict['is_stake_holder_info']:
                        create_stakeholder_serializer(project, stakeholder_dict)

            # Project specification
            json_data = request.POST["project_specification"]
            project_spec_dict = json_to_python(json_data)
            create_ProjectSpecification_serializer(project, project_spec_dict)

            gantt_id = request.POST.get('gantt_id')
            if gantt_id:
                gantt = Gantt.objects.get(pk=gantt_id)
            else:
                gantt = Gantt.objects.all().order_by('pk').first()
            project.gantt = gantt

            # DataBank
            create_databank_serializer(project, request)

            # Team Details
            json_data = request.POST["team"]
            teams = json_to_python(json_data)
            teams_list = teams["team_members"]
            for team in teams_list:
                member = TeamMember.objects.get(pk=team['id'])
                project.teammember_set.add(member)
                project.save()

                # create Notification and send mail for add new member to a project
                new_team_member_added_to_project(project, member)
                send_new_team_member_added_to_a_project.delay(project.id, member.name, team['roles'])

                # Adding project Rights
                roles = team['roles']
                create_project_right(roles, member, project)

            # Sub Contractor Details
            sub_contractor_details = request.POST["sub_contractor_details"]
            if sub_contractor_details == 'false':
                project.no_sub_contractor_info = True
            else:
                sub_contractor_details = json_to_python(sub_contractor_details)
                for subcontractor in sub_contractor_details:
                    company = Company.objects.get(pk=subcontractor['id'])
                    member = SubContractorTeamMember.objects.filter(user=company.account_owner).first()
                    project.subcontractorteammember_set.add(member)
                    project.save()

                    # create Notification and send mail for add new member to a project
                    sub_contractor_added(project, member.name)

                    # Adding project Rights
                    ProjectRight.objects.create(
                        sub_contractor_team_member=member,
                        project=project,
                        can_view=[x.upper() for x in subcontractor['stages']],
                        can_edit=[x.upper() for x in subcontractor['stages']],
                        can_work=[x.upper() for x in subcontractor['stages']],
                        can_approve=[x.upper() for x in subcontractor['stages']]
                    )

            # Create Events
            Create_Project_events(project, gantt.id)

            # create Notifications for adding new project
            new_project_created(user, project)

            # send mail for adding new project
            send_event_new_project_generated.delay(project.id)

            project.save()
            company.save()
        except:
            project.delete()
            return send_fail_http_response()

        return JsonResponse({"project_id": project.id, 'credits': company.credits})
