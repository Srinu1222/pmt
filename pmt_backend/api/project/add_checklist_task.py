from datetime import datetime

from django.http import JsonResponse

from pmt_backend.mail.mail_tasks.customers.task_added import send_task_added
from pmt_backend.models import Event, Customer, SubContractorTeamMember, TeamMember
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.serializers.Notifications.new_task_added import new_task_added
from pmt_backend.serializers.project.add_checklist_task import add_checklist_task_serializer
from pmt_backend.utils.formatting_time import formating_date_time


class AddChecklistTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()
        status = request.POST['status']
        spoc = request.POST['spoc']
        title = request.POST['title']
        due_date = request.POST.get('due_date', formating_date_time(datetime.now()))

        is_sub_contractor = request.POST.get('is_sub_contractor')
        if is_sub_contractor == 'true':
            spoc = SubContractorTeamMember.objects.get(pk=spoc)
        else:
            spoc = TeamMember.objects.get(pk=spoc)
        spoc_name = spoc.name

        id = add_checklist_task_serializer(event, status, spoc, title, due_date)

        # create Notification and send mail for add task
        new_task_added(request.user, event, title)
        send_task_added.delay(event.id, spoc_name, title)

        return JsonResponse({"id": id})
