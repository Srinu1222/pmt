from django.http import JsonResponse
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.project.get_stage_events import get_serialized_stage_events_data


class GetStageEvents(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        stage = request.GET['stage']
        project = Project.objects.get(pk=project_id)
        serialized_stage_events_data = get_serialized_stage_events_data(request.user, project, stage)
        return JsonResponse(serialized_stage_events_data)
