from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class DeleteStageEvent(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, event_id):
        try:
            Event.objects.get(pk=event_id).delete()
            return send_pass_http_response()
        except Event.DoesNotExist:
            return send_fail_http_response()
