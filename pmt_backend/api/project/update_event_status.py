from datetime import datetime

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event
from pmt_backend.schedulers.update_event_prerequisite import update_prerequisite_status_based_on_completed_event
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware


class ChangeEventStatus(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        status = request.POST['status']
        event = Event.objects.filter(pk=event_id).first()
        event.status = status.upper()

        if status.upper() == 'IN-PROGRESS':
            event.actual_start_time = datetime.now()

        elif status.upper() == 'FINISHED':

            # to check event is delayed or not
            if event.completed_time <= convert_naive_to_aware(datetime.now()):
                event.is_delayed = True

            # to update event status to finished
            event.actual_completed_time = datetime.now()
            if not event.actual_start_time:
                event.actual_start_time = datetime.now()

            # to update prerequisite status for finished event
            update_prerequisite_status_based_on_completed_event.delay(event_id)

        event.save()

        return JsonResponse({})
