from datetime import datetime
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.event_approved import send_event_approved
from pmt_backend.mail.mail_tasks.customers.event_completed import send_event_completed
from pmt_backend.mail.mail_tasks.customers.project_closed import send_project_closed
from pmt_backend.mail.mail_tasks.customers.stage_closed import send_stage_closed
from pmt_backend.models import Event, Customer, TeamMember
from pmt_backend.serializers.Notifications.event_approved import event_approved
from pmt_backend.serializers.Notifications.event_completed import event_completed
from pmt_backend.serializers.Notifications.project_closed import project_closed
from pmt_backend.serializers.Notifications.stage_closed import stage_closed


def get_next_stage(stage):
    if stage == 'PRE-REQUISITES':
        return 'APPROVALS'
    elif stage == 'APPROVALS':
        return 'ENGINEERING'
    elif stage == 'ENGINEERING':
        return 'PROCUREMENT'
    elif stage == 'PROCUREMENT':
        return 'MATERIAL HANDLING'
    elif stage == 'MATERIAL HANDLING':
        return 'CONSTRUCTION'
    elif stage == 'CONSTRUCTION':
        return 'SITE HAND OVER'
    else:
        return 'SITE HAND OVER'


class EventRequestApproval(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()

        event.approval_status = True
        event.actual_completed_time = datetime.now()
        event.status = "FINISHED"
        event.default_approver = TeamMember.objects.get(user=request.user)
        event.save()

        # Create Notification event approval
        event_approved(event.default_approver.user, event)

        # send Mail for event approval
        if event.default_spoc:
            spoc = event.default_spoc.name
        else:
            spoc = event.project.customer.customer_name
        send_event_approved.delay(event.id, spoc)

        # create Notification and send mail for event completion
        if event.status == 'FINISHED':
            event_completed(event)
            if event.default_spoc:
                spoc = event.default_spoc.name
            else:
                spoc = event.project.customer.customer_name

            send_event_completed.delay(event.id, spoc)

        # create Notification and send mail for stage completion
        finished_count = Event.objects.filter(project=event.project, stage=event.stage, status="FINISHED").count()
        total_count = Event.objects.filter(project=event.project, stage=event.stage).count()
        if finished_count == total_count:
            event.stage_status = "FINISHED"
            event.stage_completion_date = datetime.now()
            event_project = event.project
            event_project.stage_reached = get_next_stage(event.stage)
            event_project.save()
            event.save()
            stage_closed(request.user, event)
            send_stage_closed.delay(event.project.id)

        # create Notification and send mail for project completion
        project = event.project
        actual_count = project.event_set.all().count()
        completed_events_count = project.event_set.all().filter(status="FINISHED").count()
        if actual_count == completed_events_count:
            project.status = "CLOSED"
            project.project_completion_date = datetime.now()
            project.save()
            project_closed(request.user, project)
            send_project_closed.delay(project.id)
        return JsonResponse({"approved_by": event.default_approver.name})
