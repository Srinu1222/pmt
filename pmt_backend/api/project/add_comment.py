from django.http import JsonResponse

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.mail.mail_tasks.customers.new_comment_on_task import send_new_comment_on_task
from pmt_backend.models import Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.serializers.Notifications.new_comment_on_task import new_comment_on_task
from pmt_backend.serializers.project.add_comment_serializer import add_comment_serializer
from pmt_backend.utils.get_Jsonfield_dict import get_event_task_index


class add_comment(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()
        pre_requisite_id = request.POST.get('pre_requisite_id')
        checklist_id = request.POST.get('checklist_id')
        data = request.POST['data']
        file = request.FILES.get('file')

        if pre_requisite_id:
            serailized_response = add_comment_serializer(event, pre_requisite_id, data, file, type="PRE_REQUISITE")
            event_prerequisites = event.Prerequisites
            index = get_event_task_index(event_prerequisites, pre_requisite_id)
            task = event_prerequisites[index]['title']

        if checklist_id:
            serailized_response = add_comment_serializer(event, checklist_id, data, file, type="CHECKLIST")
            event_checklists = event.checklists
            index = get_event_task_index(event_checklists, checklist_id)
            task = event_checklists[index]['title']

        # create_notification and send mail for adding new comment
        new_comment_on_task(request.user, event)
        send_new_comment_on_task.delay(event.id, task, data)

        if not serailized_response:
            msg = "event id does not exist"
            return send_fail_http_response(msg=msg)
        else:
            return JsonResponse(serailized_response)
