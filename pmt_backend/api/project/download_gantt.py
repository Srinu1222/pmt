from datetime import datetime

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
import pyexcel as p

from pmt_backend.models import Project, Event, MonthlyReport, WeeklyReport, DailyReport
from pmt_backend.schedulers import generating_weekly_reports, generating_monthly_reports
from pmt_backend.schedulers.generating_monthly_reports import helper_for_generating_monthly_reports
from pmt_backend.schedulers.generating_weekly_reports import helper_for_generating_weekly_reports
from pmt_backend.serializers.project.generate_basic_gantt import generate_basic_gantt
from pmt_backend.serializers.project.generate_details_gantt import generate_details_gantt
from pmt_backend.utils.formatting_time import formating_date_time
from pmt_backend.utils.gantt_chart_csv import writing_to_csv
from pmt_backend.utils.formatting_time import formating_date_time
from pmt_backend.schedulers.generating_daily_reports import helper_for_generating_daily_reports


class DownloadGanttChart(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        project = Project.objects.get(pk=project_id)

        report_type = request.POST['report_type']

        if report_type == 'weekly':
            date = request.POST['date']
            helper_for_generating_weekly_reports(project, date)
            report = WeeklyReport.objects.filter(project=project
                                                 ).last()
            return JsonResponse({
                'report_url': report.report.url
            })
        if report_type == 'monthly':
            date = request.POST['date']
            helper_for_generating_monthly_reports(project, date)
            report = MonthlyReport.objects.filter(project=project).last()
            return JsonResponse({
                'report_url': report.report.url
            })

        if report_type == 'daily':
            date = request.POST['date']
            helper_for_generating_daily_reports(project, datetime.strptime(date, "%d/%m/%Y"))
            report = DailyReport.objects.filter(project=project).last()
            return JsonResponse({
                'report_url': report.report.url
            })

        if report_type == 'basic_gannt':
            generate_basic_gantt(project)
            return JsonResponse({
                'report_url': project.basic_gantt_report.url
            })
        else:
            generate_details_gantt(project)
            return JsonResponse({
                'report_url': project.details_gantt_report.url
            })
