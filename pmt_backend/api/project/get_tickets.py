from django.http import JsonResponse
from pmt_backend.models import Project, Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.serializers.project.get_tickets import get_serialized_tickets


class GetTicket(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        event = Event.objects.get(pk=event_id)

        tickets = get_serialized_tickets(event)

        return JsonResponse({"tickets": tickets})
