from django.http import JsonResponse
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, TeamMember
from pmt_backend.serializers.project.get_comments import get_comments_serializer


class GetComments(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        team_member = TeamMember.objects.filter(user=user).first()
        name = team_member.name
        event_id = request.GET['event_id']
        event = Event.objects.filter(pk=event_id).first()
        pre_requisite_id = request.GET.get('pre_requisite_id')
        checklist_id = request.GET.get('checklist_id')
        serialized_comment_data = get_comments_serializer(name, event, pre_requisite_id, checklist_id)
        if not serialized_comment_data:
            msg = "either event_id or pre_requisite_id or checklist_id does not exist"
            return send_fail_http_response(msg=msg)
        else:
            return JsonResponse(serialized_comment_data)
