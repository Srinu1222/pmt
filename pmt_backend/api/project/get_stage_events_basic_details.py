from django.http import JsonResponse
from pmt_backend.models import Project, Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class GetStageEventsBasicDetails(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        stage = request.GET['stage']
        project = Project.objects.get(pk=project_id)
        event_queryset = Event.objects.filter(stage=stage, project=project)
        stage_events_list = []
        for event in event_queryset:
            stage_events_list.append(
                {
                    'id': event.id,
                    'name': event.name,
                    'event_status': event.status,
                    'is_event_delayed': event.is_delayed,
                    'delayed_reason': event.delay_reason if event.delay_reason else '',
                    'is_delayed_reason_provided': event.is_delayed_reason_provided
                }
            )
        return JsonResponse({'stage_events_list': stage_events_list})
