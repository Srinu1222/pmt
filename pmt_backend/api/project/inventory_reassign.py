from django.http import JsonResponse

from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Inventory, Project



class InventoryReAssign(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        new_project_id = request.POST['new_project_id']
        inventory_id = request.POST['inventory_id']

        new_project = Project.objects.get(pk=new_project_id)
        inventory = Inventory.objects.filter(pk=inventory_id).first()

        inventory.project = new_project
        inventory.save()

        return JsonResponse({})

