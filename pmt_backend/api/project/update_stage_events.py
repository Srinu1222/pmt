from datetime import datetime, timedelta

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, TeamMember, ProjectRight, SubContractorTeamMember, Company
from pmt_backend.serializers.project.get_stage_events import get_serialized_stage_events_data
from pmt_backend.utils.convert_json_to_python import json_to_python


class ChangeStageEvent(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def patch(self, request, event_id):
        event = Event.objects.filter(pk=event_id).first()

        stage = request.POST['stage']
        team_member_id = request.POST.get('team_member_id')
        is_sub_contractor = request.POST.get('is_sub_contractor')
        days_needed = request.POST.get('days_needed')
        start_date = request.POST.get('start_date')
        sub_contractor_mode = request.POST.get('sub_contractor_mode')

        # Additional Rights to Team Members for can_work to true
        project = event.project
        event_stage = event.stage
        additional_members = request.POST.get('additional_members')
        additional_members = json_to_python(additional_members) if additional_members else None

        if additional_members:
            event.additional_members = additional_members

        if start_date:
            event.start_time = datetime.strptime(start_date, "%d/%m/%Y") + timedelta(hours=5, minutes=30)
            event.completed_time = event.start_time + timedelta(days=event.days_needed)

        if team_member_id:

            # Update all event checklists spoc because of event spoc changed
            event_checklists = event.checklists
            for checklist in event_checklists:
                checklist['spoc'] = team_member_id

            if sub_contractor_mode == 'true':
                member = TeamMember.objects.get(pk=team_member_id)
                team_member, is_new = SubContractorTeamMember.objects.get_or_create(user=member.user, name=member.name,
                                                                                    department=member.department)
                event.sub_contractor_spoc = team_member
                team_member.projects.add(event.project)

                project_right_member = ProjectRight.objects.filter(
                    sub_contractor_team_member=event.sub_contractor_approver, project=event.project).first()

                ProjectRight.objects.create(
                    sub_contractor_team_member=team_member,
                    project=event.project,
                    can_view=project_right_member.can_view,
                    can_edit=project_right_member.can_edit,
                    can_work=project_right_member.can_work,
                )
            else:
                if is_sub_contractor == 'true':
                    member = SubContractorTeamMember.objects.filter(pk=team_member_id).first()
                    event.sub_contractor_spoc = member
                    event.sub_contractor_approver = member
                    event.is_sub_contractor_event = True
                else:
                    team_member = TeamMember.objects.get(pk=team_member_id)
                    event.default_spoc = team_member

                    # giving special right for a spoc change
                    project_right = ProjectRight.objects.filter(team_member=team_member, project=event.project).first()
                    project_right_stages_for_can_work = project_right.can_work

                    if stage not in project_right_stages_for_can_work:
                        project_right_stages_for_can_work.append(stage)
                        project_right.save()

        if days_needed:
            event.days_needed = days_needed
            event.completed_time = event.start_time + timedelta(days=int(days_needed))

        event.save()

        serialized_stage_events = get_serialized_stage_events_data(request.user, event.project, stage)

        return JsonResponse(serialized_stage_events)
