from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event


class UpdateCustomerApprovalRequest(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()
        event.customer_approval_required = True
        event.save()
        return JsonResponse({})
