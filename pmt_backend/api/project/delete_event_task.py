from django.http import JsonResponse

from pmt_backend.models import Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.utils.get_Jsonfield_dict import get_event_task_index


class DeleteEventTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request):
        event_id = request.GET['event_id']
        event = Event.objects.get(pk=event_id)
        pre_requisite_id = request.GET.get('pre_requisite_id')
        checklist_id = request.GET.get('checklist_id')

        if pre_requisite_id:
            event_pre_requisites = event.Prerequisites
            index = get_event_task_index(event_pre_requisites, pre_requisite_id)
            event_pre_requisites.pop(index)
        elif checklist_id:
            event_checklists = event.checklists
            index = get_event_task_index(event_checklists, checklist_id)
            event_checklists.pop(index)

        event.save()

        return JsonResponse({})
