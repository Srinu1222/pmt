from django.http import JsonResponse

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.project.documents import get_document_serializer


class GetProjectDocs(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        is_image = request.GET.get('is_image')
        is_image = True if is_image == 'true' else False

        project = Project.objects.get(pk=project_id)
        serialized_Document_data = get_document_serializer(project, is_image, request.user)

        # if serialized_Document_data:
        #     msg = "project id does not exist"
        #     return send_fail_http_response(msg=msg)
        # else:
        return JsonResponse(serialized_Document_data)

