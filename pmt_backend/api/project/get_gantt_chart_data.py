from datetime import datetime

from django.http import JsonResponse
from pmt_backend.models import Project, Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.utils.formatting_time import formating_date_time


def actual_date(date):
    if date:
        return formating_date_time(date)
    else:
        return "Not yet started"


class GetGanttChartData(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        project_id = request.GET['project_id']
        project = Project.objects.filter(pk=project_id).first()

        stages = ['PRE-REQUISITES', 'APPROVALS', 'ENGINEERING',
                  'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER']

        project_events_list = []
        for stage in stages:
            stage_events = Event.objects.filter(project=project, stage=stage).order_by('stage', 'event_order_no')
            for event in stage_events:
                actual_start_date = actual_date(event.actual_start_time)
                actual_completed_time = actual_date(event.actual_completed_time)

                project_events_list.append(
                    {
                        'status': event.status,
                        "stage": event.stage,
                        "name": event.name,
                        "start_time": formating_date_time(event.start_time),
                        "completed_time": formating_date_time(event.completed_time)
                    }
                )

                if event.status == 'IN-PROGRESS':
                    project_events_list.append(
                        {
                            'status': event.status,
                            "stage": event.stage,
                            "name": event.name,
                            "start_time": actual_start_date,
                            "completed_time": formating_date_time(datetime.now())
                        }
                    )
                else:
                    project_events_list.append(
                        {
                            'status': event.status,
                            "stage": event.stage,
                            "name": event.name,
                            "start_time": actual_start_date,
                            "completed_time": actual_completed_time
                        }
                    )
        return JsonResponse({"project_events_list": project_events_list})
