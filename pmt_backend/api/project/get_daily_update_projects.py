from django.http import JsonResponse
from pmt_backend.models import Project, TeamMember, DailyUpdate
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from datetime import datetime


class GetDailyUpdateProject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        member = TeamMember.objects.get(user=request.user)
        projects = member.projects.filter(status='ACTIVE')
        project_list = []

        for project in projects:
            morning_eight = datetime(
                year=datetime.now().year,
                month=datetime.now().month,
                day=datetime.now().day,
                hour=8
            )

            update_required = True
            if DailyUpdate.objects.filter(project=project, member=member, date__gte=morning_eight):
                update_required = False

            project_list.append({'id': project.id, 'title': project.name, 'update_required': update_required})

        return JsonResponse({'projects': project_list})


