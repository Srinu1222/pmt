from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.event_awaiting_approval import send_event_awaiting_approval
from pmt_backend.models import Event, Customer
from pmt_backend.serializers.Notifications.event_awaiting_approval import event_awaiting_approval


class IsEventRequested(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()

        event.is_requested = True
        event.save()

        # create Notification
        user = event.default_approver.user
        event_awaiting_approval(user, event)

        # send mail for event_awaiting_approval
        if event.default_spoc:
            spoc = event.default_spoc.name
        else:
            spoc = event.project.customer.customer_name

        send_event_awaiting_approval.delay(event.id, spoc)

        return JsonResponse({})

