from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, TeamMember, SubContractorTeamMember
from pmt_backend.utils.get_Jsonfield_dict import get_event_task_dict, get_event_task_index


class ChangePreRequisiteTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        pre_requisite_id = request.POST['pre_requisite_id']
        event = Event.objects.filter(pk=event_id).first()
        status = request.POST.get('status').capitalize()
        spoc = request.POST['spoc']
        is_sub_contractor = request.POST['is_sub_contractor']

        event_prerequisites = event.Prerequisites
        pre_requisite_dict = get_event_task_dict(event_prerequisites, pre_requisite_id)
        pre_requisiste_index = get_event_task_index(event_prerequisites, pre_requisite_id)

        if is_sub_contractor == 'false':
            pre_requisite_dict['spoc'] = spoc

        if status == 'True':
            event.Prerequisites[pre_requisiste_index]['status'] = True
            if is_sub_contractor == 'false':
                member = TeamMember.objects.get(user=request.user)
                event.Prerequisites[pre_requisiste_index]['spoc'] = member.id

        event.save()

        return JsonResponse({})
