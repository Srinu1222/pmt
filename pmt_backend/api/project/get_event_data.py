from django.http import JsonResponse

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.project.get_event_data import get_serialized_event_data


class GetEvent(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, event_id):
        if request.GET.get('sub_contractor_mode') == 'true':
            is_sub_contractor = True
        else:
            is_sub_contractor = False

        event = Event.objects.filter(pk=event_id).first()
        serialized_event_data = get_serialized_event_data(request.user, event, is_sub_contractor)

        if not serialized_event_data:
            msg = "event id does not exist"
            return send_fail_http_response(msg=msg)
        else:
            return JsonResponse(serialized_event_data)
