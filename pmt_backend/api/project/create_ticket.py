from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.new_ticket_raised import send_new_ticket_raised
from pmt_backend.models import Event, Ticket, TeamMember
from pmt_backend.serializers.Notifications.new_ticket_raised import ticket_raised


class CreateTicket(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.filter(pk=event_id).first()
        subject = request.POST['subject']
        comment = request.POST.get('comment')
        assigned_to_id = request.POST['assigned_to_id']

        raised_by = request.user
        assigned_to = TeamMember.objects.get(pk=assigned_to_id)

        ticket = Ticket.objects.create(
                subject=subject, raised_by=raised_by,
                assigned_to=assigned_to, event=event
        )
        if comment:
            ticket.comment = comment
            ticket.save()

        # create notification and send mail for new ticket raised
        ticket_raised(event, ticket)
        raised_by = TeamMember.objects.get(user=request.user)
        send_new_ticket_raised.delay(event.id, raised_by.name, comment)

        return JsonResponse({"ticket_id": ticket.id})
