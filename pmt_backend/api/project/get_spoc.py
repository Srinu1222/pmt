from django.http import JsonResponse
from pmt_backend.models import Project, ProjectRight
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class GetSpoc(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        project_planning = request.GET.get('project_planning')
        stage = request.GET.get('stage')
        is_sub_contractor = request.GET.get('is_sub_contractor')

        project = Project.objects.get(pk=project_id)
        members_list = []

        if is_sub_contractor == 'true':
            sub_contractor_team_members = project.subcontractorteammember_set.all()
            for member in sub_contractor_team_members:
                members_list.append(
                    {
                        'name': member.name,
                        'id': member.id,
                        'is_sub_contractor': True
                    }
                )
        else:
                team_members = project.teammember_set.all()
                for team_member in team_members:
                    members_list.append(
                        {
                            'name': team_member.name,
                            'id': team_member.id,
                            'is_sub_contractor': False
                        }
                    )

        if project_planning:
            sub_contractor_team_members = project.subcontractorteammember_set.all()
            for member in sub_contractor_team_members:
                project_right = ProjectRight.objects.filter(project=project,
                                                            sub_contractor_team_member=member).first()
                can_view_rights = project_right.can_view if project_right else []
                rights = map(lambda x: x.upper(), can_view_rights)
                if stage.upper() in rights:
                    members_list.append(
                        {
                            'name': member.user.company.name,
                            'id': member.id,
                            'is_sub_contractor': True
                        }
                    )

        return JsonResponse({"members": members_list})
