from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, Project


class GetSpecificationEvents(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        stage = request.GET['stage']
        project_id = request.GET['project_id']
        project = Project.objects.filter(pk=project_id).first()

        stage_events = Event.objects.filter(stage=stage, project=project)
        list_of_event_names = []
        for event in stage_events:
            event_checklists = event.checklists
            for checklist in event_checklists:
                if checklist.get("specifications"):
                    list_of_event_names.append({'id': event.id, 'name': event.name})

        return JsonResponse({'list_of_event_names': list_of_event_names})
