from django.http import JsonResponse

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Event
from rest_framework.views import APIView
from rest_framework import authentication, permissions


def finding_comment_id_in_comments_list(comments, comment_id):
    deleted_index = None

    for index, comment in enumerate(comments):
        if int(comment['id']) == int(comment_id):
            deleted_index = index
            break

    return deleted_index


class DeleteComment(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        event = Event.objects.get(pk=event_id)
        comment_id = request.POST['comment_id']
        pre_requisite_id = request.POST.get('pre_requisite_id')
        checklist_id = request.POST.get('checklist_id')

        if pre_requisite_id:
            event_pre_requisites = event.Prerequisites
            for index, requisite in enumerate(event_pre_requisites):
                if int(pre_requisite_id) == int(requisite['id']):
                    found = index
                    break
            pre_requisite_dict = event_pre_requisites[found]
            comments = pre_requisite_dict['comments']
            deleted_index = finding_comment_id_in_comments_list(comments, comment_id)

        elif checklist_id:
            event_checklists = event.checklists
            for index, checklist in enumerate(event_checklists):
                if int(checklist_id) == int(checklist['id']):
                    found = index
                    break
            checklist_dict = event_checklists[found]
            comments = checklist_dict['comments']
            deleted_index = finding_comment_id_in_comments_list(comments, comment_id)

        if deleted_index or deleted_index == 0:
            comments.pop(deleted_index)
            event.save()
            return send_pass_http_response("successfully deleted")
        else:
            return send_fail_http_response("comment_id does not exist")


