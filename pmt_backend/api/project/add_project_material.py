from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Inventory, Project


class AddProjectMaterial(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        project = Project.objects.get(pk=project_id)
        category = request.POST['category']
        preferred_brands = request.POST['product_name']
        quantity = request.POST['quantity']
        quantity_format = request.POST.get('quantity_format')
        location = request.POST['location']
        rate = request.POST['rate']
        value = request.POST['value']
        upload_po_file = request.FILES.get('upload_po_file')
        specification = request.POST['specification']

        material = Inventory.objects.create(
            company=request.user.company,
            project=project,
            material_name=category,
            preferred_brands=preferred_brands,
            stock_quantity=float(quantity),
            location=location,
            target_price=rate,
            value=value,
            upload_po_file=upload_po_file,
            status='PO_PLACED',
            specifications=specification,
            quantity_format=quantity_format
        )
        return JsonResponse({"id": material.id})
