from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.new_comment_on_ticket import send_new_comment_on_ticket
from pmt_backend.models import Ticket, TeamMember, Customer
from pmt_backend.serializers.Notifications.new_comment_on_ticket import new_comment_on_ticket


class UpdateTicketComment(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        ticket_id = request.POST['ticket_id']
        comment = request.POST['comment']

        ticket = Ticket.objects.get(pk=ticket_id)
        ticket.comment = comment
        ticket.save()

        # create notification and send mail for new ticket comment raised
        new_comment_on_ticket(ticket.event, ticket)
        raised_by = TeamMember.objects.get(user=request.user).name
        if ticket.event.default_spoc:
            spoc = ticket.event.default_spoc.name
        else:
            spoc = ticket.event.project.customer.customer_name
        send_new_comment_on_ticket.delay(ticket.event.id, spoc, raised_by, comment)

        return JsonResponse({})
