from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView

from pmt_backend.models import Gantt
from pmt_backend.utils.making_project_spec_dict import making_project_spec_dict


class GetGanttSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        gantt = Gantt.objects.get(pk=request.GET['gantt_id'])
        project_specification = making_project_spec_dict(gantt)
        return JsonResponse({'project_specification': project_specification})
