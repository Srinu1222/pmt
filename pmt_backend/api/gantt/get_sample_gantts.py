from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView

from pmt_backend.serializers.gantt.get_sample_gantt import get_sample_gantt_serializer


class GetSampleGantt(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        company = user.company
        serialized_gantt_list = get_sample_gantt_serializer(company)
        return JsonResponse({'gantt_list': serialized_gantt_list})
