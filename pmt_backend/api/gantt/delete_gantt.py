from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Gantt


class DeleteGantt(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        try:
            gantt_id = request.POST['gantt_id']
            Gantt.objects.get(pk=gantt_id).delete()
            return send_pass_http_response()
        except Gantt.DoesNotExist:
            return send_fail_http_response()
