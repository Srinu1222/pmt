from django.core.files import File
import os

from django.http import JsonResponse
from openpyxl import load_workbook
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Gantt


class UpdateGanttEvent(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        gantt = Gantt.objects.get(pk=request.POST['gantt_id'])
        index = request.POST['index']
        default_spoc = request.POST.get('default_spoc')
        days_needed = request.POST.get('days_needed')
        start_date = request.POST.get('start_date')

        gantt_file = gantt.gantt_file

        workbook = load_workbook(filename=gantt_file)
        sheet = workbook.active

        if default_spoc:
            sheet[index][4].value = default_spoc
        if days_needed:
            sheet[index][9].value = days_needed
        if start_date:
            sheet[index][8].value = start_date

        workbook.save('output.xlsx')

        gantt.gantt_file = File(open('output.xlsx', 'rb'))
        gantt.save()

        os.remove('output.xlsx')

        return JsonResponse({})
