from django.http import JsonResponse
from openpyxl import load_workbook
from pmt_backend.models import Gantt
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.utils.gantt_file_stage_indexing import gantt_file_stage_indexing
from pmt_backend.utils.making_project_spec_dict import making_project_spec_dict
from pmt_backend.utils.read_impact_file import read_impact_file


def get_row_data_in_a_gantt_file(gantt_file, index):
    workbook = load_workbook(filename=gantt_file)
    sheet = workbook.active
    return {
        'index': index,
        'name': sheet[index][1].value,
        'default_spoc': sheet[index][4].value,
        'pre_requisite': [i[3:] for i in sheet[index][3].value.split("\n") if i != ""] if sheet[index][
            3].value else [],
        'checklist': [i[3:] for i in sheet[index][6].value.split("\n") if i != ""] if sheet[index][
            6].value else [],
        'start_time': sheet[index][8].value,
        'days_needed': sheet[index][9].value,
        'event_order_no': sheet[index][13].value,
    }


def helper_fun_to_append_events_based_on_spec(selected_spec, actual_spec, gantt_file, list_of_indexes_for_the_stage,
                                              events_list):
    for item in selected_spec:
        for index in actual_spec[item]:
            if index in list_of_indexes_for_the_stage:
                events_list.append(get_row_data_in_a_gantt_file(gantt_file, index))


def helper_fun_for_termination(impact_file, index):
    workbook = load_workbook(filename=impact_file)
    sheet = workbook.active
    return [sheet[index][1].value] if type(sheet[index][1].value) == int else list(map(int, sheet[index][
        1].value.split(',')))


class GetGanttEvents(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        events_list = []
        gantt_id = request.GET['gantt_id']
        stage = request.GET['stage']
        gantt = Gantt.objects.get(pk=gantt_id)
        gantt_file = gantt.gantt_file
        impact_file = gantt.project_impact

        # gantt_file
        workbook = load_workbook(filename=gantt_file)
        sheet = workbook.active

        # This is for the index is in which stage
        stage_indexes_dict = gantt_file_stage_indexing(sheet)

        list_of_indexes_for_the_stage = stage_indexes_dict[stage]

        for index in list_of_indexes_for_the_stage:
            if sheet[index][2].value == 'Yes':
                events_list.append(
                    {
                        'index': index,
                        'name': sheet[index][1].value,
                        'default_spoc': sheet[index][4].value,
                        'pre_requisite': [i[3:] for i in sheet[index][3].value.split("\n") if i != ""] if sheet[index][
                            3].value else [],
                        'checklist': [i[3:] for i in sheet[index][6].value.split("\n") if i != ""] if sheet[index][
                            6].value else [],
                        'start_time': sheet[index][8].value,
                        'days_needed': sheet[index][9].value,
                        'event_order_no': sheet[index][13].value,
                    }
                )

        # impact file
        all_drawings = read_impact_file(impact_file, 2, 17)
        all_components = read_impact_file(impact_file, 22, 47)
        all_accessories = read_impact_file(impact_file, 51, 71)

        project_specification = making_project_spec_dict(gantt)

        helper_fun_to_append_events_based_on_spec(
            project_specification['selected_drawings'],
            all_drawings,
            gantt_file,
            list_of_indexes_for_the_stage,
            events_list
        )

        helper_fun_to_append_events_based_on_spec(
            project_specification['selected_components'],
            all_components,
            gantt_file,
            list_of_indexes_for_the_stage,
            events_list
        )

        helper_fun_to_append_events_based_on_spec(
            project_specification['selected_accessories'],
            all_accessories,
            gantt_file,
            list_of_indexes_for_the_stage,
            events_list
        )

        if project_specification['selected_termination'] == 'LT':
            selected_inverter_to_ACDB = project_specification['selected_inverter_to_ACDB']
            for i in selected_inverter_to_ACDB:
                if i == 'Cable Tray':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 76)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'Cable Trench':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 77)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'HDPE Pipes':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 78)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

            selected_ACDB_to_Transformer = project_specification['selected_ACDB_to_Transformer']
            for i in selected_ACDB_to_Transformer:
                if i == 'Cable Tray':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 81)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'Cable Trench':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 82)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'HDPE Pipes':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 83)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

        elif project_specification['selected_termination'] == 'HT':
            selected_inverter_to_ACDB = project_specification['selected_inverter_to_ACDB']
            for i in selected_inverter_to_ACDB:
                if i == 'Cable Tray':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 88)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'Cable Trench':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 89)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'HDPE Pipes':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 90)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

            selected_ACDB_to_Transformer = project_specification['selected_ACDB_to_Transformer']

            for i in selected_ACDB_to_Transformer:
                if i == 'Cable Tray':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 93)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'Cable Trench':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 94)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'HDPE Pipes':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 95)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

            selected_Transformer_to_HT_Panel = project_specification['selected_Transformer_to_HT_Panel']
            for i in selected_Transformer_to_HT_Panel:
                if i == 'Cable Tray':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 98)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'Cable Trench':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 99)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

                if i == 'HDPE Pipes':
                    list_of_event_indexes = helper_fun_for_termination(impact_file, 100)
                    for j in list_of_event_indexes:
                        if j in list_of_indexes_for_the_stage:
                            events_list.append(get_row_data_in_a_gantt_file(gantt_file, j))

        events_list.sort(key=lambda x: x['event_order_no'])

        return JsonResponse({'events_list': events_list})
