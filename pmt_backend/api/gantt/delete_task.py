from django.http import JsonResponse
from openpyxl import load_workbook
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Gantt
from django.core.files import File
import os


class DeleteTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        gantt_id = request.POST['gantt_id']
        index = int(request.POST['index'])
        checklist_index = request.POST.get('checklist_index')
        pre_requisite_index = request.POST.get('pre_requisite_index')
        gantt = Gantt.objects.get(pk=gantt_id)
        gantt_file = gantt.gantt_file
        workbook = load_workbook(filename=gantt_file)
        sheet = workbook.active

        if checklist_index:
            checklists = [i for i in sheet[index][6].value.split("\n") if i != ""]
            checklists.pop(int(checklist_index))
            sheet[index][6].value = "\n".join(checklists)

        if pre_requisite_index:
            pre_requisites = [i for i in sheet[index][3].value.split("\n") if i != ""]
            pre_requisites.pop(int(pre_requisite_index))
            sheet[index][3].value = "\n".join(pre_requisites)

        workbook.save('output.xlsx')

        gantt.gantt_file = File(open('output.xlsx', 'rb'))
        gantt.save()

        os.remove('output.xlsx')

        return JsonResponse({})
