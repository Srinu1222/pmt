from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Gantt


class UpdateRemarks(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        gantt_id = request.POST['gantt_id']
        remarks = request.POST['remarks']
        gantt = Gantt.objects.get(pk=gantt_id)
        gantt.remarks = remarks
        gantt.save()
        return JsonResponse({})
