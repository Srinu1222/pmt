from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView

from pmt_backend.models import Gantt, Project
from pmt_backend.utils.formatting_time import formating_date_time
from datetime import datetime


class GetGanttDetailsForProject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        gantt = Gantt.objects.get(pk=request.GET['gantt_id'])
        remarks = gantt.remarks

        gantt_spec = {
            'gantt_id': gantt.id,
            'created_by': gantt.created_by.name if gantt.created_by else '',
            'gantt_name': gantt.name,
            'date': formating_date_time(gantt.date),
            'remarks': remarks if remarks else '',
            'gantt_image': gantt.gantt_image.url if gantt.gantt_image else '',
            'tags': gantt.tags

        }

        return JsonResponse({'gantt_spec': gantt_spec})
