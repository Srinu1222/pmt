from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Gantt
from pmt_backend.utils.update_specifications import update_specifications


class UpdateProjectSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        gantt_id = request.POST['gantt_id']
        gantt = Gantt.objects.get(pk=gantt_id)

        remarks = request.POST.get('remarks')
        if remarks:
            gantt.remarks = remarks
            gantt.save()

        selected_drawings = request.POST.get('selected_drawings')
        selected_accessories = request.POST.get('selected_accessories')
        selected_components = request.POST.get('selected_components')
        selected_area_of_installation = request.POST.get('selected_area_of_installation')
        selected_termination = request.POST.get('selected_termination')
        selected_inverter_to_ACDB = request.POST.get('selected_inverter_to_ACDB')
        selected_ACDB_to_Transformer = request.POST.get('selected_ACDB_to_Transformer')
        selected_Transformer_to_HT_Panel = request.POST.get('selected_Transformer_to_HT_Panel')

        project_specification = {
            'selected_project_types': ['Rooftop'],
            'selected_drawings': selected_drawings.split(','),
            'selected_accessories': selected_accessories.split(','),
            'selected_components': selected_components.split(','),
            'selected_area_of_installation': selected_area_of_installation.split(',') if selected_area_of_installation else [],
            'selected_termination': selected_termination.split(','),
            'selected_inverter_to_ACDB': selected_inverter_to_ACDB.split(','),
            'selected_ACDB_to_Transformer': selected_ACDB_to_Transformer.split(','),
            'selected_Transformer_to_HT_Panel': selected_Transformer_to_HT_Panel.split(',')
        }

        update_specifications(gantt, project_specification)

        return JsonResponse({})
