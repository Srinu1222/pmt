from django.http import JsonResponse
from pmt_backend.models import Inventory, Event, Databank, Document
from openpyxl import load_workbook
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.events.create_events_for_material_handling import create_events_for_material_handling_stage
from pmt_backend.utils.dict_for_linking_matrial_name_with_event import component_name_with_event


class UploadBoQFile(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        project_id = request.POST['project_id']
        project = Project.objects.filter(pk=project_id).first()
        file = request.FILES['boq_file']
        description = request.POST.get('description', '')

        workbook = load_workbook(filename=file)
        sheet = workbook.active

        inventory_list = []
        for row in sheet[2:49]:
            row_list = []
            for shell in row:
                row_list.append(shell.value)
            if row_list[1] != 'None':
                inventory_list.append(tuple(row_list))

        inventory_objs_list = []
        for inventory in inventory_list:
            material_name = inventory[0]
            preferred_brands = inventory[1]
            specifications = inventory[2]
            stock_quantity = float(inventory[3])
            target_delivery_date = inventory[4]
            target_price = inventory[5]
            pdi_needed = inventory[6]

            # inventory_objs_list.append(
            #     Inventory(
            #         project=project, material_name=material_name, preferred_brands=preferred_brands,
            #         specifications=specifications, stock_quantity=stock_quantity,
            #         target_delivery_date=target_delivery_date, target_price=target_price,
            #         pdi_needed=pdi_needed
            #     )
            # )

            # procurement specifications will be prefilled according to boq sheet
            event = Event.objects.filter(project=project, selected_names__in=material_name).first()
            if event:
                event_pre_requisites = event.Prerequisites[0]
                specification = {
                    'stock_quantity': stock_quantity,
                    'final_price': target_price,
                    'target_delivery_date': target_delivery_date,
                    'final_brands': preferred_brands,
                    'material_name': material_name,
                    'specifications': specifications
                }
                specifications = event_pre_requisites.get('specifications')
                if specifications:
                    specifications.append(specification)
                else:
                    event_pre_requisites['specifications'] = [].append(specification)
                event.save()

        # Inventory.objects.bulk_create(inventory_objs_list, ignore_conflicts=True)

        # Events Creation based on pdi
        inventory_queryset = Inventory.objects.filter(project=project, pdi_needed='Yes')
        material_names = [inventory.material_name for inventory in inventory_queryset]
        create_events_for_material_handling_stage(project, material_names)

        project.upload_boq_file_status = True
        project.save()

        databank = Databank.objects.get(project=project)
        Document.objects.create(databank=databank, event=event, title='Initial BOQ', file=file, description=description)

        return JsonResponse({})
