from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event


class GetChecklistSpecification(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        checklist_id = request.GET['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        specifications = []
        for checklist in event_checklists:
            if str(checklist['id']) == str(checklist_id):
                specifications = checklist['specifications'] if checklist.get("specifications") else []
                break

        return JsonResponse({"specifications": specifications})
