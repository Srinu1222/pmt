from datetime import datetime

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Event, Inventory, Finance
from pmt_backend.utils.convert_json_to_python import json_to_python


class CreateProcurementSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        try:
            component_name = event.selected_names[0]
        except:
            send_fail_http_response(msg='event selected name not there')

        target_price = request.POST['target_price']
        stock_quantity = float(request.POST['stock_quantity'])
        preferred_brand = request.POST['preferred_brand']
        delivery_date = request.POST['delivery_date']
        delivery_date_object = datetime.strptime(delivery_date, "%d/%m/%Y")
        specifications = request.POST['specifications']

        # Inventory.objects.create(
        #     project=event.project,
        #     stock_quantity=stock_quantity,
        #     target_price=float(target_price),
        #     target_delivery_date=delivery_date_object,
        #     preferred_brands=preferred_brand,
        #     material_name=event.name,
        #     specifications=specifications,
        #     status='PO_PLACED'
        # )

        specifications = [
            {
                'stock_quantity': stock_quantity,
                'target_price': target_price,
                'target_delivery_date': delivery_date,
                'preferred_brands': preferred_brand,
                'material_name': component_name,
                'specifications': specifications
            }
        ]

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break

        event.save()

        # to update or create finance actual budget side price
        finance = Finance.objects.filter(project=event.project).first()
        if finance:
            list_of_actual_supply_side_items = finance.actual_supply_side
            for item in list_of_actual_supply_side_items:
                if item['component'] == component_name:
                    item['basic_price'] = target_price
                    break
            finance.save()

        return JsonResponse({"specifications": specifications})
