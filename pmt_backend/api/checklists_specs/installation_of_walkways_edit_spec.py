from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, Inventory
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.utils.reading_units_module import all_components_unit_dict


class CreateInstallationOfWalkways(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        date = request.POST['date']
        consumed = request.POST['percentage']
        comment = request.POST['comment']

        material_names = event.selected_names
        quantity_units = ''
        if material_names:
            inventory = Inventory.objects.filter(
                              project=event.project, material_name__in=material_names).first()
            if inventory:
                in_stock = inventory.stock_quantity
                used_stock = inventory.used_quantity
                consumed_quantity = float(consumed)

                if in_stock >= consumed_quantity:
                    inventory.stock_quantity = in_stock - consumed_quantity
                    inventory.used_quantity = used_stock + consumed_quantity
                    inventory.status = 'USED'
                    component_units_dict = all_components_unit_dict.get(inventory.material_name)
                    if component_units_dict:
                        format_dict = component_units_dict.get(inventory.quantity_format,
                                                               component_units_dict['format1'])
                        if format_dict:
                            quantity_units = format_dict['unit_format']
                        else:
                            quantity_units = ''
                    else:
                        quantity_units = ''
                else:
                    return send_fail_http_response({'error': 'Not enough quantity in stock'})

                # To Update inventory status
                inventory.save()
            else:
                return send_fail_http_response(
                    {'error': 'Procurement Final Specification is  not filled'})

        specification = {
                'date': date,
                'percentage': consumed + ' ' + quantity_units,
                'comment': comment
            }

        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                is_specifications = checklist.get("specifications")
                if is_specifications:
                    is_specifications.append(specification)
                    updated_specs_list = is_specifications
                else:
                    checklist['specifications'] = [specification]
                    updated_specs_list = [specification]
                break

        event.save()

        return JsonResponse({"specifications": updated_specs_list, 'some': [event.selected_names, event.pk]})
