from datetime import datetime

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Event, Inventory, Finance, Databank, Document
from pmt_backend.utils.formatting_time import formating_date_time


class CreateProcurementFinalSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):

        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']
        purchase_order = request.FILES.get('purchase_order')
        description = request.POST.get('description', '')

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        try:
            component_name = event.selected_names[0]
        except:
            return send_fail_http_response(msg='component name is not there in event selected names')

        final_price = request.POST['final_price']
        stock_quantity = request.POST['stock_quantity']
        quantity_format = request.POST.get('quantity_format')
        final_brand = request.POST['final_brand']
        delivery_date = request.POST['delivery_date']
        delivery_date_object = datetime.strptime(delivery_date, "%d/%m/%Y")
        specifications = request.POST['specifications']

        Inventory.objects.create(
            company=request.user.company,
            project=event.project,
            stock_quantity=float(stock_quantity),
            target_price=final_price,
            target_delivery_date=delivery_date_object,
            preferred_brands=final_brand,
            material_name=component_name,
            specifications=specifications,
            status='PO_PLACED',
            quantity_format=quantity_format
        )

        if purchase_order:
            databank_obj = Databank.objects.get(project=event.project)
            Document.objects.create(databank=databank_obj, event=event, title='Purchase Order' + '(' + (
                event.selected_names[0] if event.selected_names else '') + ')', file=purchase_order, description=description)

        specifications = [
            {
                'stock_quantity': stock_quantity,
                'final_price': final_price,
                'target_delivery_date': delivery_date,
                'final_brands': final_brand,
                'material_name': component_name,
                'specifications': specifications
            }
        ]

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                checklist['status'] = True
                checklist['completed_time'] = formating_date_time(datetime.now())
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break
        event.save()

        # to update or create finance actual budget side price
        finance, is_created = Finance.objects.get_or_create(project=event.project)
        if is_created:
            finance.actual_supply_side = []
            finance.actual_service_side = []
            finance.budget_supply_side = []
            finance.budget_service_side = []

        list_of_actual_supply_side_items = finance.actual_supply_side
        is_available = False
        is_new = False

        for item in list_of_actual_supply_side_items:
            if item['component'] == component_name:
                item['total_price'] = final_price
                is_available = True
                break
            elif item['component'] == 'Structures' and (
                    component_name == 'Module Mounting Structures with Accessories _ Metal Sheet' or
                    component_name == 'Module Mounting Structures with Accessories _ Ground Mount' or
                    component_name == 'Module Mounting Structures with Accessories _ RCC'):
                item['total_price'] = final_price
                is_available = True
        else:
            list_of_actual_supply_side_items.append(
                {
                    'component': component_name,
                    'total_price': final_price
                }
            )
            is_new = True

        # if it's not found in actual and new finance object is not created
        if not is_available and not is_new:
            list_of_actual_supply_side_items.append(
                {
                    'component': component_name,
                    'total_price': final_price
                }
            )

        finance.actual_supply_side = list_of_actual_supply_side_items
        finance.save()

        return JsonResponse({"specifications": specifications})
