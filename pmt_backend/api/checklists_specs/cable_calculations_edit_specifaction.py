from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event
from pmt_backend.utils.convert_json_to_python import json_to_python


class CreateCableSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        cable_calculations = request.POST['cable_calculations']
        cable_calculations = json_to_python(cable_calculations)

        specifications = []
        for cable in cable_calculations:
            specifications.append(
                {
                    "cable_type": cable['cable_type'],
                    "cable_specification": cable['cable_specification'],
                    "cable_length": cable['cable_length']
                }
            )

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break
        event.save()

        return JsonResponse({"specifications": specifications})
