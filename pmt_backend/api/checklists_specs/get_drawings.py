from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView

from pmt_backend.models import Project


class GetDrawings(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        project_id = request.GET['project_id']
        project = Project.objects.get(pk=project_id)
        list_of_drawings = project.projectspecification.selected_drawings
        return JsonResponse({"list_of_drawings": list_of_drawings})
