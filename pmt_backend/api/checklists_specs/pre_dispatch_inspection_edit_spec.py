from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event


class CreatePreDispatchInspectionSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        component_name = request.POST['component_name']
        pdi_agency_name = request.POST['pdi_agency_name']
        pdi_date = request.POST['pdi_date']
        contact_person_name = request.POST['contact_person_name']
        contact_person_email = request.POST['contact_person_email']
        contact_phone_number = request.POST['contact_phone_number']

        specifications = [
            {
                'component_name': component_name,
                'pdi_agency_name': pdi_agency_name,
                'pdi_date': pdi_date,
                'contact_person_name': contact_person_name,
                'contact_person_email': contact_person_email,
                'contact_phone_number': contact_phone_number
            }
        ]

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break

        event.save()

        return JsonResponse({"specifications": specifications})
