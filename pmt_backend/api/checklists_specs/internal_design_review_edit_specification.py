from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event
from pmt_backend.utils.convert_json_to_python import json_to_python


class CreateInternalReviewDesignSpec(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        internal_review_design_change = request.POST['internal_review_design_change']

        internal_review_design_change = json_to_python(internal_review_design_change)

        specifications = []
        for change in internal_review_design_change:
            specifications.append(
                {
                    "drawing_name": change['drawing_name'],
                    "change": change['change']
                }
            )

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break

        event.save()

        return JsonResponse({"specifications": specifications})
