from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, TeamMember


class GetChecklistSpecificationforlod(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        checklist_id = request.GET['checklist_id']

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        specifications = []
        for checklist in event_checklists:
            if str(checklist['id']) == str(checklist_id):
                specifications = checklist.get("specifications")
                if specifications:
                    for spec in specifications:
                        member = TeamMember.objects.get(pk=spec['spoc'])
                        spec['spoc'] = {'name': member.name, 'id': member.pk}
                    break
                else:
                    specifications = []

        return JsonResponse({"specifications": specifications})
