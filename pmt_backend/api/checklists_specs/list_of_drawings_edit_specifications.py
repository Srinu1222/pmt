from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, ProjectRight
from pmt_backend.utils.convert_json_to_python import json_to_python


class CreateListOfDrawings(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']
        is_mobile = request.POST.get('is_mobile')

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        specifications = []
        list_of_drawings = request.POST['list_of_drawings']
        list_of_drawings = json_to_python(list_of_drawings)
        for drawing in list_of_drawings:
            if not is_mobile == 'true':
                 specifications.append(
                    {
                        "drawing": drawing['drawing'],
                        "spoc": drawing['default_spoc'],
                        "start_date": drawing['start_time']
                    }
                )
            else:
                specifications.append(
                    {
                        "drawing": drawing['drawing'],
                        "spoc": drawing['spoc']['id'],
                        "start_date": drawing['start_date']
                    }
                )

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break

        event.save()

        return JsonResponse({"specifications": specifications})

