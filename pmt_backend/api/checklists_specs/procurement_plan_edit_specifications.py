from datetime import datetime, timedelta

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event, TeamMember
from pmt_backend.utils.convert_json_to_python import json_to_python


class CreateProcurementPlan(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']
        is_mobile = request.POST.get('is_mobile')

        event = Event.objects.filter(pk=event_id).first()
        event_checklists = event.checklists

        procurement_plan = request.POST['procurement_plan']
        procurement_plan = json_to_python(procurement_plan)

        specifications = []
        for plan in procurement_plan:
            if not is_mobile=='true':
                spoc = plan['spoc']
            else:
                spoc = plan['spoc']['id']

            specifications.append(
                {
                    "component_name": plan['component_name'],
                    "spoc": spoc,
                    "start_date": plan['start_date']
                }
            )

            update_event_spoc = Event.objects.filter(project=event.project, stage='PROCUREMENT',
                                                     name=plan['component_name']).first()
            if update_event_spoc:
                update_event_spoc.default_spoc = TeamMember.objects.get(pk=spoc)
                start_time = datetime.strptime(plan['start_date'], '%d/%m/%Y')
                completed_time = start_time + timedelta(days=event.days_needed)
                update_event_spoc.completed_time = completed_time
                update_event_spoc.start_time = start_time
                update_event_spoc_checklists = update_event_spoc.checklists
                for checklist in update_event_spoc_checklists:
                    checklist['spoc'] = spoc
                    checklist['due_on'] = completed_time.strftime('%d/%m/%Y')
                update_event_spoc.save()

        # clear previous specifications
        for checklist in event_checklists:
            if checklist['id'] == int(checklist_id):
                if checklist.get("specifications"):
                    checklist.pop("specifications")
                    checklist["specifications"] = specifications
                    break
                else:
                    checklist["specifications"] = specifications
                    break

        event.save()
