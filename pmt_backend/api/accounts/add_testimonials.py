from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Testimonial


class AddTestimonial(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = request.user
        company = user.company

        Testimonial.objects.create(company=company, description=request.POST['description'],
                                   person=request.POST['person'],
                                   rating=request.POST['rating'])

        return JsonResponse({})
