from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response


class PasswordChange(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = request.user
        current_password = request.POST['current_password']
        new_password = request.POST['new_password']

        success = user.check_password(current_password)
        if success:
            user.set_password(new_password)
            user.save()
        else:
            return send_fail_http_response("Your password is incorrect")

        return JsonResponse({})
