from django.http import JsonResponse
from pmt_backend.models import KeyPersonnel
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class DeleteKeyPersonnel(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        id = request.POST['id']
        KeyPersonnel.objects.get(pk=id).delete()

        return JsonResponse({})