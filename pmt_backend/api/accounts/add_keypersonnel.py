from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import KeyPersonnel


class AddKeyPersonnel(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = request.user
        company = user.company
        KeyPersonnel.objects.create(company=company, name=request.POST['name'], image=request.FILES['image'],
                                    designation=request.POST['designation'])

        return JsonResponse({})
