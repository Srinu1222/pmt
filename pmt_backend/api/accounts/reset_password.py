from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import User


class ResetPassword(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        id = request.POST['id']
        password = request.POST['password']

        user = User.objects.get(pk=id)
        user.set_password(password)
        user.save()

        return JsonResponse({})
