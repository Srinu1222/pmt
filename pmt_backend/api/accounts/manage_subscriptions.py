from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Customer, Project, TeamMember, SubContractorTeamMember, StakeHolder


class ManageSubscription(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        company = user.company
        company_users = company.user_set.all()
        user_list = []
        for user in company_users:
            user_type = user.user_type
            member = TeamMember.objects.filter(user=user).first()
            sub_contractor_member = SubContractorTeamMember.objects.filter(user=user).first()
            if user.is_consumer:
                customer = Customer.objects.get(user=user)
                active_projects = Project.objects.filter(customer=customer, status='ACTIVE').order_by('-id')
            elif user_type in ('Investor', 'Safearth Project Manager'):
                stake_holder = StakeHolder.objects.get(user=user, stakeholder_type=user_type)
                active_projects = Project.objects.filter(stake_holder=stake_holder, status='ACTIVE').order_by('-id')
            elif sub_contractor_member:
                active_projects = sub_contractor_member.projects.all().filter(status='ACTIVE').order_by('-id')
            else:
                team_member = TeamMember.objects.filter(user=user).first()
                active_projects = team_member.projects.all().filter(status='ACTIVE').order_by('-id')

            current_projects = [project.name for project in active_projects]

            user_list.append(
                {
                    'user_id': user.pk,
                    'name': member.name,
                    'status': user.is_active,
                    'current_projects': current_projects,
                    'current_projects_count': len(current_projects)
                }
            )

        return JsonResponse({"user_list": user_list})
