from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.serializers.accounts.company_awards import company_awards_serializer
from pmt_backend.serializers.accounts.company_key_personnels import company_key_personals_serializer
from pmt_backend.serializers.accounts.company_projects import company_projects_serializer
from pmt_backend.serializers.accounts.company_testimonals import company_testimonials


class GetProfile(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        user = request.user
        company = user.company
        list_of_projects_of_a_company = company_projects_serializer(company.project_set.all())
        list_of_awards_and_recognitions = company_awards_serializer(company.award_set.all())
        list_of_key_personals = company_key_personals_serializer(company.keypersonnel_set.all())
        list_of_testimonials = company_testimonials(company.testimonial_set.all())

        profile_data = {
            'company_name': company.name,
            'company_image': company.image.url if company.image else None,
            'mail': company.primary_email if company.primary_email else None,
            'head_office': company.head_office if company.head_office else None,
            'regional_office': company.regional_office if company.regional_office else None,
            'founded_in': company.founded_year if company.founded_year else None,
            'about_company': company.description if company.description else None,
            'website': company.website if company.website else None,
            'upload_logo': company.upload_logo.url if company.upload_logo else None,
            'upload_brochure': company.upload_brochure.url if company.upload_brochure else None,
            'annual_turnover': company.annual_turn_over,
            'employee_strength': company.employee_strength,
            'capacity_of_projects_installed': company.installed_capacity,
            'minimum_size_of_projects_installed': company.min_size_of_projects_installed,
            'maximum_size_of_single_projects': company.max_size_of_projects_installed,
            'states_active_in': company.states_active_in,
            'business_model_offered': company.business_model_offered,
            'Flag_ship_projects': company.flagship_projects,
            'projects': list_of_projects_of_a_company,
            'awards_and_recognitions': list_of_awards_and_recognitions,
            'key_personals': list_of_key_personals,
            'testimonials': list_of_testimonials
        }

        return JsonResponse({'profile_data': profile_data})
