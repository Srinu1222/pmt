from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import User


class ChangeUserStatus(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        id = request.POST['id']
        user = User.objects.get(pk=id)
        status = request.POST['status']
        if status == 'true':
            user.is_active = True
        else:
            user.is_active = False
        user.save()

        return JsonResponse({})
