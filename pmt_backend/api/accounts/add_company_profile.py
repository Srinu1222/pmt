from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class AddProfile(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        user = request.user
        company = user.company

        company_name = request.POST.get('company_name')
        website = request.POST.get('website')
        head_office = request.POST.get('head_office')
        regional_office = request.POST.get('regional_office')
        founded_in = request.POST.get('founded_in')
        upload_logo = request.POST.get('upload_logo')
        upload_brochure = request.POST.get('upload_brochure')
        company_image = request.FILES.get('image')
        mail = request.POST.get('mail')
        about_company = request.POST.get('about_company')

        if company_image:
            company.image = company_image
        if mail:
            company.primary_email = mail
        if about_company:
            company.description = about_company
        if upload_brochure:
            company.upload_brochure = upload_brochure
        if upload_logo:
            company.upload_logo = upload_logo
        if founded_in:
            company.founded_year = founded_in
        if regional_office:
            company.regional_office = regional_office
        if head_office:
            company.head_office = head_office
        if website:
            company.website = website
        if company_name:
            company.name = company_name

        company.save()

        return JsonResponse({})
