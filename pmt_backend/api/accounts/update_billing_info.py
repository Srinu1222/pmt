from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import PaymentInfo


class UpdateBillingInfo(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        billing_name = request.POST.get('billing_name')
        gst = request.POST.get('gst')
        address = request.POST.get('address')
        contact_person = request.POST.get('contact_person')
        contact_person_number = request.POST.get('contact_person_number')
        estimated_amount = request.POST.get('estimated_amount')
        next_bill = request.POST.get('next_bill')

        user = request.user
        company = user.company
        payment = PaymentInfo.objects.filter(company=company).first()

        if billing_name:
            payment.billing_name = billing_name
        if gst:
            payment.gst = gst
        if address:
            payment.address = address
        if contact_person:
            payment.contact_person = contact_person
        if contact_person_number:
            payment.contact_person_number = contact_person_number
        if estimated_amount:
            payment.estimated_amount = int(estimated_amount)
        if next_bill:
            payment.next_bill = next_bill

        payment.save()

        return JsonResponse({})
