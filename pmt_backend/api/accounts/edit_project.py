from datetime import datetime

from django.http import JsonResponse
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class EditProject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        id = request.POST.get('id')
        project = Project.objects.get(pk=id)
        size = request.POST.get('size')
        business_model = request.POST.get('business_model')
        location = request.POST.get('location')
        cod = request.POST.get('cod')
        modules_used = request.POST.get('modules_used')
        inverters_used = request.POST.get('inverters_used')
        first_year_kw_generation = request.POST.get('first_year_kw_generation')

        if size:
            project.size = size
        if business_model:
            project.business_model = business_model
        if location:
            project.location = location
        if cod:
            project.cod = datetime.strptime(cod, "%d/%m/%Y")
        if modules_used:
            project.modules_used = modules_used
        if inverters_used:
            project.inverters_used = inverters_used
        if first_year_kw_generation:
            project.first_year_kw_generation = first_year_kw_generation

        project.save()

        return JsonResponse({})

