from django.http import JsonResponse

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import User
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class DeleteUser(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        id = request.POST['id']
        if str(request.user.pk) == str(id):
            send_fail_http_response(msg="User cannot be deleted.")
        else:
            User.objects.get(pk=id).delete()
            return JsonResponse({})
