from django.http import JsonResponse
from pmt_backend.models import Testimonial
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class DeleteTestimonial(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        id = request.POST['id']
        Testimonial.objects.get(pk=id).delete()

        return JsonResponse({})
