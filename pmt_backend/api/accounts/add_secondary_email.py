from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Customer, StakeHolder
from pmt_backend.utils.convert_json_to_python import json_to_python


class AddSecondaryEmails(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        user = request.user
        emails = json_to_python(request.POST['emails'])

        customer = Customer.objects.filter(user=user).first()
        if customer:
            customer.emails = customer.emails + emails
            customer.save()
        else:
            stakeholder = StakeHolder.objects.filter(user=user).first()
            if stakeholder:
                stakeholder.emails = stakeholder.emails+emails
                stakeholder.save()
            else:
                return send_fail_http_response(msg='Stakeholder does not exist with this user')

        return JsonResponse({})
