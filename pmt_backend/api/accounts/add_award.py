from datetime import datetime, timedelta
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Award
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware


class AddAward(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        user = request.user
        company = user.company

        title = request.POST['title']
        date = request.POST['date']
        date = convert_naive_to_aware(datetime.strptime(date, "%d/%m/%Y"))
        image = request.FILES['image']

        Award.objects.create(company=company, title=title,
                             date=date + timedelta(hours=5, minutes=30),
                             image=image)

        msg = "Award added successfully."
        return send_pass_http_response(msg)

