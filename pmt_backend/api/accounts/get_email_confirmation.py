from django.core.mail import EmailMultiAlternatives
from pmt_backend.utils.to_generate_random_6_digits_otp import generate_six_digit_otp
from celery.decorators import task
from django.http import JsonResponse
from pmt_backend.models import PendingOTP
from pmt.celery import app


def send_email_confirmation(request):
    otp = generate_six_digit_otp()
    email = request.GET['email']
    send_email_otp.delay(otp, email)
    pending_otp, is_new = PendingOTP.objects.get_or_create(email=email)
    pending_otp.otp = otp
    pending_otp.save()
    return JsonResponse({})


@app.task
def send_email_otp(otp, email):
    msg = EmailMultiAlternatives('OTP for Project management tool', '',
                                 '"Safearth" <info@safearth.in>', [email])
    text = '<p>This is an automatically generated 6 digit otp for verification : {}</p>' \
           '<p>Kindly use the OTP to verify the email address and proceed with your signup.'
    formatted_text = text.format(otp)
    msg.attach_alternative(formatted_text,
                           "text/html")
    msg.send()
