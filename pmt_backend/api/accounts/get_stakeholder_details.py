from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Customer, StakeHolder


class GetStakeholderDetails(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        user = request.user
        customer = Customer.objects.filter(user=user).first()
        if customer:
            data = {
                'user_name': customer.customer_name,
                'primary_email': customer.primary_email,
                'secondary_emails': customer.emails,
                'phone_number': customer.phone_number,
                'country': ''
            }
        else:
            stakeholder = StakeHolder.objects.filter(user=user).first()
            if stakeholder:
                data = {
                    'user_name': stakeholder.name,
                    'primary_email': stakeholder.primary_email,
                    'secondary_emails': stakeholder.emails,
                    'phone_number': stakeholder.phone_number,
                    'country': ''
                }
            else:
                return send_fail_http_response(msg='Any stakeholder does not exist with this user')

        return JsonResponse({'data': data})
