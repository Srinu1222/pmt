from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class AddBasicInfo(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        user = request.user
        company = user.company

        annual_turn_over = request.POST.get('annual_turn_over')
        employee_strength = request.POST.get('employee_strength')
        installed_capacity = request.POST.get('installed_capacity')
        min_size_of_projects_installed = request.POST.get('min_size_of_projects_installed')
        max_size_of_projects_installed = request.POST.get('max_size_of_projects_installed')
        states_active_in = request.POST.get('states_active_in')
        business_model_offered = request.POST.get('business_model_offered')
        flagship_projects = request.POST.get('flagship_projects')


        if annual_turn_over:
            company.annual_turn_over = annual_turn_over
        if employee_strength:
            company.employee_strength = employee_strength
        if installed_capacity:
            company.installed_capacity = installed_capacity
        if min_size_of_projects_installed:
            company.min_size_of_projects_installed = min_size_of_projects_installed
        if max_size_of_projects_installed:
            company.max_size_of_projects_installed = max_size_of_projects_installed
        if states_active_in:
            company.states_active_in = states_active_in
        if business_model_offered:
            company.business_model_offered = business_model_offered
        if flagship_projects:
            company.flagship_projects = flagship_projects

        company.save()

        return JsonResponse({})
