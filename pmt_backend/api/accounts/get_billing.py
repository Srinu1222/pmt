from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import PaymentInfo


class Billing(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        company = user.company

        company_invoices = company.invoice_set.all()
        invoice_list = []
        for invoice in company_invoices:
            invoice_list.append(
                {
                    'name': invoice.name,
                    'file': invoice.file.url if invoice.file else None
                }
            )

        users = company.user_set.all().count()
        license = users-1

        payment = PaymentInfo.objects.filter(company=company).first()

        return JsonResponse(
            {
                'current_plan_name': 'Trial',
                'license': license,
                'users': users,
                'billing_name': payment.billing_name if payment.billing_name else company.name,
                'gst': payment.gst,
                'address': payment.address,
                'contact_person': payment.contact_person,
                'contact_person_number': payment.contact_person_number,
                'estimated_amount': payment.estimated_amount,
                'next_bill': payment.next_bill,
                'invoice_list': invoice_list
            }
        )
