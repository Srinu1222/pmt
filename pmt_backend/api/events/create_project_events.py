from openpyxl import load_workbook

from pmt_backend.models import Gantt
from pmt_backend.serializers.events.create_mandatory_stage_events_serializer import \
    create_mandatory_stage_events_serializer
from pmt_backend.serializers.events.create_selected_drawing_events import create_selected_drawing_events
from pmt_backend.serializers.events.create_selected_accessories_events import create_selected_accessories_events
from pmt_backend.serializers.events.create_selected_components_events import create_selected_components_events
from pmt_backend.serializers.events.create_selected_termination_events import create_selected_termination_events


def CreateRoofTopProject(project, gantt_id):
    gantt = project.gantt
    impact_file = gantt.project_impact
    gantt_file = gantt.gantt_file

    gantt_file_workbook = load_workbook(filename=gantt_file)
    gantt_sheet = gantt_file_workbook.active

    impact_file_workbook = load_workbook(filename=impact_file)
    impact_sheet = impact_file_workbook.active

    # To create mandatory events
    create_mandatory_stage_events_serializer(gantt_sheet, project)

    # Project Specifications Events
    project_spec = project.projectspecification

    selected_drawings = project_spec.selected_drawings
    create_selected_drawing_events(project, selected_drawings, impact_sheet, gantt_sheet)

    selected_accessories = project_spec.selected_accessories
    create_selected_accessories_events(project, selected_accessories, impact_sheet, gantt_sheet)

    selected_components = project_spec.selected_components
    create_selected_components_events(project, selected_components, impact_sheet, gantt_sheet)

    selected_termination = project_spec.selected_termination[0]
    create_selected_termination_events(project, project_spec, selected_termination, impact_sheet, gantt_sheet)


def Create_Project_events(project, gantt_id):
    project_types = project.projectspecification.selected_project_types
    if "Rooftop" in project_types:
        CreateRoofTopProject(project, gantt_id)
