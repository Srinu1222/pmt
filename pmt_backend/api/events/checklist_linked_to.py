import copy

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Event
from pmt_backend.utils.get_Jsonfield_dict import get_event_task_dict


class ChecklistLinkedTo(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        checklist_id = request.POST['checklist_id']
        selected_event_id = request.POST['selected_event_id']

        event = Event.objects.filter(pk=event_id).first()
        selected_event = Event.objects.filter(pk=selected_event_id).first()

        event_checklists = event.checklists
        checklist_dict = get_event_task_dict(event_checklists, checklist_id)

        modified_checklist_dict = copy.deepcopy(checklist_dict)

        selected_event_prerequisites = selected_event.Prerequisites

        if event.name == "Re-Do of Internal Recommended Changes":
            selected_event_checklists = selected_event.checklists
            modified_checklist_dict['title'] = "Re-Do "+modified_checklist_dict['title']
            modified_checklist_dict['id'] = len(selected_event_checklists) + 1
            selected_event_checklists.append(modified_checklist_dict)
            checklist_dict['linked_to'] = {
                    'event_name': selected_event.name,
                    'event_id': event.id,
                    'task_id': modified_checklist_dict['id'],
                    'task_type': 'CHECKLISTS'
            }
            selected_event.save()
            event.save()
        else:
            modified_checklist_dict['id'] = len(selected_event_prerequisites) + 1
            selected_event_prerequisites.append(
                modified_checklist_dict
            )
            checklist_dict['linked_to'] = {
                'event_name': selected_event.name,
                'event_id': event.id,
                'task_id': modified_checklist_dict['id'],
                'task_type': 'PRE-REQUISITES'
            }
            selected_event.save()
            event.save()

        return JsonResponse({})
