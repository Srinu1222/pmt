from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, Document, Databank


class AddEventDelayInfo(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        delay_reason = request.POST['delay_reason']
        event = Event.objects.get(pk=request.POST['event_id'])
        event.delay_reason = delay_reason
        event.is_delayed_reason_provided = True
        event.save()
        return JsonResponse({})
