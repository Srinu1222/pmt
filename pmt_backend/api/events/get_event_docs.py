from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, Document


class GetEventDocs(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        event = Event.objects.get(pk=event_id)
        is_image = request.GET.get('is_image')
        is_image = True if is_image == 'true' else False

        docs_queryset = event.document_set.filter(is_image=is_image).order_by('-upload_date')

        docs_list = []
        for doc in docs_queryset:
            docs_list.append(
                {
                    "title": doc.title,
                    "file": doc.file.url,
                    "description": doc.description
                }
            )

        return JsonResponse({"docs": docs_list})
