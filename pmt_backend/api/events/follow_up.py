from django.http import JsonResponse

from pmt_backend.mail.mail_tasks.customers.follow_up_for_task import send_follow_up_for_task
from pmt_backend.models import Event, Customer
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.serializers.Notifications.follow_up_for_task import follow_up_for_task
from pmt_backend.utils.get_Jsonfield_dict import get_event_task_index


class FollowUpForTask(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST.get('event_id')
        event = Event.objects.filter(pk=event_id).first()
        pre_requisite_id = request.POST.get('pre_requisite_id')

        event_prerequisites = event.Prerequisites
        index = get_event_task_index(event_prerequisites, pre_requisite_id)

        # create notification and send mail for follow up
        follow_up_for_task(request.user, event, event_prerequisites[index])
        if event.default_spoc:
            spoc = event.default_spoc.name
        else:
            spoc = event.project.customer.customer_name
        send_follow_up_for_task.delay(event.id, spoc, event.Prerequisites[index]['title'])

        return JsonResponse({})
