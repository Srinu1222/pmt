from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, Document, Databank
from pmt_backend.utils.convert_json_to_python import json_to_python


class AddEventDoc(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        event_id = request.POST['event_id']
        is_image = request.POST.get('is_image')
        is_image = True if is_image == 'true' else False

        # Check for backward compatibility

        if 'file' in request.FILES:
            file = request.FILES['file']
            name = request.POST['doc_name']

            event = Event.objects.get(pk=event_id)
            databank = Databank.objects.get(project=event.project)

            Document.objects.create(
                databank=databank, event=event,
                is_image=is_image,
                description=event.name + '_' + 'upload',
                title=name,
                file=file
            )

            return JsonResponse({})

        files_data = json_to_python(request.POST["files_data"])
        files = request.FILES.getlist('files')

        event = Event.objects.get(pk=event_id)
        databank = Databank.objects.get(project=event.project)

        for index, i in enumerate(files_data):
            Document.objects.create(
                databank=databank, event=event,
                is_image=is_image,
                description=i['description'],
                title=i['title'],
                file=files[index]
            )

        return JsonResponse({})
