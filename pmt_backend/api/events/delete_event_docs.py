from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, Document, Databank


class DeleteEventDoc(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        # event_id = request.POST['event_id']
        doc_id = request.POST['doc_id']

        # event = Event.objects.get(pk=event_id)

        doc = Document.objects.filter(id=doc_id).first().delete()
        return JsonResponse({})
