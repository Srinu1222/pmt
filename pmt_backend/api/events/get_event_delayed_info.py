from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Event, Document
from pmt_backend.utils.formatting_time import formating_date_time


class GetEventDelayedInfo(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        event_id = request.GET['event_id']
        event = Event.objects.get(pk=event_id)
        event_delayed_info_dict = {
            'name': event.name,
            'spoc': event.default_spoc.name,
            'planned_date': formating_date_time(event.completed_time) if event.completed_time else 'Not Yet Planned',
            'completed_date': formating_date_time(event.actual_completed_time) if event.actual_completed_time else 'Not Yet Started',
            'delayed_reason': event.delay_reason if event.delay_reason else '',
            'is_delayed_reason_provided': event.is_delayed_reason_provided
        }
        return JsonResponse({"event_delayed_info_dict": event_delayed_info_dict})
