from django.shortcuts import redirect


def redirect_login(request):
    return redirect("https://pmt.safearth.in/login")
