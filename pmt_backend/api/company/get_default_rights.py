from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.utils.project_rights import can_view, can_approve, can_work, can_edit



class GetDefaultProjectRights(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):

        rights_list = []

        for index, role in enumerate(can_work.keys()):

            rights_list.append({
                'role': role,
                'can_view': can_view[role],
                'can_edit': can_edit[role],
                'can_approve': can_approve[role]
            })

        return JsonResponse({'rights_list': rights_list})

