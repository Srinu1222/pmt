from django.http import JsonResponse
from pmt_backend.models import TeamMember, Event, SubContractorTeamMember
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from datetime import datetime

from pmt_backend.utils.formatting_time import formating_date_time


class GetDailyTasks(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        sub_contractor_member = SubContractorTeamMember.objects.filter(user=request.user).first()
        member = TeamMember.objects.filter(user=request.user).first()
        project_list = []

        if sub_contractor_member:
            sub_contractor_projects = sub_contractor_member.projects.filter(status='ACTIVE')
            is_sub_contractor = True
            for project in sub_contractor_projects:
                event_queryset = Event.objects.filter(sub_contractor_spoc=sub_contractor_member, project=project,
                                                      start_time__lte=datetime.now()).exclude(
                    status='FINISHED')
                tasks = []
                for event in event_queryset:
                    checklists = event.checklists
                    for checklist in checklists:
                        if checklist['status'] == False and int(checklist['spoc']) == int(member.id):
                            tasks.append(
                                {
                                    'title': checklist['title'],
                                    'event_id': event.pk,
                                    'stage': event.stage,
                                    'event_title': event.name,
                                    'is_sub_contractor_event': event.is_sub_contractor_event,
                                }
                            )

                project_list.append(
                    {
                        'id': project.id,
                        'date': formating_date_time(datetime.now()),
                        'title': project.name,
                        'tasks': tasks,
                        'count': len(tasks),
                        'is_sub_contractor': is_sub_contractor
                    }
                )

        projects = member.projects.filter(status='ACTIVE')
        is_sub_contractor = False

        for project in projects:
            event_queryset = Event.objects.filter(default_spoc=member, project=project,
                                                  start_time__lte=datetime.now()).exclude(
                status='FINISHED')

            tasks = []
            for event in event_queryset:
                checklists = event.checklists
                for checklist in checklists:
                    if checklist['status'] == False and int(checklist['spoc']) == int(member.id):
                        tasks.append(
                            {
                                'title': checklist['title'],
                                'event_id': event.pk,
                                'stage': event.stage,
                                'event_title': event.name,
                                'is_sub_contractor_event': event.is_sub_contractor_event,
                            }
                        )

            project_list.append(
                {
                    'id': project.id,
                    'date': formating_date_time(datetime.now()),
                    'title': project.name,
                    'tasks': tasks,
                    'count': len(tasks),
                    'is_sub_contractor': is_sub_contractor
                }
            )

        return JsonResponse({'project_list': project_list})
