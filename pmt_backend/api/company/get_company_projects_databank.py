from django.http import JsonResponse
from pmt_backend.models import Project, User, Databank
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.serializers.company.databank import get_projects_databank_serializer


class GetCompanyProjectsDataBank(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        is_image = request.GET.get('is_image')
        is_image = True if is_image == 'true' else False

        project_queryset = Project.objects.filter(company=user.company)
        serialized_projects_list, serialized_basic_docs_list = get_projects_databank_serializer(
            project_queryset, is_image, user
        )
        return JsonResponse({
            'projects': serialized_projects_list,
            'basic_project_documents_details': serialized_basic_docs_list
        })