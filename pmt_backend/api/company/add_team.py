from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.mail.mail_tasks.customers.welcome_mail import send_welcome_mail
from pmt_backend.models import TeamMember, User, Gantt


class AddTeam(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        name = request.POST['name']
        number = request.POST['number']
        email = request.POST['email']
        designation = request.POST['designation']
        location = request.POST['location']
        department = request.POST['department']

        try:
            user = User.objects.create(
                        designation=designation,
                        email=email,
                        phone_number=number,
                        username=email,
                        company=request.user.company
                    )
        except:
            return send_fail_http_response('User Already exists.')

        password = str(name[:4]) + str(number)[:4]
        user.set_password(password)
        user.save()

        team_member = TeamMember.objects.create(user=user, name=name, department=department, location=location)

        company = request.user.company
        company.team_members.add(team_member)

        # to send welcome mail
        send_welcome_mail.delay(name, company.name, email, password, email)

        return JsonResponse({"id": team_member.id})
