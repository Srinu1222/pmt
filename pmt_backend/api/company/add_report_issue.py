from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Inventory, ReportIssue, ReportImages


class AddReportIssue(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        issue = request.POST['issue']
        issue_type = request.POST['issue_type']
        images = request.FILES.getlist('images')

        report = ReportIssue.objects.create(
            user=request.user,
            issue=issue,
            issue_type=issue_type
        )

        report_images_list = [ReportImages(report=report, image=image) for image in images]

        ReportImages.objects.bulk_create(report_images_list)

        return JsonResponse({"id": report.id})
