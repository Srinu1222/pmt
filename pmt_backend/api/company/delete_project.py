from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Project
from rest_framework.views import APIView
from rest_framework import authentication, permissions


class DeleteProject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['id']
        project = Project.objects.get(pk=project_id)
        project.delete()
        send_pass_http_response()
