from django.http import JsonResponse

from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Inventory, TeamMember


class AddInventoryWastage(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        inventory_id = request.POST['inventory_id']
        inventory = Inventory.objects.filter(pk=inventory_id).first()

        quantity_wasted = float(request.POST['quantity_wasted'])
        inventory.quantity_wasted = quantity_wasted

        spoc_id = request.POST['spoc_id']
        member = TeamMember.objects.filter(pk=spoc_id).first()
        inventory.spoc = member

        inventory.reason = request.POST['reason']

        inventory.is_wasted = True
        if inventory.stock_quantity > quantity_wasted:
            inventory.stock_quantity = inventory.stock_quantity - quantity_wasted
        else:
            return send_fail_http_response('Not enough quantity in stock')

        inventory.save()

        return JsonResponse({})

