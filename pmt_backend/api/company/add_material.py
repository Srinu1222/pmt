from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Inventory


class AddMaterial(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        category = request.POST['category']
        product_name = request.POST['product_name']
        quantity = float(request.POST['quantity'])
        location = request.POST['location']
        rate = request.POST['rate']
        value = request.POST['value']
        upload_po_file = request.FILES['upload_po_file']
        quantity_format = request.POST.get('quantity_format')

        material = Inventory.objects.create(
            company=request.user.company,
            material_name=category,
            product_name=product_name,
            stock_quantity=quantity,
            location=location,
            target_price=rate,
            value=value,
            upload_po_file=upload_po_file,
            status='PO_PLACED',
            quantity_format=quantity_format
        )

        return JsonResponse({"id": material.id})
