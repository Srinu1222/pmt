from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView
from pmt_backend.serializers.company.projects import get_dashboard_serializer
from datetime import datetime, timedelta, timezone


class GetDashboard(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        first_login = False
        if not user.trial_notification_shown:
            first_login = True
        user.last_login = datetime.now(timezone.utc) + timedelta(hours=5, minutes=30)
        user.trial_notification_shown = True
        user.save()
        serialized_project_dashboard = get_dashboard_serializer(user, first_login)
        return JsonResponse(serialized_project_dashboard)
