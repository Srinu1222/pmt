from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView

from pmt_backend.models import Notification, TeamMember, Ticket, SubContractorTeamMember, Customer, StakeHolder
from pmt_backend.utils.formatting_time import formating_date_time, formating_time


class GetNotifications(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        user = request.user
        customer = Customer.objects.filter(user=user).first()
        if customer:
            return JsonResponse({"notification_list": [], 'ticket_raised_list': []})
        else:
            stakeholder = StakeHolder.objects.filter(user=user).first()
            if stakeholder:
                return JsonResponse({"notification_list": [], 'ticket_raised_list': []})
            else:
                member = TeamMember.objects.get(user=user)
                see_all = request.GET.get('see_all')
                is_sub_contractor = True if SubContractorTeamMember.objects.filter(user=user).first() else False

                # MOBILE CODE
                is_mobile = request.GET.get('is_mobile')
                tickets_list = []
                if is_mobile == 'true':
                    ticket_queryset = Ticket.objects.filter(raised_by=user).order_by('-id')
                    member_name = member.name
                    for ticket in ticket_queryset:
                        event = ticket.event
                        assigned_to = ticket.assigned_to.name
                        tickets_list.append(
                            {
                                'raised_by': member_name,
                                'subject': ticket.subject,
                                'project_name': event.project.name,
                                'project_id': event.project.id,
                                'event_id': event.pk,
                                'stage': event.stage,
                                'assigned_to': assigned_to,
                                'is_sub_contractor': is_sub_contractor,
                                'date': formating_date_time(ticket.added_date),
                                'time': formating_time(ticket.added_date)
                            }
                        )

                notification_queryset = Notification.objects.filter(notified_to=member).order_by('-id')

                notification_queryset.update(is_seen=True)

                notification_list = []
                for notification in notification_queryset:
                    notification_list.append(
                        {
                            "project_name": notification.project.name if notification.project else notification.event.project.name if notification.event else '',
                            "project_id": notification.project.id if notification.project else notification.event.project.id if notification.event else '',
                            "project_image": notification.project.image.url if notification.project and notification.project.image else notification.event.project.image.url if notification.event and notification.event.project.image else '',
                            "date": formating_date_time(notification.day),
                            "time": formating_time(notification.day),
                            "title": notification.title,
                            "event_id": notification.event.id if notification.event else None,
                            "is_event_delayed": notification.event.is_delayed if notification.event else None,
                            'is_event_delayed_reason_provided': notification.event.is_delayed_reason_provided if notification.event else None,
                            "stage": notification.event.stage if notification.event else None,
                            "notification_type": notification.notification_type,
                            "description": notification.description,
                            'is_sub_contractor': is_sub_contractor,
                        }
                    )

                if see_all:
                    if is_mobile == 'true':
                        return JsonResponse(
                            {
                                "notification_list": notification_list,
                                'ticket_raised_list': tickets_list
                            }
                        )
                    else:
                        return JsonResponse({"notification_list": notification_list})
                else:
                    if is_mobile == 'true':
                        return JsonResponse(
                            {
                                "notification_list": notification_list[:10],
                                'ticket_raised_list': tickets_list[:10]
                            }
                        )
                    else:
                        return JsonResponse({"notification_list": notification_list[:10]})
