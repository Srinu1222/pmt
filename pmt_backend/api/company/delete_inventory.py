from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Event, TeamMember, Inventory
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.utils.convert_json_to_python import json_to_python


class DeleteInventory(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        inventory_list = request.POST['inventory_list']
        inventory_list = json_to_python(inventory_list)
        try:
            Inventory.objects.filter(id__in=inventory_list).delete()
            return send_pass_http_response()
        except Inventory.DoesNotExist:
            return send_fail_http_response()
