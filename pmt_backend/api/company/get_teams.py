from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.company.teams import get_teams_serializer


class GetTeams(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        is_mobile = request.GET.get('is_mobile')
        if is_mobile:
            serialized_teams = get_teams_serializer(user, is_mobile)
        else:
            serialized_teams = get_teams_serializer(user)

        return JsonResponse(serialized_teams)

