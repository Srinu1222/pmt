from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import SubContractorTeamMember


class GetUserReports(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        company = user.company

        user_reports = user.reportissue_set.all()

        user_reports_list = []
        for report in user_reports:
            report_images = report.report_images_set.all()
            user_reports_list.append(
                {
                    'issue': report.issue,
                    'issue_type': report.issue_type,
                    'images': [i.image.url for i in report_images]
                }
            )

        return JsonResponse({'user_reports_list': user_reports_list})
