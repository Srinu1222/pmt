from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.dispatchers.responses.send_pass_http_response import send_pass_http_response
from pmt_backend.models import Event, TeamMember, User, SubContractorTeamMember
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.utils.convert_json_to_python import json_to_python


class DeleteMembers(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        members_id_list = request.POST['members_id_list']
        members_id_list = json_to_python(members_id_list)
        sub_contractor_member = request.POST.get('sub_contractor_member')

        is_sub_contractor_member_list = True if sub_contractor_member == 'true' else False

        if is_sub_contractor_member_list:
            try:
                sub_contractor_teamMember_queryset = SubContractorTeamMember.objects.filter(id__in=members_id_list)
                for sub_contractor_member in sub_contractor_teamMember_queryset:
                    request.user.company.sub_contractor_company.remove(sub_contractor_member.user.company)
                return send_pass_http_response()
            except TeamMember.DoesNotExist:
                return send_fail_http_response()
        else:
            try:
                teamMember_queryset = TeamMember.objects.filter(id__in=members_id_list)
                teamMember_queryset.update(is_active_member=False)
                User.objects.filter(teammember__in=teamMember_queryset).update(is_active=False)
                return send_pass_http_response()
            except TeamMember.DoesNotExist:
                return send_fail_http_response()
