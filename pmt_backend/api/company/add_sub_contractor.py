from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.sub_contractor.sub_contractor_created import send_sub_contractor_created
from pmt_backend.models import User, Company, SubContractorTeamMember, TeamMember, PaymentInfo, Gantt


class AddSubContractor(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        company_name = request.POST['company_name']
        number = request.POST['number']
        email = request.POST['email']
        contact_person_name = request.POST['contact_person_name']

        user = User.objects.filter(email=email).first()

        if user:
            SubContractorTeamMember.objects.get_or_create(user=user,
                                                          name=contact_person_name,
                                                          department='Management')
            request.user.company.sub_contractor_company.add(user.company)
            send_sub_contractor_created.delay(request.user.company.name, [email])
        else:
            company = Company.objects.create(name=company_name)
            PaymentInfo.objects.create(company=company)
            user = User.objects.create(
                designation='Project Manager',
                email=email,
                phone_number=number,
                username=email,
                company=company
            )

            user.set_password(contact_person_name[:4] + number[:4])
            user.save() 

            user.company.account_owner = user
            member = TeamMember.objects.create(user=user, name=contact_person_name, department='Management')
            user.company.team_members.add(member)
            company.save()

            SubContractorTeamMember.objects.create(user=user, name=contact_person_name,
                                                   department='Management')
            # send email for subcontractor created
            send_sub_contractor_created.delay(request.user.company.name, [email])

            request.user.company.sub_contractor_company.add(user.company)

        return JsonResponse({})
