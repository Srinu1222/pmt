from django.http import JsonResponse
from pmt_backend.models import Project, User
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.serializers.company.projects import get_projects_serializer


class GetAllProjects(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        project_queryset = Project.objects.filter(company=user.company)
        serialized_projects_list = get_projects_serializer(project_queryset)
        return JsonResponse({
            'projects': serialized_projects_list,
        })
