from datetime import datetime

from django.http import JsonResponse

from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Inventory, TeamMember, Project, Event
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware


class InventoryAssign(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        new_project_id = request.POST['new_project_id']
        assign_spoc = request.POST['assign_spoc']
        target_date = request.POST['target_date']
        inventory_id = request.POST['inventory_id']

        new_project = Project.objects.get(pk=new_project_id)
        inventory = Inventory.objects.filter(pk=inventory_id).first()

        # creating new event for assigning inventory to another project
        event = Event.objects.filter(project=inventory.project, stage='MATERIAL HANDLING',
                                     name=inventory.material_name).first()
        if event:
            event.name = 'Movement of ' + inventory.material_name + ' from ' + inventory.project.name
            event.project = new_project
            event.pk = None
            event.save()

        inventory.project = new_project
        inventory.assign_spoc = TeamMember.objects.get(pk=assign_spoc)
        inventory.status = 'IN_TRANSIT'
        inventory.target_date = convert_naive_to_aware(datetime.strptime(target_date, "%d/%m/%Y"))

        inventory.save()

        return JsonResponse({})
