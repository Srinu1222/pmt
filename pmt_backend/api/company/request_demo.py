from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from pmt_backend.models import RequestDemo


@csrf_exempt
def AddRequestDemo(request):
    name = request.POST['name']
    company = request.POST['company']
    email = request.POST['email']
    phone_number = request.POST['phone_number']
    comment = request.POST.get('comment', '')

    demo = RequestDemo.objects.create(
        name=name,
        email=email,
        phone_number=phone_number,
        company=company,
        comment=comment
    )

    return JsonResponse({"id": demo.id})
