from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import SubContractorTeamMember


class GetAllSubContractors(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        company = user.company

        sub_contractor_companies_queryset = company.sub_contractor_company.all()

        sub_contractors_list = []
        for company in sub_contractor_companies_queryset:
            member = SubContractorTeamMember.objects.filter(user=company.account_owner).first()
            sub_contractors_list.append(
                {
                    'company_name': company.name,
                    'company_id': company.pk,
                    'user_name': member.name
                }
            )
        return JsonResponse({'sub_contractors_list': sub_contractors_list})
