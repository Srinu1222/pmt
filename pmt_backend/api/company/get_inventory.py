from django.http import JsonResponse
from rest_framework import authentication, permissions
from rest_framework.views import APIView
from pmt_backend.serializers.company.get_inventory import get_inventory_serializer
from pmt_backend.utils.convert_json_to_python import json_to_python


class GetInventory(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        is_sort = request.GET.get('is_sort')
        is_filter = request.GET.get('is_filter')
        value = request.GET.get('value')
        value = json_to_python(value) if value else []
        serialized_inventory, return_status = get_inventory_serializer(user.company, is_sort, is_filter, value)
        if return_status:
            return JsonResponse(serialized_inventory)
        else:
            return serialized_inventory
