from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.mail.mail_tasks.customers.welcome_mail import send_welcome_mail
from pmt_backend.models import TeamMember, User, Gantt
from openpyxl import load_workbook


class AddTeamMembers(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        add_members = request.FILES['add_members']
        workbook = load_workbook(filename=add_members)
        sheet = workbook.active

        for index, row in enumerate(sheet):
            if index == 0:
                continue
            else:
                if not row[0].value:
                    break
                first_name = row[0].value
                last_name = row[1].value
                name = first_name + ' ' + last_name if last_name else first_name
                number = int(row[2].value if row[2].value or row[2].value != 0 else 00000)
                email = row[3].value
                designation = row[4].value
                location = row[5].value
                department = row[6].value

                user = User.objects.create(
                    designation=designation,
                    email=email,
                    phone_number=number,
                    username=email,
                    company=request.user.company,
                    first_name=first_name,
                    last_name=last_name
                )

                password = str(name[:4]) + str(number)[:4]
                user.set_password(password)
                user.save()

                team_member = TeamMember.objects.create(user=user, name=name, department=department, location=location)

                company = request.user.company
                company.team_members.add(team_member)

                # to send welcome mail
                send_welcome_mail.delay(name, company.name, email, password, email)

        return JsonResponse({})
