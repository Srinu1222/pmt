from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import SubContractorTeamMember, Event


class GetSubContractors(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        company = user.company

        sub_contractor_companies_queryset = company.sub_contractor_company.all()

        sub_contractors_list = []
        for company in sub_contractor_companies_queryset:
            member = SubContractorTeamMember.objects.filter(user=company.account_owner).first()
            number_of_projects = member.projects.filter(status='ACTIVE', company=user.company).count()
            total_projects = member.projects.filter(company=user.company).count()
            events = Event.objects.filter(sub_contractor_spoc=member, status="IN-PROGRESS",
                                          project__in=list(member.projects.filter(company=user.company)))
            event_list = [event.name for event in events]

            sub_contractors_list.append(
                {
                    'company_name': company.name,
                    'website': company.website,
                    'email': company.account_owner.email,
                    'head_office': company.head_office,
                    'phone_number': company.account_owner.phone_number,
                    'number_of_projects': number_of_projects,
                    'total_projects': total_projects,
                    'event_list': event_list,
                    'department': member.department,
                    'member_id': member.id,
                    'member_name': member.name
                }
            )

        return JsonResponse({'sub_contractors_list': sub_contractors_list})
