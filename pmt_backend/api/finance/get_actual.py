from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.api.finance.get_budgeted import helper_to_get_units_based_on_component
from pmt_backend.models import Project, Finance


def helper_fun(list_of_actual_supply_side):
    final_list = []

    Bos = {
        'component': 'BoS',
        'components': [],
        'total': 0
    }

    actual_total_cost = 0

    for item in list_of_actual_supply_side:
        if item['component'] in (
                'Solar PV Module',
                'Solar Inverter',
                'Module Mounting Structures with Accessories _ Metal Sheet',
                'Module Mounting Structures with Accessories _ Ground Mount',
                'Module Mounting Structures with Accessories _ RCC',
                'Structures'
        ):
            actual_total_cost += float(item['total_price'])
            final_list.append(
                {
                    'component': item['component'],
                    'total': item['total_price'],
                    'units': helper_to_get_units_based_on_component(item['component']),
                }
            )
        else:
            actual_total_cost += float(item['total_price'])
            Bos['components'].append(
                {
                    'component': item['component'],
                    'total': item['total_price'],
                    'units': helper_to_get_units_based_on_component(item['component']),
                }
            )
            Bos['total'] += float(item['total_price'])

    final_list.append(Bos)

    return final_list, actual_total_cost


class GetActual(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        project = Project.objects.get(pk=project_id)
        finance = Finance.objects.filter(project=project).first()

        list_of_actual_supply_side = []
        list_of_actual_service_side = []
        actual_total_cost_supply_side = 0
        actual_total_cost_service_side = 0

        if finance:
            if finance.actual_supply_side:
                list_of_actual_supply_side, actual_total_cost_supply_side = helper_fun(finance.actual_supply_side)
            else:
                list_of_actual_supply_side = []
                actual_total_cost_supply_side = 0
            if finance.actual_service_side:
                list_of_actual_service_side, actual_total_cost_service_side = helper_fun(
                    finance.actual_service_side)
            else:
                list_of_actual_service_side = []
                actual_total_cost_service_side = 0
        else:
            Finance.objects.create(
                project=project,
                actual_supply_side=[],
                actual_service_side=[],
                budget_supply_side=[],
                budget_service_side=[],
            )

        return JsonResponse({
            'list_of_actual_supply_side': list_of_actual_supply_side,
            'list_of_actual_service_side': list_of_actual_service_side,
            'actual_total_budget_supply_side': actual_total_cost_supply_side,
            'actual_total_budget_service_side': actual_total_cost_service_side
        })
