from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.models import Project, Finance
from pmt_backend.utils.reading_units_module import all_components_unit_dict


def helper_to_get_units_based_on_component(component_name):
    units = ''
    component_units_dict = all_components_unit_dict.get(component_name)
    if component_units_dict:
        format_dict = component_units_dict['format1']
        units = format_dict['unit_format']
    return units


def helper_fun_service_side(list_of_budgeted_service_side):
    final_list = []
    actual_total_cost = 0
    for item in list_of_budgeted_service_side:
        actual_total_cost += float(item['total'])
        final_list.append(
            {
                'component': item['component'],
                'units': helper_to_get_units_based_on_component(item['component']),
                'indirect_cost': item['indirect_cost'],
                'direct_cost': item['direct_cost'],
                'taxes': item['taxes'],
                'total': item['total']
            }
        )
    return final_list, actual_total_cost


def helper_fun(list_of_budgeted_side_items):
    final_list = []

    Bos = {
        'component': 'BoS',
        'components': [],
        'total': 0
    }

    actual_total_cost = 0

    for item in list_of_budgeted_side_items:
        if item['component'] in ('Solar PV Module', 'Solar Modules', 'Inverters', 'Solar Inverter', 'Structures'):
            actual_total_cost += float(item['total']) if item['total'] else 0
            final_list.append(
                {
                    'component': item['component'],
                    'basic_price': item['basic_price'],
                    'transport': item['transport'],
                    'gst': item['gst'],
                    'total': item['total'],
                    'units': helper_to_get_units_based_on_component(item['component'])
                }
            )
        else:
            actual_total_cost += float(item['total']) if item['total'] else 0
            Bos['components'].append(
                {
                    'component': item['component'],
                    'total': item['total'],
                    'units': helper_to_get_units_based_on_component(item['component'])
                }
            )
            Bos['total'] += float(item['total']) if item['total'] else 0

    final_list.append(Bos)
    return final_list, actual_total_cost


class GetBudgeted(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        project_id = request.GET['project_id']
        project = Project.objects.get(pk=project_id)
        finance = Finance.objects.filter(project=project).first()
        if finance:
            if finance.budget_supply_side:
                list_of_budgeted_supply_side, actual_total_cost_supply_side = helper_fun(finance.budget_supply_side)
            else:
                list_of_budgeted_supply_side = []
                actual_total_cost_supply_side = 0

            if finance.budget_service_side:
                list_of_budgeted_service_side, actual_total_cost_service_side = helper_fun_service_side(
                    finance.budget_service_side
                )
            else:
                list_of_budgeted_service_side = []
                actual_total_cost_service_side = 0
        else:
            list_of_budgeted_supply_side = []
            list_of_budgeted_service_side = []
            actual_total_cost_supply_side = 0
            actual_total_cost_service_side = 0

        return JsonResponse({
            'list_of_budgeted_supply_side': list_of_budgeted_supply_side,
            'list_of_budgeted_service_side': list_of_budgeted_service_side,
            'actual_total_budget_supply_side': actual_total_cost_supply_side,
            'actual_total_budget_service_side': actual_total_cost_service_side,
            'project_size': project.size
        })
