from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from openpyxl import load_workbook

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Project, Finance


def supply_side_helper_fun(sheet, start, end):
    budgeted_supply_side = []
    for row in sheet[start:end]:
        if row[2].value and row[3].value:
            budgeted_supply_side.append(
                {
                    'component': row[1].value,
                    'basic_price': row[2].value,
                    'transport': row[3].value,
                    'gst': row[4].value,
                    'total': row[5].value
                }
            )
        elif row[2].value:
            budgeted_supply_side.append(
                {
                    'component': row[1].value,
                    'total_price': row[2].value,
                }
            )

    return budgeted_supply_side


def service_side_helper_fun(sheet, start, end):
    budgeted_service_side = []
    for row in sheet[start:end]:
        if row[11].value:
            budgeted_service_side.append(
                {
                    'component': row[10].value,
                    'direct_cost': row[11].value,
                    'indirect_cost': row[12].value,
                    'taxes': row[13].value,
                    'total': row[14].value
                }
            )

    return budgeted_service_side


class UploadFinanceFile(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        finance_file = request.FILES['finance_file']

        project = Project.objects.get(pk=project_id)

        workbook = load_workbook(filename=finance_file)
        sheet = workbook.active

        list1 = supply_side_helper_fun(sheet, 3, 5)
        list2 = supply_side_helper_fun(sheet, 9, 49)
        budgeted_supply_side = list1 + list2

        budget_service_side = service_side_helper_fun(sheet, 3, 6)

        finance = Finance.objects.filter(project=project).first()
        if finance:
            finance.budget_service_side = budget_service_side
            finance.budget_supply_side = budgeted_supply_side
            finance.save()
        else:
            finance = Finance.objects.create(project=project,
                                             budget_service_side=budget_service_side,
                                             budget_supply_side=budgeted_supply_side
                                             )
        finance.actual_supply_side = []
        finance.actual_service_side = []
        finance.save()

        return JsonResponse({})
