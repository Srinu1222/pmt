from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions

from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.models import Project, Finance


class calculate_p_and_l(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        project_id = request.POST['project_id']
        project = Project.objects.get(pk=project_id)

        if project.project_completion_date:
            finance = Finance.objects.filter(project=project).first()

            actual_price = 0
            if finance:
                list_of_actual_supply_side = finance.actual_supply_side
                list_of_budgeted_supply_side = finance.budget_supply_side
                for item in list_of_actual_supply_side:
                    actual_price += item['basic_price']
                for item in list_of_budgeted_supply_side:
                    actual_price += item['basic_price']

            mode = request.POST['mode']

            serialized_response = {}

            if mode == 'CAPEX':
                basic = request.POST['basic']
                gst = request.POST['gst']
                total_price = int(request.POST['total_price'])
                size = int(request.POST['size'])

                revenue = total_price * size
                margin = total_price - actual_price
                margin_percentage = margin / 100
                project_completion_date = project.project_completion_date
                project_start_date = project.project_start_date
                if project_completion_date and project_start_date:
                    time = (project_completion_date - project_start_date).days
                revenue_per_day = revenue / time

                serialized_response = {
                    'actual_cost': actual_price,
                    'revenue': revenue,
                    'margin': margin,
                    'margin_percentage': margin_percentage,
                    'time': time,
                    'revenue_per_day': revenue_per_day,
                }

            if mode == 'OPEX':
                signing_price = int(request.POST['signing_price'])
                epc_price_target = int(request.POST['epc_price_target'])
                size = int(request.POST['size'])

                revenue = epc_price_target * size
                margin = epc_price_target - actual_price
                margin_percentage = margin / 100
                project_completion_date = project.project_completion_date
                project_start_date = project.project_start_date
                if project_completion_date and project_start_date:
                    time = (project_completion_date - project_start_date).days
                revenue_per_day = revenue / time

                serialized_response = {
                    'actual_cost': actual_price,
                    'revenue': revenue,
                    'margin': margin,
                    'margin_percentage': margin_percentage,
                    'time': time,
                    'revenue_per_day': revenue_per_day,
                }

            return JsonResponse({'p_and_l': serialized_response})
        else:
            return send_fail_http_response(
                'Profit and loss can only be calculated after the project completion.'
                ' Please try again after the project is completed')
