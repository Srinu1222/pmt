from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import Project, Finance
from pmt_backend.utils.convert_json_to_python import json_to_python


def helper_supply_side(budget_side, actual_budget_side):
    if budget_side['component'] == 'BoS':
        for item in actual_budget_side:
            for i in budget_side['components']:
                if item['component'] == i['component']:
                    item['total'] = i['total']
    else:
        for item in actual_budget_side:
            if item['component'] == budget_side['component']:
                item['basic_price'] = budget_side['basic_price']
                item['transport'] = budget_side['transport']
                item['gst'] = budget_side['gst']
                item['total'] = float(budget_side['basic_price']) + float(budget_side['transport']) + float(budget_side['gst'])
                break


def helper_service_side(budget_service_side, actual_budget_service_side):
    for item in actual_budget_service_side:
        if item['component'] == budget_service_side['component']:
            item['direct_cost'] = budget_service_side['direct_cost']
            item['indirect_cost'] = budget_service_side['indirect_cost']
            item['taxes'] = budget_service_side['taxes']
            item['total'] = float(budget_service_side['direct_cost']) + float(budget_service_side['indirect_cost']) + \
                            float(budget_service_side['taxes'])
            break


class UpdateBudgeted(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        project_id = request.POST['project_id']
        project = Project.objects.get(pk=project_id)
        is_budget_supply_side = request.POST.get('budget_supply_side')
        is_budget_service_side = request.POST.get('budget_service_side')

        finance = Finance.objects.filter(project=project).first()

        # For First Case Empty Scenario
        is_new = False
        if not finance:
            is_new = True
            if is_budget_supply_side:
                finance = Finance.objects.create(
                    project=project,
                    budget_supply_side=[json_to_python(is_budget_supply_side)],
                    budget_service_side=[]
                )
            elif is_budget_service_side:
                finance = Finance.objects.create(
                    project=project,
                    budget_supply_side=[],
                    budget_service_side=[json_to_python(is_budget_service_side)]
                )

        # For Already Existing Data
        if not is_new:
            if is_budget_supply_side:
                budget_supply_side = json_to_python(is_budget_supply_side)
                # object there but budget supply side is empty
                if not finance.budget_supply_side:
                    budget_supply_side['total'] = float(budget_supply_side['basic_price']) + float(budget_supply_side['transport']) + float(budget_supply_side['gst'])
                    finance.budget_supply_side = [budget_supply_side]
                else:
                    # component already exists
                    actual_budget_supply_side = finance.budget_supply_side
                    helper_supply_side(budget_supply_side, actual_budget_supply_side)

            elif is_budget_service_side:
                budget_service_side = json_to_python(is_budget_service_side)
                # object there but budget service side is empty
                if not finance.budget_service_side:
                    budget_service_side['total'] = float(budget_service_side['direct_cost']) + float(budget_service_side['indirect_cost']) + \
                            float(budget_service_side['taxes'])
                    finance.budget_service_side = [budget_service_side]
                else:
                    # component already exists
                    actual_budget_service_side = finance.budget_service_side
                    helper_service_side(budget_service_side, actual_budget_service_side)
            finance.save()
        return JsonResponse({})
