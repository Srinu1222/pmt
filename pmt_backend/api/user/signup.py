from pmt_backend.dispatchers.responses.send_fail_http_response import send_fail_http_response
from pmt_backend.mail.mail_tasks.customers.welcome_mail import send_welcome_mail
from pmt_backend.models import User, Company, TeamMember, PaymentInfo, Gantt, PendingOTP
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse


@csrf_exempt
def verify_otp(request):
    if request.method == 'POST':
        password = request.POST['password1']
        email = request.POST['email']
        otp = request.POST['otp']
        pending_otp = PendingOTP.objects.get(email=email)
        company = request.POST['company']
        website = request.POST.get('website')
        head_office = request.POST['head_office']
        regional_offices = request.POST.get('regional_office')
        description = request.POST.get('description')
        turnover = request.POST['turnover']
        employee_strength = request.POST['employee_strength']
        maximum_size_project = request.POST.get('maximum_size_project')
        capacity_installed = request.POST['capacity_installed']
        business_model = request.POST['business_model']
        flagship_projects = request.POST.get('flagship_projects')
        minimum_size_project = request.POST['minimum_size_project']

        if int(otp) != int(pending_otp.otp):
            return send_fail_http_response('OTP Verification Failed.')

        user = User.objects.filter(username=email).first()
        if user:
            msg = "email or username already exists."
            return send_fail_http_response(msg)
        phone_number = request.POST['phone_number']
        name = request.POST['contact_person']
        num_projects_per_year = request.POST['num_projects_per_year']
        company = Company.objects.create(name=company)
        company.website = website
        company.head_office = head_office
        company.regional_office = regional_offices
        company.description = description
        company.annual_turn_over = turnover
        company.employee_strength = employee_strength
        company.max_size_of_projects_installed = maximum_size_project
        company.min_size_of_projects_installed = minimum_size_project
        company.installed_capacity = capacity_installed
        company.business_model_offered = business_model
        company.flagship_projects = flagship_projects
        company.num_projects_per_year = num_projects_per_year
        if 'brochure' in request.FILES:
            brochure = request.FILES['brochure']
            company.upload_brochure = brochure

        PaymentInfo.objects.create(company=company)
        designation = request.POST['department']
        is_consumer = request.POST.get('is_consumer', False)
        user_type = request.POST.get('user_type', 'Normal User')

        user = User.objects.create(
            username=email,
            email=email,
            phone_number=phone_number,
            company=company,
            designation=designation,
            is_consumer=is_consumer,
            user_type=user_type
        )

        user.set_password(password)
        user.save()

        member = TeamMember.objects.create(user=user, name=name, department='Management')
        user.company.team_members.add(member)
        user.save()

        user.company.account_owner = user
        user.company.credits = 20
        company.save()

        # gantt
        gantt = Gantt.objects.all().order_by('pk').first()
        gantt.company.add(company)
        gantt.save()

        # to send welcome mail
        send_welcome_mail.delay(name, company.name, email, password, email)
        return JsonResponse({})
