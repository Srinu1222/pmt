from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from pmt_backend.models import TeamMember


class UpdateDepartment(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        member_id = request.POST['member_id']
        department = request.POST['department']

        member = TeamMember.objects.get(pk=member_id)
        member.department = department

        member.save()

        return JsonResponse({})
