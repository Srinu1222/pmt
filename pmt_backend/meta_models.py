STAGE_STATUS = (
    ('PENDING', 'PENDING'),
    ('IN-PROGRESS', 'IN-PROGRESS'),
    ('FINISHED', 'FINISHED'),
    ('DELAYED', 'DELAYED'),
)

DEPARTMENT = (
    ('Management', 'Management'),
    ('Sales', 'Sales'),
    ('Project', 'Project'),
    ('Procurement', 'Procurement'),
    ('O&M', 'O&M'),
)

DESIGNATION = (
    ('Customer', 'Customer'),
    ('Project Owner', 'Project Owner'),
    ('Project Manager', 'Project Manager'),
    ('Sales', 'Sales'),
    ('Liasoning Manager', 'Liasoning Manager'),
    ('Liasoning Agent', 'Liasoning Agent'),
    ('Engineering Head', 'Engineering Head'),
    ('Designer', 'Designer'),
    ('Lead Designer', 'Lead Designer'),
    ('Procurement Manager', 'Procurement Manager'),
    ('Procurement Executive', 'Procurement Executive'),
    ('Logistics Manager', 'Logistics Manager'),
    ('Logistics Executive', 'Logistics Executive'),
    ('Construction manager', 'Construction manager'),
    ('Safety Manager', 'Safety Manager'),
    ('Site In-Charge', 'Site In-Charge'),
    ('Site Executive', 'Site Executive'),
    ('O&M Manager', 'O&M Manager'),
    ('O&M Executive', 'O&M Executive'),
)

PROJECT_STATUS = (
    ('ACTIVE', 'ACTIVE'),
    ('CLOSED', 'CLOSED')
)

STAGES = (
    ('PRE-REQUISITES', 'PRE-REQUISITES'),
    ('APPROVALS', 'APPROVALS'),
    ('ENGINEERING', 'ENGINEERING'),
    ('PROCUREMENT', 'PROCUREMENT'),
    ('MATERIAL HANDLING', 'MATERIAL HANDLING'),
    ('CONSTRUCTION', 'CONSTRUCTION'),
    ('SITE HAND OVER', 'SITE HAND OVER'),
)

EVENT_STATUS = (
    ('PENDING', 'PENDING'),
    ('IN-PROGRESS', 'IN-PROGRESS'),
    ('DELAYED', 'DELAYED'),
    ('FINISHED', 'FINISHED')
)

PROPERTIES = (
    ('COMMERCIAL', 'COMMERCIAL'),
    ('INDUSTRIAL', 'INDUSTRIAL'),
    ('RESEDENTIAL', 'RESEDENTIAL')
)

TEAM_ROLE_TYPES = (
    ('Customer', 'Customer'),
    ('Project Manager', 'Project Manager'),
    ('Sales', 'Sales'),
    ('Liasoning Manager', 'Liasoning Manager'),
    ('Liasoning Officer', 'Liasoning Officer'),
    ('Designer', 'Designer'),
    ('Lead Designer', 'Lead Designer'),
    ('Procurement Manager', 'Procurement Manager'),
    ('Procurement Executive', 'Procurement Executive'),
    ('Logistics Manager', 'Logistics Manager'),
    ('Logistics Executive', 'Logistics Executive'),
    ('Safety Manager', 'Safety Manager'),
    ('Site In-Charge', 'Site In-Charge'),
    ('Site Executive', 'Site Executive'),
    ('O&M Manager', 'O&M Manager'),
    ('O&M Executive', 'O&M Executive')
)

DRAWINGS = (
    ('ARRAY LAYOUT', 'Array Layout'),
    ('PV-SYST', 'PV-Syst'),
    ('EQUIPMENT LAYOUT', 'Equipment Layout'),
    ('EARTHING', 'Earthing'),
    ('LA LAYOUT', 'LA Layout'),
    ('CABLE LAYOUT', 'Cable Layout'),
    ('STRUCTURE DRAWINGS_RCC', 'Structure Drawings_RCC'),
    ('STRUCTURE DRAWINGS_METAL SHEET', 'Structure Drawings_Metal Sheet'),
    ('STRUCTURE DRAWINGS_GROUND MOUNT', 'Structure Drawings_Ground Mount'),
    ('WALKWAYS', 'Walkways'),
    ('SAFETY LINE', 'Safety Line'),
    ('SAFETY RAILS', 'Safety Rails'),
    ('PROJECT SLD', 'Project SLD'),
    ('FINAL BOQ', 'Final BOQ'),
    ('DETAILED PROJECT REPORT', 'Detailed Project Report')
)

ACCESSORIES = (
    ('REVERSE PROTECTION RELAY', 'Reverse Protection Relay'),
    ('NET METERING', 'Net Metering'),
    ('DG SYNCHRONIZATION', 'DG Synchronization'),
    ('LADDERS', 'Ladders'),
    ('SAFETY RAILS', 'Safety Rails'),
    ('WALKWAYS', 'Walkways'),
    ('SCADA SYSTEM', 'Scada System'),
    ('PYRO METER', 'Pyro Meter'),
    ('AMBIENT TEMPERATURE SENSORS', 'Ambient Temperature Sensors'),
    ('MODULE TEMPERATURE SENSORS', 'Module Temperature Sensors'),
    ('UPS', 'UPS')
)

AREA_OF_INSTALLATIONS = (
    ('GROUND MOUNT', 'Ground Mount'),
    ('RCC', 'RCC'),
    ('METAL SHEET', 'Metal Sheet')
)

TERMINATION_CHOICES = (
    ('LT', 'LT'),
    ('HT', 'HT')
)

INVERTER_TO_ACDB = ACDB_TO_TRANSFORMER = TRANSFORMER_TO_HT_PANEL = (
    ('CABLE TRAYS', 'Cable Trays'),
    ('CABLE TRENCHES', 'Cable Trenches'),
    ('HDPE PIPES', 'HDPE Pipes')
)

NOTIFICATION_CHOICES = (
    ('project_gantt', 'project_gantt'),
    ('project_dashboard', 'project_dashboard'),
    ('detailed_event', 'detailed_event'),
    ('specification_page', 'specification_page'),
    ('team_page', 'team_page'),
    ('gantt_page', 'gantt_page'),
    ('ticket_view', 'ticket_view'),
    ('gantt_chart', 'gantt_chart'),
    ('stage_detail', 'stage_detail'),
    ('gantt_view', 'gantt_view'),
    ('popup_request', 'popup_request'),
    ('detailed_stage', 'detailed_stage'),
    ('non_clickable', 'non_clickable'),
    ('particular_event', 'particular_event'),
    ('event', 'event'),
    ('stage_page', 'stage_page')
)

INVENTORY_STATUS = (
    ('PO_PLACED', 'PO_PLACED'),
    ('IN_TRANSIT', 'IN_TRANSIT'),
    ('DELIVERED_ON_SITE', 'DELIVERED_ON_SITE'),
    ('STORED_ON_SITE', 'STORED_ON_SITE'),
    ('USED', 'USED'),
    ('WASTAGE', 'WASTAGE')
)

PDI_NEEDED = (
    ('Yes', 'Yes'),
    ('No', 'No')
)


COMPONENTS = (
    ('Solar PV Module', 'Solar PV Module'),
    ('Solar Inverter', 'Solar Inverter'),
    ('MC4 Connectors', 'MC4 Connectors'),
    ('Module Mounting Structures with Accessories _ Metal Sheet',
     'Module Mounting Structures with Accessories _ Metal Sheet'),
    ('Module Mounting Structures with Accessories _ Ground Mount',
     'Module Mounting Structures with Accessories _ Ground Mount'),
    ('Module Mounting Structures with Accessories _ RCC', 'Module Mounting Structures with Accessories _ RCC'),
    ('Structure Foundation/\n Pedestal', 'Structure Foundation/\n Pedestal'),
    ('Solar DC Cable', 'Solar DC Cable'),
    ('Cabling Accessories', 'Cabling Accessories'),
    ('Earthing Protection System', 'Earthing Protection System'),
    ('Lighting Arrestor', 'Lighting Arrestor'),
    ('LT Panel', 'LT Panel'),
    ('HT Panel', 'HT Panel'),
    ('AC Distribution (Combiner) Panel Board', 'AC Distribution (Combiner) Panel Board'),
    ('Transformer', 'Transformer'),
    ('LT/HT Power Cable 1', 'LT/HT Power Cable 1'),
    ('LT/HT Power Cable 2', 'LT/HT Power Cable 2'),
    ('LT/HT Power Cable 3', 'LT/HT Power Cable 3'),
    ('Earthing Cable', 'Earthing Cable'),
    ('Module Earthing Cable', 'Module Earthing Cable'),
    ('DC Junction Box (SCB/SMB)', 'DC Junction Box (SCB/SMB)'),
    ('GI Strip', 'GI Strip'),
    ('Metering Panel', 'Metering Panel'),
    ('MDB Breaker', 'MDB Breaker'),
    ('Cable trays', 'Cable trays'),
    ('HT Breaker', 'HT Breaker'),
    ('Bus bar', 'Bus bar'),
    ('InC Contractor', 'InC Contractor'),
    ('Reverse Protection Relay', 'Reverse Protection Relay'),
    ('Net-Metering', 'Net-Metering'),
    ('DG Synchronization', 'DG Synchronization'),
    ('Communication Cable', 'Communication Cable'),
    ('Safety Rails', 'Safety Rails'),
    ('Walkways', 'Walkways'),
    ('Safety Lines', 'Safety Lines'),
    ('SCADA System', 'SCADA System'),
    ('Pyranometer', 'Pyranometer'),
    ('Weather Monitoring Unit', 'Weather Monitoring Unit'),
    ('Ambient Temperature Sensors', 'Ambient Temperature Sensors'),
    ('UPS', 'UPS'),
    ('Module Temperature Sensors', 'Module Temperature Sensors'),
    ('Wire mesh for protection of Skylights', 'Wire mesh for protection of Skylights'),
    ('Cleaning System', 'Cleaning System'),
    ('Danger Board and Signs', 'Danger Board and Signs'),
    ('Fire Extinguisher', 'Fire Extinguisher'),
    ('Generation Meter', 'Generation Meter'),
    ('Zero Export Device', 'Zero Export Device'),
    ('Other Sensors', 'Other Sensors')
)

TAGS = (
    ('RoofTop', 'RoofTop'),
    ('Commercial', 'Commercial'),
    ('Residential', 'Residential'),
    ('CarPort', 'CarPort'),
    ('Ground Mount', 'Ground Mount'),
)

STAKEHOLDERTYPES = (
    ('Sub Contractor', 'Sub Contractor'),
    ('Customer', 'Customer'),
    ('Investor', 'Investor'),
    ('Safearth Project Manager', 'Safearth Project Manager'),
    ('Normal User', 'Normal User')
)

USERTYPES = (
    ('Sub Contractor', 'Sub Contractor'),
    ('Customer', 'Customer'),
    ('Investor', 'Investor'),
    ('Safearth Project Manager', 'Safearth Project Manager'),
    ('Normal User', 'Normal User')
)

ISSUETYPES = (
    ('Major Issue', 'Major Issue'),
    ('Minor Issue', 'Minor Issue'),
    ('Feedback', 'Feedback')
)
