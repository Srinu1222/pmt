from datetime import datetime, timedelta
from pmt_backend.models import Project
from pmt_backend.utils.convert_naive_to_aware import convert_naive_to_aware
from math import floor

def demo(project_id):
    project = Project.objects.get(pk=project_id)
    old_start_date = project.project_start_date
    project.project_start_date = datetime(2021, 10, 1)
    project.save()
    project_start_date = project.project_start_date

    event_set = project.event_set.all().order_by('?')

    total_events = event_set.count()
    eighty_percent = floor(80 / 100 * total_events)
    ten_percent = floor(20 / 100 * total_events)

    for event in event_set:
        start_days = (event.start_time - old_start_date).days
        start_time = convert_naive_to_aware(project_start_date + timedelta(days=start_days))
        event.start_time = start_time
        event.completed_time = start_time + timedelta(days=event.days_needed)

        if eighty_percent != 0:
            event.status = 'FINISHED'
            event.actual_start_time = event.start_time
            event.actual_completed_time = event.completed_time
            eighty_percent -= 1
        elif ten_percent != 0:
            event.is_delayed = True
            event.actual_start_date = event.start_time + timedelta(days=1)
            event.actual_completed_time = event.completed_time + timedelta(days=1)
            ten_percent -= 1

        event.save()
