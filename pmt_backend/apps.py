from django.apps import AppConfig


class PmtBackendConfig(AppConfig):
    name = 'pmt_backend'
