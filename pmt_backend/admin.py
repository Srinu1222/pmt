from django.contrib import admin
from pmt_backend.models import (
    User, Project, Customer, Databank, Document, Inventory,
    TeamMember, Company, Ticket, Event, ProjectSpecification, ProjectRight,
    Notification, DailyUpdate, DailyReport, MonthlyReport, Testimonial, Award, KeyPersonnel,
    Invoice, Gantt, SubContractorTeamMember, PaymentInfo, Finance, PendingOTP, SampleEvent, StakeHolder, RequestDemo
)
from import_export.admin import ImportExportModelAdmin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from import_export import resources
from import_export.fields import Field

admin.site.site_header = 'PMT SafEarth Administration'
admin.site.site_title = 'PMT SafEarth Administration'
admin.site.index_title = 'PMT SafEarth Administration'


class EventAdmin(admin.ModelAdmin):
    list_display = ['name', 'id', 'default_approver_id', 'default_spoc_id', 'stage', 'project_id', "start_time", 'completed_time']
    search_fields = ('project__name', 'id', 'stage', 'project__id')
    ordering = ('-start_time',)


class TicketAdmin(admin.ModelAdmin):
    list_display = '__all__'


class UserResource(resources.ModelResource):
    username = Field(attribute='username', column_name='Email ID of employee who is filling the form')
    email = Field(attribute='email', column_name='Email ID of employee who is filling the form')
    phone_number = Field(attribute='phone_number', column_name='Phone Number of employee who is filling the form')

    class Meta:
        model = User


class UserAdmin(BaseUserAdmin, ImportExportModelAdmin):
    add_fieldsets = (
        (None, {
            'fields': (
                'email', 'username', 'password1', 'password2', 'phone_number',
                'last_login'
            )
        }),
        ('Permissions', {
            'fields': ('is_superuser', 'is_staff')
        })
    )
    fieldsets = (
        (None, {
            'fields': (
                'email', 'username', 'password', 'phone_number', 'last_login')
        }),
        ('Permissions', {
            'fields': ('is_superuser', 'is_staff')
        })
    )
    list_display = ['email', 'username' , 'last_login']
    search_fields = ('email', 'username')
    resource_class = UserResource


class ProjectAdmin(admin.ModelAdmin):
    list_display = ['company', 'name']


class CompanyAdmin(admin.ModelAdmin):
    search_fields = ('name',)


class DocumentAdmin(admin.ModelAdmin):
    list_display = ['databank', 'upload_date']
    # search_fields = ('databank__id')
    # ordering = ('-upload_date')


# Register your models here.
admin.site.register(User, UserAdmin)
admin.site.register(Finance)
admin.site.register(RequestDemo)
admin.site.register(Invoice)
admin.site.register(PaymentInfo)
admin.site.register(Notification)
admin.site.register(SubContractorTeamMember)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Ticket)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Customer)
admin.site.register(Inventory)
admin.site.register(Event, EventAdmin)
admin.site.register(ProjectRight)
admin.site.register(Databank)
admin.site.register(Document, DocumentAdmin)
admin.site.register(TeamMember)
admin.site.register(ProjectSpecification)
admin.site.register(DailyUpdate)
admin.site.register(DailyReport)
admin.site.register(Testimonial)
admin.site.register(Award)
admin.site.register(KeyPersonnel)
admin.site.register(Gantt)
admin.site.register(MonthlyReport)
admin.site.register(PendingOTP)
admin.site.register(SampleEvent)
admin.site.register(StakeHolder)


